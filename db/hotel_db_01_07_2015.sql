-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 01, 2015 at 02:02 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hotel_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_owners`
--

CREATE TABLE IF NOT EXISTS `admin_owners` (
  `ao_id` int(13) NOT NULL AUTO_INCREMENT,
  `ao_firstname` varchar(50) NOT NULL,
  `ao_lastname` varchar(50) NOT NULL,
  `ao_email` varchar(255) NOT NULL,
  `ao_username` varchar(25) NOT NULL,
  `ao_password` varchar(25) NOT NULL,
  `ao_address` text NOT NULL,
  `ao_country` varchar(50) NOT NULL,
  `ao_city` varchar(50) NOT NULL,
  `ao_zip` varchar(25) NOT NULL,
  `ao_phone` varchar(25) NOT NULL,
  `ao_type` enum('admin','mod','owner','cadmin') NOT NULL,
  `ao_status` enum('active','inactive') NOT NULL,
  `ao_created_by` int(13) NOT NULL,
  `ao_created_at` datetime NOT NULL,
  `ao_update_by` int(13) NOT NULL,
  `ao_updated_at` datetime NOT NULL,
  PRIMARY KEY (`ao_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(13) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(50) NOT NULL,
  `user_lastname` varchar(50) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_username` varchar(25) NOT NULL,
  `user_password` varchar(25) NOT NULL,
  `user_address` text NOT NULL,
  `user_country` varchar(50) NOT NULL,
  `user_city` varchar(50) NOT NULL,
  `user_zip` varchar(25) NOT NULL,
  `user_phone` varchar(25) NOT NULL,
  `user_photo` text NOT NULL,
  `user_type` enum('admin','mod','owner','cadmin') NOT NULL,
  `user_status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
