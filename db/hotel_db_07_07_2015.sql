-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2015 at 07:13 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hotel_db`
--
CREATE DATABASE IF NOT EXISTS `hotel_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hotel_db`;

-- --------------------------------------------------------

--
-- Table structure for table `admin_owners`
--

CREATE TABLE IF NOT EXISTS `admin_owners` (
  `ao_id` int(13) NOT NULL AUTO_INCREMENT,
  `ao_firstname` varchar(50) NOT NULL,
  `ao_lastname` varchar(50) NOT NULL,
  `ao_email` varchar(255) NOT NULL,
  `ao_username` varchar(25) NOT NULL,
  `ao_password` varchar(50) NOT NULL,
  `ao_address` text NOT NULL,
  `ao_country` varchar(50) NOT NULL,
  `ao_city` varchar(50) NOT NULL,
  `ao_zip` varchar(25) NOT NULL,
  `ao_phone` varchar(25) NOT NULL,
  `ao_type` enum('admin','mod','owner','cadmin') NOT NULL,
  `ao_status` enum('active','inactive') NOT NULL,
  `ao_created_by` int(13) NOT NULL,
  `ao_created_at` datetime NOT NULL,
  `ao_update_by` int(13) NOT NULL,
  `ao_updated_at` datetime NOT NULL,
  PRIMARY KEY (`ao_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `admin_owners`
--

INSERT INTO `admin_owners` (`ao_id`, `ao_firstname`, `ao_lastname`, `ao_email`, `ao_username`, `ao_password`, `ao_address`, `ao_country`, `ao_city`, `ao_zip`, `ao_phone`, `ao_type`, `ao_status`, `ao_created_by`, `ao_created_at`, `ao_update_by`, `ao_updated_at`) VALUES
(1, 'Mahedi', 'Hasan', 'mahedi302@gmail.com', 'mahedi007', 'e10adc3949ba59abbe56e057f20f883e', 'nikunja-2', 'bangladesh', 'dhaka', '1239', '', 'admin', 'active', 1, '2015-07-01 12:18:20', 0, '0000-00-00 00:00:00'),
(2, 'Ponir', ' Rafi', 'ponir@gmail.com', 'ponir007', 'e10adc3949ba59abbe56e057f20f883e', 'dsrsdf', 'dsf', 'sdfsd', 'fdsf', '', 'mod', 'active', 1, '2015-07-02 08:31:32', 0, '0000-00-00 00:00:00'),
(3, 'Anik', 'Sarker', 'amik@gmail.com', 'anik007', 'e10adc3949ba59abbe56e057f20f883e', 'fsdfsdf', 'hgfhfgh', 'sfdsfddf', 'hhfg', '', 'owner', 'active', 2, '2015-07-02 10:13:11', 0, '0000-00-00 00:00:00'),
(5, 'Fayme', 'Shariar', 'fayme@gmail.com', 'fayme007', 'e10adc3949ba59abbe56e057f20f883e', 'fdgf', 'dsfdsf', 'fdsfds', 'sdfsdf', '', 'cadmin', 'active', 1, '2015-07-06 07:14:49', 0, '0000-00-00 00:00:00'),
(6, 'Rafi', 'Bhai', 'rafi@gmail.com', 'rafi007', 'e10adc3949ba59abbe56e057f20f883e', 'jhfusygf', 'dsfdsf', 'sdfsdf', 'dsfdsf', '', 'owner', 'inactive', 1, '2015-07-06 09:52:38', 0, '0000-00-00 00:00:00'),
(7, 'Rofi', 'faltu', 'rofi@gmail.com', 'faltu007', 'e10adc3949ba59abbe56e057f20f883e', 'drdfgdfg', 'dfgfdg', 'fdgfdg', 'fdgfdg', '', 'cadmin', 'inactive', 6, '2015-07-06 10:00:11', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_settings`
--

CREATE TABLE IF NOT EXISTS `admin_settings` (
  `ads_id` int(11) NOT NULL AUTO_INCREMENT,
  `ads_key` varchar(50) NOT NULL,
  `ads_title` varchar(50) NOT NULL,
  `ads_value` varchar(50) NOT NULL,
  PRIMARY KEY (`ads_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `admin_settings`
--

INSERT INTO `admin_settings` (`ads_id`, `ads_key`, `ads_title`, `ads_value`) VALUES
(1, 'room_type', 'Room Type', 'Deluxe'),
(2, 'room_type', 'Room Type', 'Normal'),
(4, 'room_type', 'Room Type', 'Premium'),
(6, 'room_feature', 'Room Type', 'BathTubb'),
(7, 'room_feature', 'Room Feature', 'AC'),
(8, 'room_feature', 'Room Feature', 'Heater');

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE IF NOT EXISTS `hotels` (
  `hotel_id` int(13) NOT NULL AUTO_INCREMENT,
  `hotel_name` varchar(255) NOT NULL,
  `hotel_description` text NOT NULL,
  `hotel_banner_image` text NOT NULL,
  `hotel_owner_id` int(13) NOT NULL,
  `hotel_type` varchar(255) NOT NULL,
  `hotel_category` varchar(255) NOT NULL,
  `hotel_address` text NOT NULL,
  `hotel_city` varchar(255) NOT NULL,
  `hotel_zip` varchar(255) NOT NULL,
  `hotel_country` varchar(255) NOT NULL,
  `hotel_facilities` text NOT NULL,
  `hotel_website` text NOT NULL,
  `hotel_facebook` text NOT NULL,
  `hotel_twitter` text NOT NULL,
  `hotel_status` enum('active','inactive') NOT NULL,
  `hotel_created_by` int(13) NOT NULL,
  `hotel_created_on` datetime NOT NULL,
  `hotel_updated_by` int(13) NOT NULL,
  `hotel_update_on` datetime NOT NULL,
  PRIMARY KEY (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`hotel_id`, `hotel_name`, `hotel_description`, `hotel_banner_image`, `hotel_owner_id`, `hotel_type`, `hotel_category`, `hotel_address`, `hotel_city`, `hotel_zip`, `hotel_country`, `hotel_facilities`, `hotel_website`, `hotel_facebook`, `hotel_twitter`, `hotel_status`, `hotel_created_by`, `hotel_created_on`, `hotel_updated_by`, `hotel_update_on`) VALUES
(1, 'Abc', 'adsad', '', 1, 'asdda', 'dd', 'adsd', 'adad', 'adad', 'adad', 'adad', '', 'adad', 'adad', 'active', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(2, 'ddddddddddddddddda', 'addddd', 'ddddddddddddddddda_70.jpg', 0, 'adad', 'adas', 'adas', 'adsas', 'asdas', 'sadas', 'asdas', '', 'asdasd', 'asdas', 'active', 2, '2015-07-03 08:13:52', 2, '2015-07-03 08:13:52'),
(3, 'test anik', 'fsfsf', 'testanik_0.jpg', 3, 'sfsdf', 'sfsf', 'sfsd', 'sf', 'sdfs', 'sfsdf', 'sfdsd', '', 'sfsf', 'sfsf', 'active', 3, '2015-07-03 09:31:16', 3, '2015-07-03 09:31:16');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_admins`
--

CREATE TABLE IF NOT EXISTS `hotel_admins` (
  `ha_id` int(13) NOT NULL AUTO_INCREMENT,
  `ha_hotel_id` int(13) NOT NULL,
  `ha_ao_id` int(13) NOT NULL,
  `ha_status` enum('active','inactive') NOT NULL,
  `ha_created_by` int(13) NOT NULL,
  `ha_created_on` datetime NOT NULL,
  `ha_updated_by` int(13) NOT NULL,
  `ha_update_on` datetime NOT NULL,
  PRIMARY KEY (`ha_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `hotel_admins`
--

INSERT INTO `hotel_admins` (`ha_id`, `ha_hotel_id`, `ha_ao_id`, `ha_status`, `ha_created_by`, `ha_created_on`, `ha_updated_by`, `ha_update_on`) VALUES
(1, 3, 5, 'active', 1, '2015-07-06 07:14:49', 0, '0000-00-00 00:00:00'),
(2, 4, 7, 'active', 6, '2015-07-06 10:00:12', 0, '0000-00-00 00:00:00'),
(3, 2, 4, 'active', 6, '2015-07-06 10:00:12', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_rooms`
--

CREATE TABLE IF NOT EXISTS `hotel_rooms` (
  `hr_id` int(13) NOT NULL AUTO_INCREMENT,
  `hr_hotel_id` int(13) NOT NULL,
  `hr_room_title` varchar(255) NOT NULL,
  `hr_room_description` text NOT NULL,
  `hr_room_features` text NOT NULL,
  `hr_room_size` text NOT NULL,
  `hr_room_status` enum('active','inactive') NOT NULL,
  `hr_created_on` datetime NOT NULL,
  `hr_created_by` int(13) NOT NULL,
  `hr_update_on` datetime NOT NULL,
  `hr_updated_by` int(13) NOT NULL,
  `hr_room_images` text NOT NULL,
  `hr_room_type` varchar(255) NOT NULL,
  `hr_adult` int(13) NOT NULL,
  `hr_children` int(13) NOT NULL,
  `hr_price` double NOT NULL,
  PRIMARY KEY (`hr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(13) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(50) NOT NULL,
  `user_lastname` varchar(50) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_username` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `user_address` text NOT NULL,
  `user_country` varchar(50) NOT NULL,
  `user_city` varchar(50) NOT NULL,
  `user_zip` varchar(25) NOT NULL,
  `user_phone` varchar(25) NOT NULL,
  `user_photo` text NOT NULL,
  `user_status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_firstname`, `user_lastname`, `user_email`, `user_username`, `user_password`, `user_address`, `user_country`, `user_city`, `user_zip`, `user_phone`, `user_photo`, `user_status`) VALUES
(1, 'Rafiuzzaman', 'ponir', 'raf.ponir@gmail.com', 'ponir', 'e10adc3949ba59abbe56e057f20f883e', 'dhaka', 'Bangladesh', 'Dhaka', '1229', '01674564993', '', 'active'),
(2, 'assas', 'asasass', 'raf@gmail.com', 'aasasas', 'e10adc3949ba59abbe56e057f20f883e', 'sdsd', 'sdsdsd', 'sdsdsd', 'ssdsf', 'ZCZXZCz', '', 'inactive'),
(3, 'asdasdad', 'adasdada', 'admin@gmail.com', 'adadaa', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', 'adad', 'adad', 'adad', 'adad', 'adadad', 'adadaa_26.jpg', 'active');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
