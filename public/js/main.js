var baseUrl = $("meta[name='baseUrl']").attr("content");
$("#register").on("click", function () {

    var _token = $("#_token").val();
    var fname = $("#fname").val();
    var lname = $("#lname").val();
    var email = $("#email").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var con_password = $("#con_password").val();
    var admin_type = $("#admin_type").val();
    var address = $("#address").val();
    var city = $("#city").val();
    var zip = $("#zip").val();
    var country = $("#country").val();
    if ($.trim(fname) == "" || $.trim(fname) == null||!isNaN(fname)) {
        $("#fname").focus();
    } else if ($.trim(lname) == "" || $.trim(lname) == null||!isNaN(lname)) {
        $("#lname").focus();
    } else if ($.trim(email) == "" || $.trim(email) == null) {
        $("#email").focus();
    } else if ($.trim(username) == "" || $.trim(username) == null) {
        $("#username").focus();
    } else if ($.trim(fname) == "" || $.trim(fname) == null) {
        $("#fname").focus();
    } else if ($.trim(password) == "" || $.trim(password) == null || password.length < 6) {
        $("#password").focus();
    } else if ($.trim(password) != $.trim(con_password)) {
        $("#con_password").focus();
        alert("password doesn't match")
    } else if ($.trim(admin_type) == "" || $.trim(admin_type) == null) {
        $("#admin_type").focus();
    } else if ($.trim(address) == "" || $.trim(address) == null) {
        $("#address").focus();
    } else if ($.trim(city) == "" || $.trim(city) == null) {
        $("#city").focus();
    } else if ($.trim(zip) == "" || $.trim(zip) == null) {
        $("#zip").focus();
    } else if ($.trim(country) == "" || $.trim(country) == null) {
        $("#country").focus();
    }

    else {
        $.ajax({
            url: baseUrl + "/admin/registerAdmin",
            type: "POST",
            dataType:"json",
            data: {
                fname: fname,
                lname: lname,
                ao_email: email,
                ao_username: username,
                password: password,
                admin_type: admin_type,
                address: address,
                city: city,
                zip: zip,
                country: country,
                con_password: con_password,
                _token: _token

            },
            success: function (result) {
                if(result.status)
                {
                    window.location = baseUrl + "/admin/manage";
                }

            }

        });
    }

});