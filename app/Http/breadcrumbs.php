<?php
Breadcrumbs::register('admin', function($breadcrumbs)
{
    $breadcrumbs->push('admin', url('admin'));
});
// createAdmin
Breadcrumbs::register('createadmin', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('createadmin', url('admin/createadmin'));
});

// createHotelAdmin
Breadcrumbs::register('createHotelAdmin', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('createHotelAdmin', url('admin/createHotelAdmin'));
});

Breadcrumbs::register('manageadmin', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('manageadmin', url('admin/manage'));
});

// Home > Blog > [Category]
Breadcrumbs::register('admindetails', function($breadcrumbs, $admin_owner)
{
    $breadcrumbs->parent('manageadmin');
    $breadcrumbs->push($admin_owner->ao_firstname, url('admin/manage/details/{id}',$admin_owner->ao_id));
});

Breadcrumbs::register('createhotel', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('createhotel', url('admin/hotel/create'));
});
Breadcrumbs::register('managehotel', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('managehotel', url('admin/hotel/manage'));
});
Breadcrumbs::register('edithotel', function($breadcrumbs, $hotel)
{
    $breadcrumbs->parent('managehotel');
    $breadcrumbs->push($hotel->hotel_name, url('admin/hotel/edit/{id}',$hotel->hotel_id));
});
Breadcrumbs::register('detailhotel', function($breadcrumbs, $hotel)
{
    $breadcrumbs->parent('managehotel');
    $breadcrumbs->push($hotel->hotel_name, url('admin/hotel/details/{id}',$hotel->hotel_id));
});
//settings
Breadcrumbs::register('createroomtype', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('createroomtype', url('admin/setting/roomtype/create'));
});
Breadcrumbs::register('manageroomtype', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('manageroomtype', url('admin/setting/roomtype/manage'));
});
Breadcrumbs::register('editmanageroomtype', function($breadcrumbs, $setting)
{
    $breadcrumbs->parent('manageroomtype');
    $breadcrumbs->push($setting->ads_value, url('admin/setting/roomtype/edit/{id}',$setting->ads_id));
});

Breadcrumbs::register('createroomfeature', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('createroomfeature', url('admin/setting/roomfeature/create'));
});
Breadcrumbs::register('manageroomfeature', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('manageroomfeature', url('admin/setting/roomfeature/manage'));
});


Breadcrumbs::register('managehotelfeature', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('managehotelfeature', url('admin/setting/hotelfeature/manage'));
});
Breadcrumbs::register('createhotelfeature', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('createhotelfeature', url('admin/setting/hotelfeature/create'));
});
Breadcrumbs::register('edithotelfeature', function($breadcrumbs, $setting)
{
    $breadcrumbs->parent('managehotelfeature');
    $breadcrumbs->push($setting->ads_value, url('admin/setting/roomfeature/edit/{id}',$setting->ads_id));
});

Breadcrumbs::register('editmanageroomfeature', function($breadcrumbs, $setting)
{
    $breadcrumbs->parent('manageroomfeature');
    $breadcrumbs->push($setting->ads_value, url('admin/setting/roomfeature/edit/{id}',$setting->ads_id));
});
//rooms
Breadcrumbs::register('manageroom', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('manageroom', url('admin/room/manage'));
});

Breadcrumbs::register('createroom', function($breadcrumbs)
{
    $breadcrumbs->parent('manageroom');
    $breadcrumbs->push('createroom', url('admin/room/create'));
});
Breadcrumbs::register('editroom', function($breadcrumbs, $room)
{
    $breadcrumbs->parent('manageroom');
    $breadcrumbs->push($room->hr_room_title, url('admin/room/edit/{id}',$room->hr_id));
});
//room ends

//voucher

Breadcrumbs::register('vouchermanage', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('vouchermanage', url('admin/voucher/manage'));
});
Breadcrumbs::register('createvoucher', function($breadcrumbs)
{
    $breadcrumbs->parent('vouchermanage');
    $breadcrumbs->push('createvoucher', url('admin/voucher/create'));
});
Breadcrumbs::register('editvoucher', function($breadcrumbs,$voucher)
{
    $breadcrumbs->parent('vouchermanage');
    $breadcrumbs->push($voucher->voucher_code, url('admin/voucher/edit/{id}',$voucher->voucher_id));
});

Breadcrumbs::register('createbooking', function($breadcrumbs)
{
    $breadcrumbs->push('createbooking', url('admin/booking/create'));
});
Breadcrumbs::register('bookingmanage', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('bookingmanage', url('admin/booking/manage'));
});
Breadcrumbs::register('editbooking', function($breadcrumbs,$booking)
{
    $breadcrumbs->parent('bookingmanage');
    $breadcrumbs->push($booking->booking_id, url('admin/voucher/edit/{id}',$booking->booking_id));
});