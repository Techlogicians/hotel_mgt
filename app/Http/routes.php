<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('home', 'HomeController@index');

Route::group(array('prefix' => 'admin'), function() {
    Route::get('/', 'Admin\HomeController@viewLogin');
    Route::get('createadmin', 'Admin\HomeController@viewRegister');
    Route::post('registerAdmin', 'Admin\HomeController@adminRegister');
    Route::post('checkLogin', 'Admin\HomeController@checkLogin');
    Route::get('logout', 'Admin\HomeController@logout');
    Route::get('changePassword', 'Admin\HomeController@viewChangePassword');
    Route::post('changeAdminPassword', 'Admin\HomeController@changePassword');
    Route::get('createHotelAdmin', 'Admin\HomeController@createHotelAdmin');
    Route::post('hotelAdmin/create', 'Admin\HomeController@saveHotelAdmin');
    Route::get('manage', 'Admin\HomeController@manageAdmin');
    Route::get('manage/edit/{id}', 'Admin\HomeController@editadmin');
    Route::get('manage/details/{id}', 'Admin\HomeController@showAdminDetails');
    Route::post('manage/update/{id}', 'Admin\HomeController@updateAdmin');
    Route::get('manage/delete/{id}', 'Admin\HomeController@deleteAdmin');
    Route::post('manage/block', 'Admin\HomeController@block');
    Route::post('manage/unblock', 'Admin\HomeController@unblock');

    //hotel
    Route::get('hotel/create', 'Admin\HotelController@create');
    Route::Post('hotel/save', 'Admin\HotelController@SaveHotel');
    Route::get('hotel/manage', 'Admin\HotelController@manage');
    Route::get('hotel/edit/{id}', 'Admin\HotelController@editHotel');
    Route::get('hotel/details/{id}', 'Admin\HotelController@showHotelDetails');
    Route::post('hotel/update/{id}', 'Admin\HotelController@updateHotel');
    Route::get('hotel/delete/{id}', 'Admin\HotelController@deleteHotel');
    Route::post('/hotel/block', 'Admin\HotelController@block');
    Route::post('/hotel/unblock', 'Admin\HotelController@unblock');

    //settings
    Route::get('setting/roomtype/create', 'Admin\SettingsController@createRoomType');
    Route::Post('setting/roomtype/save', 'Admin\SettingsController@saveRoomType');
    Route::get('setting/roomtype/manage', 'Admin\SettingsController@manageRoomtype');
    Route::get('setting/roomtype/edit/{id}', 'Admin\SettingsController@editRoomType');
    Route::post('setting/roomtype/update/{id}', 'Admin\SettingsController@updateRoomType');
    Route::get('setting/roomtype/delete/{id}', 'Admin\SettingsController@deleteSetting');

    Route::get('setting/roomfeature/create', 'Admin\SettingsController@createRoomFeature');
    Route::Post('setting/roomfeature/save', 'Admin\SettingsController@saveRoomFeature');
    Route::get('setting/roomfeature/manage', 'Admin\SettingsController@manageRoomFeature');
    Route::get('setting/roomfeature/edit/{id}', 'Admin\SettingsController@editRoomFeature');
    Route::post('setting/roomfeature/update/{id}', 'Admin\SettingsController@updateRoomFeature');

    Route::get('setting/hotelfeature/create', 'Admin\SettingsController@CreateHotelFeature');
    Route::Post('setting/hotelfeature/save', 'Admin\SettingsController@SaveHotelFeature');
    Route::get('setting/hotelfeature/manage', 'Admin\SettingsController@ManageHotelFeature');
    Route::get('setting/hotelfeature/edit/{id}', 'Admin\SettingsController@EditHotelFeature');
    Route::post('setting/hotelfeature/update/{id}', 'Admin\SettingsController@UpdateHotelFeature');
    //Room
    Route::get('room/create', 'Admin\RoomController@create');
    Route::Post('room/save','Admin\RoomController@save');
    Route::get('room/manage','Admin\RoomController@manage');
    Route::get('room/delete/{id}', 'Admin\RoomController@deleteRoom');
    Route::post('room/block', 'Admin\RoomController@block');
    Route::post('room/unblock', 'Admin\RoomController@unblock');
    Route::get('room/edit/{id}', 'Admin\RoomController@editRoom');
    Route::Post('room/removeimage', 'Admin\RoomController@RemoveRoomImage');
    Route::Post('room/update/{id}', 'Admin\RoomController@UpdateRoom');
    Route::get('room/pdf', 'Admin\RoomController@pdf');

    //Vouchers
    Route::get('vouchers/create', 'Admin\VouchersController@create');
    Route::Post('vouchers/save','Admin\VouchersController@save');
    Route::get('vouchers/manage','Admin\VouchersController@manage');
    Route::get('vouchers/edit/{id}', 'Admin\VouchersController@edit');
    Route::Post('vouchers/update/{id}', 'Admin\VouchersController@update');
    Route::get('vouchers/delete/{id}', 'Admin\VouchersController@delete');

    //Booking
    Route::get('booking/create', 'Admin\BookingController@create');
    Route::Post('booking/save','Admin\BookingController@save');
    Route::get('booking/manage', 'Admin\BookingController@manage');
    Route::get('booking/edit/{id}', 'Admin\BookingController@edit');
    Route::Post('booking/update/{id}', 'Admin\BookingController@update');
    Route::get('booking/delete/{id}', 'Admin\BookingController@delete');
    Route::Post('booking/hotel/roomlist','Admin\BookingController@getRoomListofHotel');

});


Route::get('calendar', 'CalendarController@index');

//Site
Route::get('/', 'SiteController@index');
Route::get('/available-hotels','SiteController@SearchHotels');
Route::post('/createbooking','SiteController@BookRoom');
Route::Post('/getLocations','SiteController@GetLocations');

//Hotel
Route::get('/hotel/{id}','HotelController@HotelDetails');
Route::get('/roomlist/{id}','HotelController@RoomList');
Route::get('/roomdetails/{id}','HotelController@RoomDetails');
Route::get('/hotels-list','HotelController@HotelsList');

// User
Route::get('login', 'UserController@login');
Route::get('logout', 'UserController@logout');
Route::Post('checkLogin', 'UserController@checkLogin');
Route::get('signup', 'UserController@signup');
Route::Post('register', 'UserController@create');
Route::get('verify', 'UserController@verifyAccount');
Route::get('profile','UserController@profile');
Route::get('editprofile','UserController@editProfile');
Route::Post('updateprofile','UserController@updateProfile');
Route::Post('changeprofilepic','UserController@changeProfilePic');
Route::get('changePassword','UserController@changePassword');
Route::Post('updatePassword','UserController@updatePassword');
Route::Post('citylist','UserController@getcity');

