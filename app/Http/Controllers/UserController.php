<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Locations;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\Users;

use Illuminate\Http\Request;

class UserController extends Controller
{


    public function login()
    {
        return view('user.site.login');
    }

    public function logout()
    {
        if ($this->islogin()) {
            Session::forget('user');
            return redirect('login');
        }
    }

    public function checkLogin()
    {
        $input = Input::all();
        $user = new Users();
        $result = $user->CheckLogin($input);

        if (empty($result->errorMessage)) {
            $userData['user'] = array('user_id' => $result->user_id, 'username' => $result->user_username, 'email' => $result->user_email);
            Session::put($userData);
            return redirect('profile');
        } else {
            return redirect()->back()->withInput()->withErrors("Wrong username or Password");

        }
    }

    public function signup()
    {
        return view('user.site.signup');
    }

    public function create(Request $request)
    {

        $rule = [
            'firstname' => 'required|min:3',
            'lastname' => 'required|min:3',
            'email' => 'required|email|unique:users,user_email',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ];

        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user = new Users();
        $user->user_firstname = Input::get('firstname');
        $user->user_lastname = Input::get('lastname');
        $user->user_email = Input::get('email');
        $user->user_password = md5(Input::get('password'));
        $user->user_status = "inactive";


        if ($user->save()) {
            $this->sendMail($user);
            Session::flash('regSuccess', 'resgistration Successfull');
//			echo json_encode(array('status'=>TRUE,'msg'=>'A mail is sent to confirm account'));
            return redirect('/');
        }
    }

    public function sendMail($data)
    {

        $verification_code = substr(sha1($data->user_email), 9, 13);

        if ($_SERVER['HTTP_HOST'] == 'localhost') {
            $sendUrl = "http://www.workspaceit.com/hotel_management/verify?email=" . $data->user_email . "&verifycode=" . $verification_code;
        } else {
            //$serverUrl = $exp[0] . '.' . $exp[1];
            $sendUrl = 'http://www.workspaceit.com/hotel_management/verify?email=' . $data->user_email . '&verifycode=' . $verification_code;
        }
        $to = $data->user_email;
        $subject = "Account Verification";
        $message = $data->user_lastname . ",";
        $message .= "Please click the following link: " . $sendUrl . " ... then follow the on-site instructions. ";
        $message .= "Thank you,";
        $from = "Admin@admin.com";
        $headers = "From:" . $from;
        mail($to, $subject, $message, $headers);
    }

    public function verifyAccount()
    {
        $email = Input::get('email');
        $verification_code = Input::get('verifycode');
        $code = substr(sha1($email), 9, 13);
        if ($verification_code == $code) {
            $user = Users::where('user_email', '=', $email)->first();
            $user->user_id = $user->user_id;
            $user->user_status = "active";
            if ($user->save()) {
                Session::flash('verified', 'Your Account has been successfully verified');
                return redirect('login');
            }
        }
    }

    public function profile()
    {

        if ($this->islogin()) {
            $user = Session::get('user');
            $userinfo = Users::with("Countries")->with("cities")->find($user['user_id']);
            return view('user.profile.profile', array('user' => $userinfo));
        } else {

            return redirect('login');
        }

    }

    public function editProfile()
    {
        if ($this->islogin()) {
            $user = Session::get('user');
            $userinfo = Users::find($user['user_id']);
            $locations = Locations::where('location_type','=','0')->get();

         $cities = Locations::where('parent_id','=',$userinfo->user_country)->get();
            return view('user.profile.editprofile', array('user' => $userinfo,'locations'=>$locations,'cities'=>$cities));
        } else {
            return redirect('login');
        }
    }

    public function updateProfile(Request $request)
    {
            $rule = [
                'firstname' => 'required|min:3',
                'lastname' => 'required|min:3',
                'address' => 'required',
                'city' => 'required',
                'phone' => 'required',
                'zip' => 'required',
                'country' => 'required',

            ];

            $validator = Validator::make($request->all(), $rule);

            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            $user = Session::get('user');
            $userinfo = Users::find($user['user_id']);
            $userinfo->user_firstname = $request->firstname;
            $userinfo->user_lastname = $request->lastname;
            $userinfo->user_country = $request->country;
            $userinfo->user_zip = $request->zip;
            $userinfo->user_phone = $request->phone;
            $userinfo->user_city = $request->city;
            $userinfo->user_address = $request->address;

            if ($userinfo->save()) {
                Session::flash('updated', 'Profile Info has been updated');
                return redirect('profile');
            }


    }

    public function changeProfilePic()
    {
        $file = Input::file('avatar');
        $fileCount = count($file);

        $user = Session::get('user');
        $userinfo = Users::find($user['user_id']);
        if ($fileCount != 0) {
            $destinationPath = 'public/images/users'; // upload path
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $imageName = $file->getClientOriginalName();
            $fileName = str_replace(' ', '', $imageName) . "_" . rand(0, 100) . '.' . $extension; // renameing image
            $file->move($destinationPath, $fileName);
            $userinfo->user_photo = $fileName;
            if ($userinfo->save()) {
                echo json_encode(array('status' => TRUE, 'msg' => 'photo updated'));
            }
        }


    }

    public function changePassword()
    {
        if ($this->islogin()) {

            return view('user.profile.changepassword');
        } else {
            return redirect('login');
        }
    }

    public function updatePassword(Request $request)
    {
        $user = Session::get('user');
        $userinfo = Users::find($user['user_id']);


        $rule = [
            'new_password' => 'required|min:6|confirmed|different:oldpassword',
            'new_password_confirmation' => 'required|min:6',
        ];

        $validator = Validator::make($request->all(), $rule);
        if (md5($request->oldpassword) != $userinfo->user_password) {
            return redirect()->back()->withInput()->withErrors("Wrong Old Password");
        } else if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $userinfo->user_password = md5($request->new_password);

        if ($userinfo->save()) {
            Session::flash('updated', 'Password has been updated');
            return redirect('profile');
        }
    }

    public function islogin()
    {
        $uid = Session::get('user');
        return ($uid) ? TRUE : FALSE;

    }

    public function getcity()
    {
        $id = Input::get('id');
        $locations=Locations::where('parent_id','=',$id)->where('location_type','=','1')->get();
        $html="<select name='city' class='form-control'>";

        foreach($locations as $location)
        {
            $html.="<option value='$location->location_id'>".$location->name."</option>";
        }
        $html.="</select>";

        return json_encode(array('status'=>true,'html'=>$html));


    }

}
