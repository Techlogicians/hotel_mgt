<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Session;
abstract class Controller extends BaseController
{

    use DispatchesCommands, ValidatesRequests;

    public function createSlug($data)
    {
        $title = preg_replace('/\%/', ' percentage', $data);
        $title = preg_replace('/\@/', ' at ', $title);
        $title = preg_replace('/\&/', ' and ', $title);
        $title = preg_replace('/\s[\s]+/', '-', $title);    // Strip off multiple spaces
        $title = preg_replace('/[\s\W]+/', '-', $title);    // Strip off spaces and non-alpha-numeric
        $title = preg_replace('/^[\-]+/', '', $title); // Strip off the starting hyphens
        $title = preg_replace('/[\-]+$/', '', $title); // // Strip off the ending hyphens
        $title = strtolower($title);
        return $title;
    }

    public function IsLoggedIn()
    {
        $uid = Session::get('user');
        return ($uid) ? TRUE : FALSE;
    }

}
