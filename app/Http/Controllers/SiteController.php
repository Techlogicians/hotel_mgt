<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Bookings;
use App\Models\Hotels;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Models\Settings;
use App\Models\Rooms;
use Psy\Util\Json;
use App\Models\Locations;


class SiteController extends Controller
{

    public function index()
    {
        $room_type = Settings::where('ads_key', '=', 'room_type')->get();
        return view('user.site.index', array('room_types' => $room_type));
    }

    public function SearchHotels()
    {

        $input = Input::all();
        $hotels = DB::table('hotel_rooms')
            ->Join('hotels',function($join){
                $join->on('hotel_rooms.hr_hotel_id', '=', 'hotels.hotel_id');
            })

            ->where('hotels.hotel_name','Like',"%".$input['Search']."%")
            ->Orwhere('hotels.hotel_city','Like',"%".$input['Search']."%")
            ->Orwhere('hotels.hotel_country','Like',"%".$input['Search']."%");
        if($input['room']!=null)
        {
            $hotels= $hotels->where('hotel_rooms.hr_room_type',"=",$input['room']);
        }
        if($input['checkin']!=null&&$input['checkout']!=null){
          //  $query = "select * from `bookings` where ('".$input["checkin"]."' < `booking_start_date` OR '".$input["checkin"]."'> `booking_END_date`) and( ('".$input["checkout"]."' < `booking_start_date` OR '".$input['checkout']."'> `booking_END_date`))";
           $query = "SELECT * FROM `bookings` WHERE (('".$input['checkin']."' BETWEEN `booking_start_date` AND`booking_end_date`) OR ('".$input['checkout']."' BETWEEN `booking_start_date` AND`booking_end_date` ))OR('".$input['checkin']."'<`booking_start_date` AND '".$input['checkout']."'>`booking_end_date`)";
            $data=DB::select(DB::raw($query));

            $ids = [];

            foreach($data as $key)
            {
             array_push($ids,$key->booking_room_id);
            }

           $hotels= $hotels->whereNotIn('hr_id',$ids) ;
        }
        $room_type = Settings::where('ads_key', '=', 'room_type')->get();
        return view('user.site.search',array("hotels"=>$hotels->distinct('hr_hotel_id')->get(),'room_types' => $room_type,'input'=>$input));
    }



    public  function BookRoom(){
       $Isavilable = $this->CheckRoomAvailability(Input::get('room'),Input::get('start'),Input::get('start'));
       if($Isavilable){
           $loggedIn  = $this->IsLoggedIn();
           if($loggedIn){
               $user = Session::get('user');

           }
           $booking = new Bookings();
           $booking->booking_user_id = $user['user_id'];
           $booking->booking_hotel_id = Input::get('hotel');
           $booking->booking_room_id = Input::get('room');
           $booking->booking_start_date= Input::get('start');
           $booking->booking_end_date = Input::get('end');
           $booking->booking_status="payment_due";

           if($booking->save()){
               echo json_encode(array('status' => TRUE, 'msg' => 'Room Booked Successfully'));
           }
       }
        else{
            echo json_encode(array('status' => FALSE, 'msg' => 'Sorry!Room is  not available'));
        }
    }
    private function CheckRoomAvailability($roomId,$cheekIn,$checkOut)
    {
        $query ="SELECT * FROM `bookings` WHERE `booking_room_id`= '".$roomId."' AND( (('".$cheekIn."' BETWEEN `booking_start_date` AND`booking_end_date`) OR ('".$checkOut."' BETWEEN `booking_start_date` AND`booking_end_date` ))OR('".$cheekIn."'<`booking_start_date` AND '".$checkOut."'>`booking_end_date`))";
        $data=DB::select(DB::raw($query));
        if(empty($data))
        {
            return true;
        }
        return false;
    }
    public function GetLocations()
    {
        $locations  = Locations::where('name',"Like","%".Input::get('location')."%")
            ->get();
        $newArray = array();
        foreach($locations as $loc)
        {
            array_push($newArray,$loc->name);
        }
        echo json_encode($newArray);
    }


}
