<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Bookings;
use App\Models\Hotels;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Models\Settings;
use App\Models\Rooms;
use Psy\Util\Json;
use Illuminate\Pagination\Paginator;


class HotelController extends Controller
{


    public function HotelDetails($id)
    {
        $hotel = Hotels::where('hotel_id', '=', $id)->where('hotel_status', '=', 'active')->get();
        $loggedIn = $this->IsLoggedIn();
        $user = null;
        if ($loggedIn) {
            $user = Session::get('user');

        }
        $otherRooms = Rooms::where('hr_hotel_id', '=', $id)->where('hr_room_status', '=', 'active')->get();
//        echo "<pre>";
//        print_r($otherRooms);
//        die;
        return view('user.hotel.hoteldetail', array('hotel' => $hotel[0], 'LoggedIn' => $loggedIn, "user" => $user, 'otherRooms' => $otherRooms));

    }

    public function RoomDetails($id)
    {
        $room = Rooms::find($id);
        $loggedIn = $this->IsLoggedIn();
        $user = null;
        if ($loggedIn) {
            $user = Session::get('user');

        }
        $otherRooms = Rooms::where('hr_hotel_id', '=', $room->hr_hotel_id)->whereRaw('hr_id!=' . $room->hr_id)->get();

        return view('user.hotel.roomdetail', array('room' => $room, 'LoggedIn' => $loggedIn, "user" => $user, 'otherRooms' => $otherRooms));

    }

    public function RoomList($id)
    {
        $rooms = Rooms::where('hr_hotel_id', '=', $id)
            ->where('hr_room_status', '=', 'active')
            ->get();
        $roomtypes = Settings::where('ads_key', '=', 'room_type')->get();
        return view('user.hotel.roomlist', array('roomlist' => $rooms, 'roomtypes' => $roomtypes));
    }

    public function HotelsList()
    {
        $type = null;
        if (Input::get('type') != '') {
            $hotels = Hotels::where('hotel_status', "=", "active")
                ->where('hotel_type', "=", Input::get('type'))->paginate(300);
            $type = Input::get('type');

        } else {
            $hotels = Hotels::where('hotel_status', "=", "active")->paginate(15);

        }

        $hotels->setPath(url('hotels-list'));

        return view('user.hotel.hotelslist', array('hotels' => $hotels,'type'=>$type));
    }


}
