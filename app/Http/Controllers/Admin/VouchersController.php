<?php
/**
 * Created by PhpStorm.
 * User: Mausum
 * Date: 7/8/2015
 * Time: 2:57 PM
 */

namespace App\Http\Controllers\Admin;

use App\Models\AdminOwners;
use App\Models\HotelAdmins;
use App\Models\Hotels;
use App\Models\Vouchers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class VouchersController extends MainController
{

    public function  Create()
    {
        if (!$this->_response['status']['isModerator']) {
            $hotels = null;
            if ($this->isSuperAdmin) {
                $hotels = Hotels::all();
            } else {

                $hotels = Hotels::where('hotel_owner_id', '=', $this->cuser['admin_uid'])->get();
            }
            return view('admin.voucher.create', array('response' => $this->_response, 'hotels' => $hotels));
        } else {
            echo "Not permitted !!";
        }
    }

    public function save()
    {
        $rule = [
            'voucher_code' => 'required',
            'voucher_description' => 'required|min:5',
            'voucher_discount_type' => 'required',
            'voucher_discount_amount' => 'required',
            'voucher_expire_date' => 'required'
        ];
        $input = Input::all();
        $validator = Validator::make($input, $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $item = new Vouchers();

        $item->voucher_code = $input['voucher_code'];
        $item->voucher_description = $input['voucher_description'];
        $item->voucher_discount_type = $input['voucher_discount_type'];
        $item->voucher_discount_amount = $input['voucher_discount_amount'];
        $item->voucher_expire_date = $input['voucher_expire_date'];
        $item->voucher_created_on = date("Y-m-d H:i:s");
        $item->voucher_updated_on = date("Y-m-d H:i:s");
        $item->voucher_created_by = $this->cuser['admin_uid'];
        $item->voucher_updated_by = $this->cuser['admin_uid'];
        if( $input['hotel']!="all"){
            $item->voucher_hotel_id = $input['hotel'];
        }


        if ($item->save()) {
            return redirect('admin/vouchers/manage');
        }
    }

    public function manage()
    {
        $data = null;
        if ($this->isSuperAdmin) {
            $data = Vouchers::with('hotels')->get();
        } else {

            $data = Vouchers::with('hotels')->where('voucher_created_by', '=', $this->cuser['admin_uid'])->get();
        }

        return view('admin.voucher.manage', array('response' => $this->_response, 'vouchers' => $data));
    }

    public function edit($id)
    {
        $voucher = Vouchers::find($id);
        if ($this->isSuperAdmin || $voucher->voucher_created_by == $this->cuser['admin_uid']) {
            $hotels = null;
            if ($this->isSuperAdmin) {
                $hotels = Hotels::all();
            } else {

                $hotels = Hotels::where('hotel_owner_id', '=', $this->cuser['admin_uid'])->get();
            }
            return view('admin.voucher.edit', array('response' => $this->_response, 'voucher' => $voucher, 'hotels' => $hotels));
        }
    }

    public function update($id)
    {
        $item = Vouchers::find($id);
        if ($this->isSuperAdmin || $item->voucher_created_by == $this->cuser['admin_uid']) {
            $rule = [
                'voucher_code' => 'required',
                'voucher_description' => 'required|min:5',
                'voucher_discount_type' => 'required',
                'voucher_discount_amount' => 'required',
                'voucher_expire_date' => 'required'
            ];
            $input = Input::all();
            $validator = Validator::make($input, $rule);

            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }


            $item->voucher_code = $input['voucher_code'];
            $item->voucher_description = $input['voucher_description'];
            $item->voucher_discount_type = $input['voucher_discount_type'];
            $item->voucher_discount_amount = $input['voucher_discount_amount'];
            $item->voucher_expire_date = $input['voucher_expire_date'];
            $item->voucher_created_on = date("Y-m-d H:i:s");
            $item->voucher_updated_on = date("Y-m-d H:i:s");
            $item->voucher_created_by = $this->cuser['admin_uid'];
            $item->voucher_updated_by = $this->cuser['admin_uid'];

            if( $input['hotel']!="all"){
                $item->voucher_hotel_id = $input['hotel'];
            }
            if ($item->save()) {
                return redirect('admin/vouchers/manage');
            }
        }

    }

    public function delete($id)
    {
        $item = Vouchers::find($id);
        if   ($this->isSuperAdmin || $item->voucher_created_by == $this->cuser['admin_uid']) {
            if ($item->delete()) {
                return redirect()->back()->with('Hotel Delete Successful');
            }
        }
    }

}