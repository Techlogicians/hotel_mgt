<?php
/**
 * Created by PhpStorm.
 * User: Mausum
 * Date: 7/8/2015
 * Time: 2:57 PM
 */

namespace App\Http\Controllers\Admin;

use App\Models\AdminOwners;
use App\Models\Bookings;
use App\Models\HotelAdmins;
use App\Models\Hotels;
use App\Models\Rooms;
use App\Models\Users;
use App\Models\Vouchers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class BookingController extends MainController
{

    public function  Create()
    {
        if (!$this->_response['status']['isModerator']) {
            $hotels = null;
            $users = Users::all();
            if ($this->isSuperAdmin) {
                $hotels = Hotels::all();

            } else {

                $hotels = Hotels::where('hotel_owner_id', '=', $this->cuser['admin_uid'])->get();
            }
            return view('admin.booking.create', array('response' => $this->_response, 'hotels' => $hotels,'users'=>$users));
        } else {
            echo "Not permitted !!";
        }
    }

    public function save()
    {
        $rule = [
            'booked_by' => 'required',
            'hotel_name' => 'required',
            'room_name' => 'required',
            'booking_start_date' => 'required',
            'booking_end_date' => 'required',
            'status'=>'required'
        ];
        $input = Input::all();
        $validator = Validator::make($input, $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $booking = new Bookings();
        $booking->booking_user_id = Input::get('booked_by');
        $booking->booking_hotel_id = Input::get('hotel_name');
        $booking->booking_room_id = Input::get('room_name');
        $booking->booking_start_date= Input::get('booking_start_date');
        $booking->booking_end_date = Input::get('booking_end_date');
        $booking->booking_status=Input::get('status');

        $booking->booking_created_by = $this->cuser['admin_uid'];
        $booking->booking_created_on = date("Y-m-d H:i:s");
        $booking->booking_updated_by = $this->cuser['admin_uid'];
        $booking->booking_updated_on =date("Y-m-d H:i:s");


        if($booking->save())
        {
            return redirect('admin/booking/manage');
        }
    }

    public function manage()
    {
        $data = null;
        if ($this->isSuperAdmin) {
            $data = Bookings::with('hotels','users','hotel_rooms')->get();
        } else {
            $data = Bookings::with('hotels','users','hotel_rooms')->where('booking_created_by', '=', $this->cuser['admin_uid'])->get();
        }
        return view('admin.booking.manage', array('response' => $this->_response, 'bookings' => $data));
    }

    public function edit($id)
    {
        $booking = Bookings::find($id);

        if ($this->isSuperAdmin || $booking->booking_created_by == $this->cuser['admin_uid']) {
            $hotels = null;
            $users = Users::all();
            $room = Rooms::find($booking->booking_room_id);
            if ($this->isSuperAdmin) {
                $hotels = Hotels::all();
            } else {

                $hotels = Hotels::where('hotel_owner_id', '=', $this->cuser['admin_uid'])->get();
            }
            return view('admin.booking.edit', array('response' => $this->_response, 'booking' => $booking, 'hotels' => $hotels,'users'=>$users,'room'=>$room));
        }
    }

    public function update($id)
    {
        $booking = Bookings::find($id);
        if ($this->isSuperAdmin || $booking->voucher_created_by == $this->cuser['admin_uid']) {
            $rule = [
                'booked_by' => 'required',
                'hotel_name' => 'required',
                'room_name' => 'required',
                'booking_start_date' => 'required',
                'booking_end_date' => 'required',
                'status'=>'required'
            ];
            $input = Input::all();
            $validator = Validator::make($input, $rule);

            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }


            $booking->booking_user_id = Input::get('booked_by');
            $booking->booking_hotel_id = Input::get('hotel_name');
            $booking->booking_room_id = Input::get('room_name');
            $booking->booking_start_date= Input::get('booking_start_date');
            $booking->booking_end_date = Input::get('booking_end_date');
            $booking->booking_status=Input::get('status');

            $booking->booking_created_by = $this->cuser['admin_uid'];
            $booking->booking_created_on = date("Y-m-d H:i:s");
            $booking->booking_updated_by = $this->cuser['admin_uid'];
            $booking->booking_updated_on =date("Y-m-d H:i:s");
            if($booking->save())
            {
               return redirect('admin/booking/manage');
            }
        }

    }

    public function delete($id)
    {
        $item = Bookings::find($id);
        if   ($this->isSuperAdmin || $item->booking_created_by == $this->cuser['admin_uid']) {
            if ($item->delete()) {
                return redirect()->back()->with('Hotel Delete Successful');
            }
        }
    }
    public function getRoomListofHotel()
    {
        $id = Input::get('id');
        $rooms=Rooms::where('hr_hotel_id','=',$id)->get();
        $html="<select name='room_id' class='form-control'>";

        foreach($rooms as $room)
        {
            $html.="<option value='$room->hr_id'>".$room->hr_room_title."</option>";
        }
        $html.="</select>";

        return json_encode(array('status'=>true,'html'=>$html));


    }

}