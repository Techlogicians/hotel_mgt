<?php

namespace App\Http\Controllers\Admin;

use App\Models\AdminOwners;
use App\Models\HotelAdmins;
use App\Models\Hotels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use App\Models\Settings;

class HotelController extends MainController {

    public function create() {
        if (!$this->_response['status']['isModerator']) {
            $owner = new AdminOwners();
            $data = $owner->getAllOwner();
            $hotel_feature = Settings::where('ads_key', '=', 'hotel_feature')->get();
            return view('admin.hotel.create', array('response' => $this->_response, 'owner' => $data,'hotel_feature'=>$hotel_feature));
        } else {
            echo "Not permitted !!";
        }
    }

    public function SaveHotel(Request $request) {
        $rule = [
            'hotel_name' => 'required',
            'hotel_description' => 'required|min:5',
            'hotel_banner_image' => 'required',
            'hotel_type' => 'required',
            'hotel_category' => 'required',
            'hotel_address' => 'required',
            'hotel_city' => 'required',
            'hotel_zip' => 'required',
            'hotel_country' => 'required',
            'hotel_facilities' => 'required',
            'hotel_facebook' => 'required',
            'hotel_twitter' => 'required',
        ];

        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $hotel = new Hotels();
        $hotel->hotel_name = $request->hotel_name;
        $hotel->hotel_description = $request->hotel_description;
        if ($this->_response['status']['isHotelOwner']) {
            $hotel->hotel_owner_id = $this->cuser['admin_uid'];
        } else {
            $hotel->hotel_owner_id = $request->hotel_owner_id;
        }

        $hotel->hotel_type = $request->hotel_type;
        $hotel->hotel_category = $request->hotel_category;
        $hotel->hotel_address = $request->hotel_address;
        $hotel->hotel_city = $request->hotel_city;
        $hotel->hotel_zip = $request->hotel_zip;
        $hotel->hotel_country = $request->hotel_country;
        $hotel->hotel_facilities = json_encode($request->hotel_facilities);
        $hotel->hotel_website = $request->hotel_website;
        $hotel->hotel_facebook = $request->hotel_facebook;
        $hotel->hotel_twitter = $request->hotel_twitter;
        $hotel->hotel_created_by = $this->cuser['admin_uid'];
        $hotel->hotel_updated_by = $this->cuser['admin_uid'];
        $hotel->hotel_created_on = date("Y-m-d H:i:s");
        $hotel->hotel_update_on = date("Y-m-d H:i:s");
        $hotel->hotel_status = "active";
        $file = Input::file('hotel_banner_image');
        $fileCount = count($file);


        if ($fileCount != 0) {
            $destinationPath = 'public/images/hotels'; // upload path
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $fileName = str_replace(' ', '', $hotel->hotel_name) . "_" . rand(0, 100) . '.' . $extension;
            $file->move($destinationPath, $fileName);
            $hotel->hotel_banner_image = $fileName;
        }
        if ($hotel->save()) {
            //$this->sendMail($user);
            return redirect('admin/hotel/manage');
        }
    }

    public function manage() {
        $data = null;
        if ($this->_response['status']['isSuperAdmin']) {
            $data = Hotels::all();

        }else if($this->_response['status']['isHotelOwner']) {
            $owner_id = $this->cuser['admin_uid'];
            $data = Hotels::where('hotel_owner_id', '=', $owner_id)->get();
        }

        return view('admin.hotel.manage', array('response' => $this->_response, 'hotels' => $data));
    }

    public function editHotel($id) {
        $hotel = Hotels::find($id);
        if ($this->isSuperAdmin || $hotel->hotel_owner_id == $this->cuser['admin_uid']) {
            $hotel_feature = Settings::where('ads_key', '=', 'hotel_feature')->get();
            return view('admin.hotel.edit', array('response' => $this->_response, 'hotel' => $hotel,'hotel_feature'=>$hotel_feature));
        }
    }

    public function updateHotel($id) {
        $rule = [
            'hotel_name' => 'required',
            'hotel_description' => 'required|min:5',
            'hotel_type' => 'required',
            'hotel_category' => 'required',
            'hotel_address' => 'required',
            'hotel_city' => 'required',
            'hotel_zip' => 'required',
            'hotel_country' => 'required',
            'hotel_facilities' => 'required',
        ];
        $input = Input::all();
        $validator = Validator::make($input, $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $hotel = Hotels::find($id);
        if ($this->isSuperAdmin || $hotel->hotel_owner_id == $this->cuser['admin_uid']) {
            $hotel->hotel_name = $input['hotel_name'];
            $hotel->hotel_description = $input['hotel_description'];
            $hotel->hotel_type = $input['hotel_type'];
            $hotel->hotel_category = $input['hotel_category'];
            $hotel->hotel_address = $input['hotel_address'];
            $hotel->hotel_city = $input['hotel_city'];
            $hotel->hotel_zip = $input['hotel_zip'];
            $hotel->hotel_country = $input['hotel_country'];
            $hotel->hotel_facilities = json_encode($input['hotel_facilities']);
            $hotel->hotel_website = $input['hotel_website'];
            $hotel->hotel_facebook = $input['hotel_facebook'];
            $hotel->hotel_twitter = $input['hotel_twitter'];
            $hotel->hotel_updated_by = $this->cuser['admin_uid'];
            $hotel->hotel_update_on = date("Y-m-d H:i:s");
            $hotel->hotel_status = $input['hotel_status'];
            $file = Input::file('hotel_banner_image');
            $fileCount = count($file);


            if ($fileCount != 0) {
                $destinationPath = 'public/images/hotels'; // upload path
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $fileName = str_replace(' ', '', $hotel->hotel_name) . "_" . rand(0, 100) . '.' . $extension;
                $file->move($destinationPath, $fileName);
                $hotel->hotel_banner_image = $fileName;
            }
            if ($hotel->save()) {
                //$this->sendMail($user);
                return redirect('admin/hotel/manage');
            }
        }
    }

    public function showHotelDetails($id) {
        $hotel = Hotels::find($id);
        $owner = AdminOwners:: find($hotel->hotel_owner_id);
        if ($this->isSuperAdmin || $hotel->hotel_owner_id == $this->cuser['admin_uid']) {
            return view('admin.hotel.details', array('response' => $this->_response, 'hotel' => $hotel, 'owner' => $owner));
        }
    }

    public function deleteHotel($id) {
        $hotel = Hotels::find($id);
        if ($this->isSuperAdmin || $hotel->hotel_owner_id == $this->cuser['admin_uid']) {
            if ($hotel->delete()) {
                return redirect()->back()->with('Hotel Delete Successful');
            }
        }
    }
    public function block() {
        $id = Input::get('id');

        $hotel = Hotels::find($id);
        $hotel->hotel_status = "inactive";
        if ($hotel->save()) {
            echo json_encode(array('status' => true));
        }
    }

    public function unblock() {
        $id = Input::get('id');
        $hotel = Hotels::find($id);
        $hotel->hotel_status = "active";
        if ($hotel->save()) {
            echo json_encode(array('status' => true));
        }
    }
}
    
