<?php

namespace App\Http\Controllers\Admin;

use \App\Http\Controllers\Controller;
use \Illuminate\Support\Facades\Session;


class MainController extends  Controller{

    public $_response;
    public $islogin;
    public $cuser;
    public $isSuperAdmin;
    public $isHotelAdmin;
    public $isHotelOwner;
    public $isModerator;

    public function __construct() {
        $this->islogin = $this->login();
        $this->cuser = $this->getUserInfo();
        $this->isSuperAdmin = $this->superAdmin();
        $this->isHotelAdmin = $this->hotelAdmin();
        $this->isHotelOwner = $this->hotelOwner();
        $this->isModerator = $this->moderator();
        $this->_response['status']['islogin'] = $this->islogin;
        $this->_response['status']['isSuperAdmin'] = $this->isSuperAdmin;
        $this->_response['status']['isHotelAdmin'] = $this->isHotelAdmin;
        $this->_response['status']['isHotelOwner'] = $this->isHotelOwner;
        $this->_response['status']['isModerator'] = $this->isModerator;
        $this->_response['status']['cuser'] = $this->cuser;
    }

    public function login() {
        $uid = Session::get('adminUser');
        return ($uid) ? TRUE : FALSE;
    }
    public function getUserInfo() {
        $userInfo = Session::get('adminUser');
        return $userInfo;
    }
    public function superAdmin() {
        $user = Session::get('adminUser');
        return ($user['type'] == 'admin') ? TRUE : FALSE;
    }
    public function hotelAdmin() {
        $user = Session::get('adminUser');
        return ($user['type'] == 'cadmin') ? TRUE : FALSE;
    }
    public function hotelOwner() {
        $user = Session::get('adminUser');
        return ($user['type'] == 'owner') ? TRUE : FALSE;
    }
    public function moderator() {
        $user = Session::get('adminUser');
        return ($user['type'] == 'mod') ? TRUE : FALSE;
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
}
