<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use App\Models\AdminOwners;
use App\Models\HotelAdmins;
use App\Models\Hotels;
use Illuminate\Support\Facades\DB;

class HomeController extends MainController {

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
//        if ($this->islogin) {
//            return view('admin/site/index', array('response' => $this->_response));
//        }else{
//            return view('admin/site/login');
//        }
    }

    public function viewRegister() {
        if ($this->islogin) {
            if ($this->isSuperAdmin || $this->isModerator) {
                //Session::flush();
                return view('admin/site/register', array('response' => $this->_response));
            }
        } else {
            
        }
    }

    public function viewLogin() {
        //var_dump($this->islogin);die;
        if ($this->islogin) {
            return view('admin/site/index', array('response' => $this->_response));
        } else {
            return view('admin/site/login');
        }
    }

    public function viewChangePassword() {
        if ($this->islogin) {
            return view('admin/site/changePassword', array('response' => $this->_response));
        }
    }

    public function changePassword(Request $request) {
        if ($this->islogin) {
            $v = Validator::make($request->all(), [
                'new_password' => 'required|min:6',
            ]);
            if ($v->fails()) {
                return redirect()->back()->withErrors($v->messages());
            } else {
                $input = Input::all();
                $helper = new AdminOwners();
                $newPassword = $helper->updatePassword($this->cuser['admin_uid'], $input);
//                dd($newPassword);
                if (isset($newPassword->errormessage)) {
//                echo json_encode(array('status' => FALSE, 'msg' => $newPassword->errormessage));
                    return redirect()->back()->withErrors($newPassword->errormessage);
                } else {
                    return redirect('admin/');
                }
            }
        }
    }

    public function adminRegister(Request $request) {
        if ($this->islogin) {
            $v = Validator::make($request->all(), [
                        'ao_email' => 'required|email|unique:admin_owners',
                        'password' => 'required|min:6',
                        'con_password' => 'required|same:password',
                        'ao_username' => 'required|unique:admin_owners',
                        'admin_type' => ($this->isModerator) ? 'in:owner,cadmin' : (($this->isHotelOwner) ? 'in:cadmin' : 'in:admin,mod,owner,cadmin'),
            ]);
            if ($v->fails()) {
                return redirect()->back()->withErrors($v->messages());
            } else {
                $helper = new AdminOwners();
                $adminUser = $helper->registerAdmin($request, $this->cuser);
                if (!empty($adminUser)) {
                    //$this->sendMail($adminUser);
                    return json_encode(array('status'=>TRUE));
                }
            }
        } else {
            
        }
    }

    public function checkLogin() {
        $input = Input::all();
        $helper = new AdminOwners();
        $result = $helper->checkLogin($input);
        if (@$result->errorMessage) {
            return redirect()->back()->withErrors($result->errorMessage);
        } else {
            $userData['adminUser'] = array('admin_uid' => $result['ao_id'], 'username' => $result['ao_username'], 'email' => $result['ao_email'], 'type' => $result['ao_type']);
            Session::put($userData);
            //echo json_encode(array('status' => TRUE, 'msg' => 'Login Successful'));
            return redirect('admin/');
        }
    }

    public function logout() {
        if ($this->islogin) {
            Session::forget('adminUser');
            return redirect('admin/');
        }
    }

    public function sendMail($data) {

        $verification_code = substr(sha1($data->ao_email), 9, 13);

        if ($_SERVER['HTTP_HOST'] == 'localhost') {
            $sendUrl = "http://localhost/hotel_management/verify?email=" . $data->ao_email . "&verifycode=" . $verification_code;
        } else {
            //$serverUrl = $exp[0] . '.' . $exp[1];
            $sendUrl = 'http://localhost/hotel_management/verify?email=' . $data->ao_email . '&verifycode=' . $verification_code;
        }
        $to = $data->ao_email;
        $subject = "Account Verification";
        $message = $data->ao_username . ",";
        $message .= "Please click the following link: " . $sendUrl . " ... then follow the on-site instructions. ";
        $message .= "Thank you,";
        $from = "Admin@admin.com";
        $headers = "From:" . $from;
        mail($to, $subject, $message, $headers);
    }

    public function verifyAccount() {
        $email = Input::get('email');
        $verification_code = Input::get('verifycode');
        $code = substr(sha1($email), 9, 13);
        if ($verification_code == $code) {
            $user = Users::where('ao_email', '=', $email)->first();
            if (!empty($user)) {
                $user->ao_status = "active";
            }
            $user->save();
            echo "verified";
        }
    }

    public function createHotelAdmin() {
        if (!$this->isHotelAdmin) {
            if ($this->isHotelOwner) {
                $hotels = Hotels::where('hotel_owner_id', '=', $this->cuser['admin_uid'])->get();
            } else {
                $hotels = Hotels::all();
            }
//            echo "<pre>";print_r($hotels);die;
            if (count($hotels)) {
                return view('admin/hotel/createHotelAdmin', array('response' => $this->_response, 'hotels' => $hotels));
            } else {
                return view('admin/hotel/createHotelAdmin', array('response' => $this->_response));
            }
        }
    }

    public function saveHotelAdmin(Request $request) {
        if ($this->islogin) {
            $v = Validator::make($request->all(), [
                        'ao_email' => 'required|email|unique:admin_owners',
                        'password' => 'required|min:6',
                        'con_password' => 'required|same:password',
                        'ao_username' => 'required|unique:admin_owners',
            ]);
            if ($v->fails()) {
                return redirect()->back()->withErrors($v->messages());
            } else {
                $request->admin_type = "cadmin";
                $hotelAdmin = new AdminOwners();
                $adminUser = $hotelAdmin->registerAdmin($request, $this->cuser);
                if (!empty($adminUser)) {
                    if ($request->hotel_id != "") {
                        $hotel = Hotels::find($request->hotel_id);
                        if (!empty($hotel)) {
                            $assignHotel = new HotelAdmins();
                            $relation = $assignHotel->assignToHotel($hotel->hotel_id, $adminUser, $this->cuser);
                        }
                    }
                    //$this->sendMail($adminUser);
                    return redirect('admin/');
                }
            }
        }
    }

    public function manageAdmin() {
        if (!$this->isHotelAdmin) {
            $data = null;
            if ($this->isHotelOwner) {
//                $admins = AdminOwners::with('hotels.hotelAdmins.AdminOwner')->find($this->cuser['admin_uid']);
//                $adminKey = DB::table('admin_owners')
//                        ->join('hotels', 'admin_owners.ao_id', '=', 'hotels.hotel_owner_id')
//                        ->join('hotel_admins', 'hotels.hotel_id', '=', 'hotel_admins.ha_hotel_id')
//                        ->where('hotels.hotel_owner_id', '=', $this->cuser['admin_uid'])
//                        ->select('hotel_admins.ha_ao_id')
//                        ->get();
//                $admins = array();
//                foreach ($adminKey as $key => $data) {
//                    $admins[] = AdminOwners::find($data->ha_ao_id);
//                }
                $admins = $this->getHotelAdmins();
            } else {
                $admins = AdminOwners::with('hotels')->get();
            }
            return view('admin.site.manageAdmin', array('response' => $this->_response, 'admins' => $admins));
        }
    }

    public function editAdmin($id) {
        if (!$this->isHotelAdmin) {
            if ($this->isHotelOwner) {
                $admins = $this->getHotelAdmins();
//                echo "<pre>";print_r($admin);die;
                if (count($admins)) {
                    $hasHotel = $this->hasHotel($admins, $id);
                    if ($hasHotel) {
                        $admin = AdminOwners::find($id);
                        $hotels = HotelAdmins::with('ownersHotel')->where('ha_ao_id', '=', $id)->first();
                        if (!empty($hotels->ownersHotel)) {
                            $hotels->owner = AdminOwners::with('hotels')->find($hotels->ownersHotel->hotel_owner_id);
                            return view('admin.site.editAdmin', array('response' => $this->_response, 'admin' => $admin, 'hotels' => $hotels->ownersHotel, 'owner' => $hotels->owner, 'hotelAdmin' => $hotels));
                        }
                    } else {
                        return redirect('admin/');
                    }
                }
            } else {
                $admin = AdminOwners::with('hotels')->find($id);
                $hotels = HotelAdmins::with('ownersHotel')->where('ha_ao_id', '=', $id)->first();
                if (!empty($hotels->ownersHotel)) {
                    $hotels->owner = AdminOwners::with('hotels')->find($hotels->ownersHotel->hotel_owner_id);
                    return view('admin.site.editAdmin', array('response' => $this->_response, 'admin' => $admin, 'hotels' => $hotels->ownersHotel, 'owner' => $hotels->owner, 'hotelAdmin' => $hotels));
                } else {
                    return view('admin.site.editAdmin', array('response' => $this->_response, 'admin' => $admin));
                }
            }
        }
    }

    public function updateAdmin($id) {
        if (!$this->isHotelAdmin) {
            $rule = [
                'ao_username' => 'required',
            ];
            $input = Input::all();
            $validator = Validator::make($input, $rule);

            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            $isUserNameExist = AdminOwners::where('ao_username', '=', $input['ao_username'])->where('ao_id', '!=', $id)->get();
            if (count($isUserNameExist)) {
                return redirect()->back()->withInput()->withErrors("UserName is Already Taken");
            } else {
                $adminUser = AdminOwners::find($id);
                if (!empty($adminUser)) {
                    $hasHotel = false;
                    if ($this->isHotelOwner) {
                        $admins = $this->getHotelAdmins();
                        if (count($admins)) {
                            $hasHotel = $this->hasHotel($admins, $id);
                        }
                    }
                    if ($this->isSuperAdmin || $this->isModerator || $hasHotel) {
                        $adminUser->ao_firstname = $input['fname'];
                        $adminUser->ao_lastname = $input['lname'];
                        $adminUser->ao_username = $input['ao_username'];
                        $adminUser->ao_address = $input['address'];
                        $adminUser->ao_country = $input['country'];
                        $adminUser->ao_city = $input['city'];
                        $adminUser->ao_zip = $input['zip'];
                        $adminUser->ao_type = $input['admin_type'];
                        $adminUser->ao_status = $input['admin_status'];
                        $adminUser->ao_update_by = $this->cuser['admin_uid'];
                        $adminUser->ao_updated_at = date("Y-m-d H:i:s");
                        if ($adminUser->save()) {
                            if ($adminUser->ao_type == "cadmin") {
                                $hotels = HotelAdmins::where('ha_ao_id', '=', $id)->first();
                                if ($input['hotel_id'] != $hotels->ha_hotel_id) {
                                    $assignHotel = new HotelAdmins();
                                    $relation = $assignHotel->assignToHotel($input['hotel_id'], $adminUser, $this->cuser);
                                }
                            }
                            //$this->sendMail($adminUser);
                            return redirect('admin/manage');
                        }
                    }
                }
            }
        }
    }

    public function showAdminDetails($id) {
        if (!$this->isHotelAdmin) {
            if ($this->isHotelOwner) {
                $admins = $this->getHotelAdmins();
//                echo "<pre>";print_r($admin);die;
                if (count($admins)) {
                    $hasHotel = $this->hasHotel($admins, $id);
                    if ($hasHotel) {
                        $admin = AdminOwners::find($id);
                        $hotels = HotelAdmins::with('ownersHotel')->where('ha_ao_id', '=', $id)->first();
                        if (!empty($hotels->ownersHotel)) {
                            $hotels->owner = AdminOwners::find($hotels->ownersHotel->hotel_owner_id);
                            return view('admin.site.adminDetails', array('response' => $this->_response, 'admin' => $admin, 'hotels' => $hotels->ownersHotel, 'owner' => $hotels->owner));
                        }
                    } else {
                        return redirect('admin/');
                    }
                }
            } else {
                $admin = AdminOwners::with('hotels')->find($id);
                $hotels = HotelAdmins::with('ownersHotel')->where('ha_ao_id', '=', $id)->first();
                if (!empty($hotels->ownersHotel)) {
                    $hotels->owner = AdminOwners::find($hotels->ownersHotel->hotel_owner_id);
                    return view('admin.site.adminDetails', array('response' => $this->_response, 'admin' => $admin, 'hotels' => $hotels->ownersHotel, 'owner' => $hotels->owner));
                } else {
                    return view('admin.site.adminDetails', array('response' => $this->_response, 'admin' => $admin));
                }
            }
        }
    }

    public function deleteAdmin($id)
    {
        if (!$this->isHotelAdmin) {
            if ($this->isHotelOwner) {
                $admins = $this->getHotelAdmins();
//                echo "<pre>";print_r($admin);die;
                if (count($admins)) {
                    $hasHotel = $this->hasHotel($admins, $id);
                    if ($hasHotel) {
                        $admin = AdminOwners::find($id);
                        if ($admin->delete()) {
                            $hotelAdmin = HotelAdmins::where('ha_ao_id', '=', $id)->get();
                            if (count($hotelAdmin)) {
                                foreach ($hotelAdmin as $key => $hadmin) {
                                    $hotelAdmin[$key]->delete();
                                }
                            }
                            return redirect()->back()->with('Admin Delete Successful');
                        }
                    } else {
                        return redirect()->back()->withErrors('Admin Cannot be delete');
                    }
                }
            } else {

                $admin = AdminOwners::find($id);
                if ($admin->delete()) {
                    return redirect()->back()->with('Admin Delete Successful');
                }
            }
           // echo "ok";
        }
    }

    public function block() {
        $id = Input::get('id');

        $hotel = AdminOwners::find($id);
        $hotel->ao_status = "inactive";
        if ($hotel->save()) {
            echo json_encode(array('status' => true));
        }
    }

    public function unblock() {
        $id = Input::get('id');
        $hotel = AdminOwners::find($id);
        $hotel->ao_status = "active";
        if ($hotel->save()) {
            echo json_encode(array('status' => true));
        }
    }

    public function hasHotel($admins, $id) {
        $status = False;
        foreach ($admins as $admin) {
            if ($admin->ao_id == $id) {
                $status = True;
            }
        }
        return $status;
    }

    public function getHotelAdmins() {
        $admins = DB::table('hotel_admins')
                ->join('hotels', 'hotel_admins.ha_hotel_id', '=', 'hotels.hotel_id')
                ->join('admin_owners', 'hotel_admins.ha_ao_id', '=', 'admin_owners.ao_id')
                ->where('hotels.hotel_owner_id', '=', $this->cuser['admin_uid'])
                ->select('admin_owners.*')
                ->get();
        return $admins;
    }

}
