<?php
/**
 * Created by PhpStorm.
 * User: Imtiaz
 * Date: 7/6/2015
 * Time: 2:09 PM
 */

namespace App\Http\Controllers\Admin;


use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class SettingsController extends MainController {

    public function createRoomType()
    {
       return view('admin.settings.createroomtype',array('response' => $this->_response));
    }

    public function saveRoomType(Request $request)
    {
        $rule = [
            'room_type' => 'required',
            ];
        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $setting = new Settings();
        $setting->ads_key = "room_type";
        $setting->ads_title="Room Type";
        $setting->ads_value = $request->room_type;

        if($setting->save())
        {
            return redirect('admin/setting/roomtype/manage');
        }


    }
    public function editRoomType($id)
    {
        $setting = Settings::find($id);
        return view('admin.settings.editroomtype',array('response' => $this->_response,'setting' => $setting));
    }

    public function updateRoomType($id)
    {
        $rule = [
            'room_type' => 'required',
        ];
        $input = Input::all();
        $validator = Validator::make($input, $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $setting = Settings::find($id);
        $setting->ads_key = "room_type";
        $setting->ads_title="Room Type";
        $setting->ads_value = $input['room_type'];

        if($setting->save())
        {

            return redirect('admin/setting/roomtype/manage');
        }


    }
    public function manageRoomtype()
    {
        $setting = Settings::where('ads_key','=','room_type')->get();
        return view('admin.settings.manageroomtype', array('response' => $this->_response, 'roomtypes' => $setting));
    }
    public function deleteSetting($id) {
           $hotel = Settings::find($id);

            if ($hotel->delete()) {
                return redirect()->back()->with('Hotel Delete Successful');
            }

    }
     // Room Features
    public function createRoomFeature()
    {
        return view('admin.settings.createroomfeature',array('response' => $this->_response));
    }

    public function saveRoomFeature(Request $request)
    {
        $rule = [
            'room_feature' => 'required',
        ];
        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $setting = new Settings();
        $setting->ads_key = "room_feature";
        $setting->ads_title="Room Feature";
        $setting->ads_value = $request->room_feature;

        if($setting->save())
        {
            return redirect('admin/setting/roomfeature/manage');
        }


    }
    public function editRoomFeature($id)
    {
        $setting = Settings::find($id);
        return view('admin.settings.editroomfeature',array('response' => $this->_response,'setting' => $setting));
    }

    public function updateRoomFeature($id)
    {
        $rule = [
            'room_feature' => 'required',
        ];
        $input = Input::all();
        $validator = Validator::make($input, $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $setting = Settings::find($id);
        $setting->ads_key = "room_feature";
        $setting->ads_title="Room Type";
        $setting->ads_value = $input['room_feature'];

        if($setting->save())
        {

            return redirect('admin/setting/roomfeature/manage');
        }


    }
    public function manageRoomFeature()
    {
        $setting = Settings::where('ads_key','=','room_feature')->get();
        return view('admin.settings.manageroomfeature', array('response' => $this->_response, 'roomfeatures' => $setting));
    }

    //Hotels Features
    public function CreateHotelFeature()
    {
        return view('admin.settings.createhotelfeature',array('response' => $this->_response));
    }

    public function SaveHotelFeature(Request $request)
    {
        $rule = [
            'room_feature' => 'required',
        ];
        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $setting = new Settings();
        $setting->ads_key = "hotel_feature";
        $setting->ads_title="Hotel Feature";
        $setting->ads_value = $request->room_feature;

        if($setting->save())
        {
            return redirect('admin/setting/hotelfeature/manage');
        }


    }
    public function ManageHotelFeature()
    {
        $setting = Settings::where('ads_key','=','hotel_feature')->get();
        return view('admin.settings.managehotelfeature', array('response' => $this->_response, 'hotelfeatures' => $setting));
    }
    public function EditHotelFeature($id)
    {
        $setting = Settings::find($id);
        return view('admin.settings.edithotelfeature',array('response' => $this->_response,'setting' => $setting));
    }

    public function UpdateHotelFeature($id)
    {
        $rule = [
            'room_feature' => 'required',
        ];
        $input = Input::all();
        $validator = Validator::make($input, $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $setting = Settings::find($id);
        $setting->ads_value = $input['room_feature'];

        if($setting->save())
        {

            return redirect('admin/setting/hotelfeature/manage');
        }


    }

}