<?php

/**
 * Created by PhpStorm.
 * User: Imtiaz
 * Date: 7/6/2015
 * Time: 2:09 PM
 */

namespace App\Http\Controllers\Admin;

use App\Models\Settings;
use App\Models\Rooms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\Hotels;
use App\Models\AdminOwners;
use App\Models\HotelAdmins;
use Barryvdh\DomPDF\PDF;

class RoomController extends MainController
{

    public function create()
    {
        $room_type = Settings::where('ads_key', '=', 'room_type')->get();
        $room_feature = Settings::where('ads_key', '=', 'room_feature')->get();
        if ($this->isHotelOwner) {
            $hotelOwner = AdminOwners::with('hotels')->find($this->cuser['admin_uid']);
            return view('admin.room.create', array('response' => $this->_response, 'room_types' => $room_type, 'room_features' => $room_feature, 'hotels' => $hotelOwner->hotels));
        } else if ($this->isHotelAdmin) {
            $hotels = HotelAdmins::with('ownersHotel')->where('ha_ao_id', '=', $this->cuser['admin_uid'])->get();
            foreach ($hotels as $key => $hotel) {
                $hotels[$key] = $hotel->ownersHotel;
            }
            return view('admin.room.create', array('response' => $this->_response, 'room_types' => $room_type, 'room_features' => $room_feature, 'hotels' => $hotels));
        }
//        $hotels = null;
//        if ($this->_response['status']['isSuperAdmin']) {
//            $hotels = Hotels::all();
//        } else if ($this->_response['status']['isHotelOwner']) {
//            $owner_id = $this->cuser['admin_uid'];
//            $hotels = Hotels::where('hotel_owner_id', '=', $owner_id)->get();
//        }
        else {
            $hotels = Hotels::all();
            return view('admin.room.create', array('response' => $this->_response, 'room_types' => $room_type, 'room_features' => $room_feature, 'hotels' => $hotels));
        }
    }

    public function save(Request $request)
    {
        $rule = [
            'room_title' => 'required',
            'room_size' => 'required',
            'room_type' => 'required',
            'adult' => 'required',
            'child' => 'required',
            'description' => 'required|min:10',
            'roomfeatures' => 'required',
            'room_price' => 'required',
            'images'=>'required'

        ];


        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $item = new Rooms();
        $files = Input::file('images');
        $fileArray = [];

        if ($files[0]!='') {
            foreach ($files as $file) {

                $destinationPath = 'public/images/rooms';
                $filename = rand() . $file->getClientOriginalName();
                $upload_success = $file->move($destinationPath, $filename);
                if ($upload_success) {
                    array_push($fileArray, $filename);
                }
            }
            $fileJson = json_encode($fileArray);
            $item->hr_room_images = $fileJson;
        }
        else
        {

            return redirect()->back()->withInput()->withErrors("Room image  is required");
        }


        $item->hr_room_title = $request->room_title;
        $item->hr_room_description = $request->description;
        $item->hr_room_features = json_encode($request->roomfeatures);
        $item->hr_room_type = $request->room_type;
        $item->hr_price = $request->room_price;

        $item->hr_adult = $request->adult;
        $item->hr_children = $request->child;
        $item->hr_created_by = $this->cuser['admin_uid'];
        $item->hr_updated_by = $this->cuser['admin_uid'];
        $item->hr_created_on = date("Y-m-d H:i:s");
        $item->hr_update_on = date("Y-m-d H:i:s");
        $item->hr_room_status = "active";
        $item->hr_room_size = $request->room_size;
        $item->hr_hotel_id = $request->hotel;

        if ($item->save()) {

            return redirect('admin/room/manage');
        }
    }

    public function manage()
    {

        $data = Rooms::with('hotels')->get();

        return view('admin.room.manage', array('response' => $this->_response, 'rooms' => $data));
    }

    public function deleteRoom($id)
    {
        $room = Rooms::find($id);

        if ($room->delete()) {
            return redirect()->back()->with('Room Delete Successful');
        }
    }

    public function block()
    {
        $id = Input::get('id');

        $room = Rooms::find($id);
        $room->hr_room_status = "inactive";
        if ($room->save()) {
            echo json_encode(array('status' => true));
        }
    }

    public function unblock()
    {
        $id = Input::get('id');
        $room = Rooms::find($id);
        $room->hr_room_status = "active";
        if ($room->save()) {
            echo json_encode(array('status' => true));
        }
    }

    public function editRoom($id)
    {
        $room = Rooms::with('hotels')->find($id);
        $room_type = Settings::where('ads_key', '=', 'room_type')->get();
        $room_feature = Settings::where('ads_key', '=', 'room_feature')->get();

        $hotels = Hotels::all();

        return view('admin.room.edit', array('response' => $this->_response, 'room' => $room, 'room_types' => $room_type, 'room_features' => $room_feature, 'hotels' => $hotels));
    }

    public function RemoveRoomImage()
    {
        $id = Input::get('id');
        $value = Input::get('name');
        $room = Rooms::find($id);
        $image = json_decode($room->hr_room_images);
        foreach ($image as $key => $val) {
            if ($val == $value) {
                unset($image[$key]);
            }
        }
        $room->hr_room_images = json_encode(array_values($image));

        if ($room->save()) {
            echo json_encode(array('status' => true, 'msg' => "Success"));
        }
    }

    public function UpdateRoom($id)
    {
        $rule = [
            'room_title' => 'required',
            'room_size' => 'required',
            'room_type' => 'required',
            'adult' => 'required',
            'child' => 'required',
            'description' => 'required|min:10',
            'roomfeatures' => 'required',
            'room_price' => 'required',
        ];
        $input = Input::all();

        $validator = Validator::make($input, $rule);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $item = Rooms::find($id);
        $files = Input::file('images');

        if (count($files) > 0) {
            $fileArray = json_decode($item->hr_room_images);
            foreach ($files as $file) {
                $destinationPath = 'public/images/rooms';
                $filename = rand() . $file->getClientOriginalName();
                $upload_success = $file->move($destinationPath, $filename);
                if ($upload_success) {
                    array_push($fileArray, $filename);
                }
            }
            $item->hr_room_images = json_encode($fileArray);
        }


        $item->hr_room_title = $input['room_title'];
        $item->hr_room_description = $input['description'];
        $item->hr_room_features = json_encode($input['roomfeatures']);
        $item->hr_room_type = $input['room_type'];
        $item->hr_price = $input['room_price'];
        $item->hr_adult = $input['adult'];
        $item->hr_children = $input['child'];
        $item->hr_room_size = $input['room_size'];
        $item->hr_hotel_id = $input['hotel'];
        $item->hr_updated_by = $this->cuser['admin_uid'];
        $item->hr_update_on = date("Y-m-d H:i:s");
        $item->hr_room_status = "active";
        if ($item->save()) {

            return redirect('admin/room/manage');
        }
    }
    public function pdf()
    {
        $data = Rooms::with('hotels')->get();
        $html = view('admin.room.pdf')->with('rooms', $data)->with('response',  $this->_response);

       // $html = view('admin.room.manage', array('response' => $this->_response, 'rooms' => $data));

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($html);
        return $pdf->stream();
    }

}
