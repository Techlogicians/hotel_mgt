<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

//use Illuminate\Auth\Passwords\CanResetPassword;
//use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class HotelAdmins extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hotel_admins';
    public $timestamps = false;
    public $primaryKey = 'ha_id';

    public function AdminOwner() {
        return $this->hasOne('App\Models\AdminOwners', 'ao_id', 'ha_ao_id');
    }
    public function ownersHotel() {
        return $this->hasOne('App\Models\Hotels', 'hotel_id', 'ha_hotel_id');
    }
//    public function hasHotels() {
//        return $this->hasMany('App\Models\Hotels', 'hotel_id', 'ha_hotel_id');
//    }
    

    public function assignToHotel($hotel, $adminUser, $cuser) {
        $hotelAdmin = new HotelAdmins();
        $hotelAdmin->ha_hotel_id = $hotel;
        $hotelAdmin->ha_ao_id = $adminUser->ao_id;
        $hotelAdmin->ha_status = 'active';
        $hotelAdmin->ha_created_by = $cuser['admin_uid'];
        $hotelAdmin->ha_created_on = date("Y-m-d H:i:s");
        $hotelAdmin->save();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
}
