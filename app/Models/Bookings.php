<?php
/**
 * Created by PhpStorm.
 * User: Mausum
 * Date: 7/8/2015
 * Time: 2:58 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Bookings extends Model {
    protected $table = 'bookings';
    protected $primaryKey = 'booking_id';
    public $timestamps = false;

    public function Hotels(){
        return $this->belongsTo('App\Models\Hotels', 'booking_hotel_id');
    }
    public function Users(){
        return $this->belongsTo('App\Models\Users', 'booking_user_id');
    }
    public function Hotel_rooms(){
        return $this->belongsTo('App\Models\Rooms', 'booking_room_id');
    }

}