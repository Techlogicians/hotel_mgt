<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

//use Illuminate\Auth\Passwords\CanResetPassword;
//use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class AdminOwners extends Model {

    protected $fillable = [];
    protected $table = 'admin_owners';
    public $timestamps = false;
    public $primaryKey = 'ao_id';
    
    public function hotels(){
        return $this->hasMany('App\Models\Hotels', 'hotel_owner_id');
    }

    public function getAllOwner() {
        $user = AdminOwners::where('ao_type', '=', 'owner')->get();
        return $user;
    }

    public function registerAdmin($request, $cuser) {
        $adminUser = new AdminOwners();
        $adminUser->ao_firstname = $request->fname;
        $adminUser->ao_lastname = $request->lname;
        $adminUser->ao_email = $request->ao_email;
        $adminUser->ao_username = $request->ao_username;
        $adminUser->ao_password = md5($request->password);
        $adminUser->ao_address = $request->address;
        $adminUser->ao_country = $request->country;
        $adminUser->ao_city = $request->city;
        $adminUser->ao_zip = $request->zip;
        $adminUser->ao_type = $request->admin_type;
        $adminUser->ao_status = "active";
        $adminUser->ao_created_by = $cuser['admin_uid'];
        $adminUser->ao_created_at = date("Y-m-d H:i:s");
        $adminUser->save();
        return $adminUser;
    }

    public function checkLogin($input) {
        $user = AdminOwners::where('ao_username', '=', $input['username'])
                ->orwhere('ao_email', '=', $input['username'])
                ->get();
        if (count($user)) {
            $user = $user[0];
            if (md5($input['password']) == $user['ao_password']) {
                $user->message = "login successful";
                return $user;
            } else {
                $user->errorMessage = "Invalid Username or Password";
                return $user;
            }
        } else {
            $user->errorMessage = "User does not exist";
            return $user;
        }
    }

    public function updatePassword($id, $input) {
        $user = AdminOwners::where('ao_id', '=', $id)->where('ao_password', '=', md5($input['old_password']))->first();
        if (!empty($user)) {
            $user->ao_password = md5($input['new_password']);
            $user->save();
            return $user;
        } else {
            $user = new AdminOwners();
            $user->errormessage = "Password doesn't match";
            return $user;
        }
    }

}
