<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Auth\Passwords\CanResetPassword;
//use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;



class Hotels extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hotels';
    protected $primaryKey = 'hotel_id';
    public $timestamps = false;
    
    public function hotelAdmins(){
        return $this->hasMany('App\Models\HotelAdmins', 'ha_hotel_id');
    }
    public function Rooms(){
        return $this->hasMany('App\Models\Rooms', 'hotel_id');
    }
}
