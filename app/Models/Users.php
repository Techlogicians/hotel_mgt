<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Auth\Passwords\CanResetPassword;
//use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;



class Users extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $primaryKey  = 'user_id';
	public $timestamps = false;

	public function Countries(){
		return $this->belongsTo('App\Models\Locations', 'user_country');
	}
	public function Cities(){
		return $this->belongsTo('App\Models\Locations', 'user_city');
	}

	public function CheckLogin($input) {
		$user = Users::where('user_username', '=', $input['username'])
			->orwhere('user_email', '=', $input['username'])
			->where('user_status','=','active')
			->get();
		if (count($user)) {
			$user = $user[0];
			if (md5($input['password']) == $user['user_password']) {
				$user->message = "login successful";
				return $user;
			} else {
				$user = new Users();
				$user->errorMessage = "Invalid Username or Password";
				return $user;
			}
		} else {
			$user = new Users();
			$user->errorMessage = "User does not exist";
			return $user;
		}
	}



}
