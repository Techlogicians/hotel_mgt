<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Auth\Passwords\CanResetPassword;
//use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;



class Rooms extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'hotel_rooms';
	protected $primaryKey  = 'hr_id';
	public $timestamps = false;

	public function Hotels(){
		return $this->belongsTo('App\Models\Hotels', 'hr_hotel_id');
	}




}
