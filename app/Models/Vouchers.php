<?php
/**
 * Created by PhpStorm.
 * User: Mausum
 * Date: 7/8/2015
 * Time: 2:58 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Vouchers extends Model {
    protected $table = 'vouchers';
    protected $primaryKey = 'voucher_id';
    public $timestamps = false;

    public function Hotels(){
        return $this->belongsTo('App\Models\Hotels', 'voucher_hotel_id');
    }

}