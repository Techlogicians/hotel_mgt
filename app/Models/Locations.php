<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

//use Illuminate\Auth\Passwords\CanResetPassword;
//use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;



class Locations extends Model {

    protected $table = 'location';
    protected $primaryKey = 'location_id';
    public $timestamps = false;


}
