@extends('admin.layout.main_layout')
@section('content')


    <script src="{{URL::to('public/Plugins/datetimepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{URL::to('public/Plugins/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
    <link href="{{URL::to('public/Plugins/datetimepicker/bootstrap-datetimepicker.css')}}" type="text/css"
          rel="stylesheet"/>
    <link href="{{URL::to('public/Plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" type="text/css"
          rel="stylesheet"/>

    <h2 class="margin-none">Edit Voucher&nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>
    <div class="panel panel-default col-md-8">
        {!! Breadcrumbs::render('editvoucher',$voucher) !!}
        <div class="panel-body">
            <form action="{{URL::to('admin/vouchers/update/'.$voucher->voucher_id)}}" method="Post" enctype="multipart/form-data" id="form">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <?php if($errors->first() != ""){?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                    {{ $errors->first() }}
                </div>
                <?php }?>

                @if($response['status']['isHotelOwner'])
                    <div class="form-group">
                        <label>Hotel Name: </label>
                        <select name="hotel" class="form-control">
                            @foreach($hotels as $hotel)
                                <option value="{{$hotel['hotel_id']}}">{{$hotel['hotel_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                @elseif($response['status']['isSuperAdmin'])
                    <div class="form-group">
                        <label>Hotel Name: </label>
                        <select name="hotel" class="form-control">
                            <option value="all">All Hotels</option>
                            @foreach($hotels as $hotel)
                                <option value="{{$hotel['hotel_id']}}">{{$hotel['hotel_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div class="form-group">
                    <label>Voucher Code: </label>
                    <input type="text" class="form-control" name="voucher_code" id="hotel_name" value="{{$voucher->voucher_code}}"/>
                </div>
                <div class="form-group">
                    <label>Voucher Description: </label>
                    <textarea class="form-control" name="voucher_description">{{$voucher->voucher_description}}</textarea>
                </div>
                <div class="form-group">
                    <label>Voucher Discount Type: </label>
                    <select name="voucher_discount_type" class="form-control">
                        <option value="percent" @if($voucher->voucher_discount_type=="percent"){{"Selected"}}@endif>Percent</option>
                        <option value="fixed"@if($voucher->voucher_discount_type=="fixed"){{"Selected"}}@endif>Fixed</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Voucher Discount Amount: </label>
                    <input type="text" class="form-control" name="voucher_discount_amount" id="hotel_name" value="{{$voucher->voucher_discount_amount}}"/>
                </div>
                <div class="form-group">
                    <label>Voucher Expire Date: </label>

                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' name="voucher_expire_date" class="form-control"value="{{$voucher->voucher_expire_date}}"/>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>

                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" name="register" id="register"
                           value="Save Changes"/>
                </div>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $(function () {
                $('#datetimepicker1').datepicker({
                    format: 'yyyy-mm-dd'
                });
            });
        });
    </script>

@stop