@extends('admin.layout.main_layout')
@section('content')
    <h2 class="margin-none">Manage Admin &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>

    <div class="separator-h"></div>
<div class="row">
    <div class="widget widget-body-white widget-heading-simple">
        {!! Breadcrumbs::render('manageadmin') !!}
        <div class="widget-body">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token()}}">
                <table class="display" cellspacing="0" width="100%" id="myTable">
                    <thead class="bg-gray">
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach($admins as $admin)
                        <tr>
                            <td>{{$ct= $admin->ao_id}} </td>
                            <td>{{$admin->ao_firstname. " ". $admin->ao_lastname}} </td>
                            <td>{{$admin->ao_email}} </td>
                            <td>
                                @if($admin->ao_type=="admin")
                                <span>SuperAdmin</span>
                                @elseif($admin->ao_type=="mod")
                                <span>Moderator</span>
                                @elseif($admin->ao_type=="owner")
                                <span>Hotel Owner</span>
                                @else
                                <span>Hotel Admin</span>
                                @endif
                            </td>
                            <td>
                                @if($admin->ao_status=="active")
                                <span id='status_{{$ct}}'>Active </span>
                                @endif
                                @if($admin->ao_status=="inactive")
                                <span id='status_{{$ct}}'>Inactive </span>
                                @endif
                            </td>
                            <td>
                                <a href="{{URL::to('/admin/manage/edit/').'/'.$ct}}" title="edit" class="btn btn-circle btn-success"><i class="icon-compose"></i></a>
                                <a onclick="myfunction('{{$ct}}');" class="btn btn-circle btn-danger"><i class="fa fa-trash-o"></i></a>
                                <a href="{{URL::to('/admin/manage/details/').'/'.$ct}}" title="details" class="btn btn-circle btn-success"><i class="fa fa-fw icon-dial-pad"></i></a>
                                @if($admin->ao_status=="active")

                                <a id="block_admin_{{$ct}}" href="javascript:void(0)" onclick="block_admin('{{$ct}}')" title="Block" class="btn btn-circle btn-success"><i class="icon-lock-fill"></i></a>
                                <a id="unblock_admin_{{$ct}}"  onclick="unblock_admin('{{$ct}}')" style="display:none;" title="Unblock" class="btn btn-circle btn-success"><i class="icon-unlock-fill"></i></a>
                                @endif
                                @if($admin->ao_status=="inactive")

                                <a id="unblock_admin_{{$ct}}" onclick="unblock_admin('{{$ct}}')"  title="Unblock" class="btn btn-circle btn-success"><i class="icon-unlock-fill"></i></a>
                                <a id="block_admin_{{$ct}}" onclick="block_admin('{{$ct}}')" style="display:none;" title="Block" class="btn btn-circle btn-success"><i class="icon-lock-fill"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/js/jquery.dataTables.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
<script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/TableTools/media/js/TableTools.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
<script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/ColVis/media/js/ColVis.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
<script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/custom/js/DT_bootstrap.js?v=v1.0.2&sv=v0.0.1')}}"></script>
<script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/custom/js/datatables.init.js?v=v1.0.2&sv=v0.0.1')}}"></script>
<script src="{{URL::to('resources/assets/components/common/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.0.2&sv=v0.0.1')}}"></script>
<script src="{{URL::to('resources/assets/components/common/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.0.2&sv=v0.0.1')}}"></script>
<script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/FixedHeader/FixedHeader.js?v=v1.0.2&sv=v0.0.1')}}"></script>
<script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/ColReorder/media/js/ColReorder.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
<script>

                                                                                $(document).ready(function(){
                                                                        $('#myTable').DataTable();
                                                                        });    </script>
<script>
                                            function block_admin(id)
                                            {
                                            var token = $('#_token').val();
                                                    $.ajax({
                                                    url: $("#base_url").val() + "/admin/manage/block",
                                                            type: 'POST',
                                                            data: {id: id, _token:token},
                                                            dataType:'json',
                                                            success: function(result) {
                                                            if (result.status)
                                                            {
                                                            $('#status_' + id).html('Inactive');
                                                                    $('#block_admin_' + id).hide();
                                                                    $('#unblock_admin_' + id).show();
                                                            }
                                                            }
                                                    });
                                            }
                                    function unblock_admin(id)
                                    {
                                    var token = $('#_token').val();
                                            $.ajax({
                                            url: $("#base_url").val() + "/admin/manage/unblock",
                                                    type: 'POST',
                                                    data: {id: id, _token:token},
                                                    dataType:'json',
                                                    success: function(result) {
                                                    if (result.status)
                                                    {
                                                    $('#status_' + id).html('Active');
                                                            $('#unblock_admin_' + id).hide();
                                                            $('#block_admin_' + id).show();
                                                    }


                                                    }
                                            });
                                    }
                                    function myfunction(id) {

                                    if (confirm("Do you want to delete this item?") == true) {

                                    window.location.href = $("meta[name='baseUrl']").attr("content") + "/admin/manage/delete/" + id;
                                    } else {
                                    return false;
                                    }

                                    }
</script>
@stop