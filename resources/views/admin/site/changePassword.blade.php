@extends('admin.layout.main_layout')
@section('content')
    <div class="panel panel-default col-md-8">
        <div class="panel-body">
            <form action="{{URL::to('admin/changeAdminPassword')}}" method="Post">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <?php if($errors->first()!=""){?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                        {{ $errors->first() }}
                </div>
                <?php }?>
                <div class="form-group">
                    <label>Email: </label>
                    <input type="text" class="form-control" name="admin_email" id="admin_email" value="{{$response['status']['cuser']['email']}}" disabled="disabled"/>
                </div>
                <div class="form-group">
                    <label>Old Password: </label>
                    <input type="password" class="form-control" name="old_password" id="old_password" value="" required/>
                </div>
                <div class="form-group">
                    <label>New Password: </label>
                    <input type="password" class="form-control" name="new_password" id="new_password" value="" placeholder="Minimum 6 characters" required/>
                </div> 
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" name="changePassword" id="changePassword" value="Update Password"/>
                </div>
            </form>
        </div>
    </div>

@stop
