@extends('admin/layout/main_layout')
@section('content')
    <h2 class="margin-none">Admin Details &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>

    <div class="separator-h"></div>
<div class="panel panel-default col-md-8">
    {!! Breadcrumbs::render('admindetails',$admin) !!}
    <div class="panel-body">
        <div class="form-group">
            <div class="form-group">
                <label>First Name: </label>
                <input type="text" class="form-control" name="fname" id="fname" value="{{$admin['ao_firstname']}}" disabled/>
            </div>
            <div class="form-group">
                <label>Last Name: </label>
                <input type="text" class="form-control" name="lname" id="lname" value="{{$admin['ao_lastname']}}" disabled/>
            </div>
            <div class="form-group">
                <label>Email: </label>
                <input type="email" class="form-control" name="ao_email" id="ao_email" value="{{$admin['ao_email']}}" disabled/>
            </div>
            <div class="form-group">
                <label>Username: </label>
                <input type="text" class="form-control" name="ao_username" id="ao_username" value="{{$admin['ao_username']}}" disabled/>
            </div>
            <div class="form-group">
                <label>Admin Type: </label>
                <select name="admin_type" class="form-control" disabled>
                    <option value="{{$admin->ao_type}}"> @if($admin->ao_type == "admin")Super Admin @elseif($admin->ao_type == "owner")Hotel Owner @elseif($admin->ao_type == "mod")Moderator @else Hotel Admin @endif</option>
                </select>
            </div>
            @if($admin->ao_type == "cadmin")
            @if(isset($hotels))
            @if(isset($owner))
            <div class="form-group">
                <label>Hotel Owner: </label>
                <select name="hotel_owner" class="form-control" disabled>
                    <option value=""> {{$owner->ao_firstname. " ". $owner->ao_lastname}}</option>
                </select>
            </div>
            @endif
            <div class="form-group">
                <label>Hotel Name: </label>
                <select name="hotel_name" class="form-control" disabled>
                    <option value=""> {{$hotels->hotel_name}}</option>
                </select>
            </div>
            @endif
            @endif

            <div class="form-group">
                <label>Address: </label>
                <input type="text" class="form-control" name="address" id="address" value="{{$admin['ao_address']}}" disabled/>
            </div>
            <div class="form-group">
                <label>City: </label>
                <input type="text" class="form-control" name="city" id="city" value="{{$admin['ao_city']}}" disabled/>
            </div>
            <div class="form-group">
                <label>Zip: </label>
                <input type="text" class="form-control" name="zip" id="zip" value="{{$admin['ao_zip']}}" disabled/>
            </div>
            <div class="form-group">
                <label>Country: </label>
                <input type="text" class="form-control" name="country" id="country" value="{{$admin['ao_country']}}" disabled/>
            </div>
            <div class="form-group">
                <label>Admin Status: </label>
                <select name="admin_status" class="form-control" disabled>
                    <option value=""> {{ucfirst($admin->ao_status)}} </option>
                </select>
            </div>
        </div>
    </div>

    @stop

