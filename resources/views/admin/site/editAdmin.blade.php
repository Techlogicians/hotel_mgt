@extends('admin/layout/main_layout')
@section('content')
<h2 class="margin-none">Edit Admin&nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>

<div class="separator-h"></div>
<div class="panel panel-default col-md-8">
    {!! Breadcrumbs::render('admindetails',$admin) !!}
    <div class="panel-body">
        <form action="{{URL::to('admin/manage/update/'.$admin->ao_id)}}" method="Post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token()}}">
            <?php if ($errors->first() != "") { ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                    {{ $errors->first() }}
                </div>
            <?php } ?>
            <div class="form-group">
                <div class="form-group">
                    <label>First Name: </label>
                    <input type="text" class="form-control" name="fname" id="fname" value="{{$admin['ao_firstname']}}"/>
                </div>
                <div class="form-group">
                    <label>Last Name: </label>
                    <input type="text" class="form-control" name="lname" id="lname" value="{{$admin['ao_lastname']}}"/>
                </div>
                <div class="form-group">
                    <label>Email: </label>
                    <input type="email" class="form-control" name="ao_email" id="ao_email" value="{{$admin['ao_email']}}"/>
                </div>
                <div class="form-group">
                    <label>Username: </label>
                    <input type="text" class="form-control" name="ao_username" id="ao_username" value="{{$admin['ao_username']}}"/>
                </div>
                <div class="form-group">
                    <label>Admin Type: </label>
                    <select name="admin_type" class="form-control">
                        <option value="{{$admin->ao_type}}"> @if($admin->ao_type == "admin")Super Admin @elseif($admin->ao_type == "owner")Hotel Owner @elseif($admin->ao_type == "mod")Moderator @else Hotel Admin @endif</option>
                    </select>
                </div>
                @if($admin->ao_type == "cadmin")
                @if(isset($hotels))
                @if(isset($owner))
                <div class="form-group">
                    <label>Hotel Owner: </label>
                    <select name="hotel_owner" class="form-control">
                        <option value=""> {{$owner->ao_firstname. " ". $owner->ao_lastname}}</option>
                    </select>
                </div>
                @endif
                <div class="form-group">
                    <label>Hotel Name: </label>
                    <select name="hotel_id" class="form-control">
                        @foreach($owner->hotels as $ownersHotel)
                        <option value="{{$ownersHotel->hotel_id}}" @if($ownersHotel->hotel_id == $hotelAdmin->ha_hotel_id) selected @endif> {{$ownersHotel->hotel_name}}</option>
                        @endforeach
                    </select>
                </div>
                @endif
                @endif

                <div class="form-group">
                    <label>Address: </label>
                    <input type="text" class="form-control" name="address" id="address" value="{{$admin['ao_address']}}"/>
                </div>
                <div class="form-group">
                    <label>City: </label>
                    <input type="text" class="form-control" name="city" id="city" value="{{$admin['ao_city']}}"/>
                </div>
                <div class="form-group">
                    <label>Zip: </label>
                    <input type="text" class="form-control" name="zip" id="zip" value="{{$admin['ao_zip']}}"/>
                </div>
                <div class="form-group">
                    <label>Country: </label>
                    <input type="text" class="form-control" name="country" id="country" value="{{$admin['ao_country']}}"/>
                </div>
                <div class="form-group">
                    <label>Admin Status: </label>
                    <select name="admin_status" class="form-control">
                        <option value="active" @if($admin->ao_status == 'active') selected @endif>{{ucfirst('active')}}</option>
                        <option value="inactive" @if($admin->ao_status == 'inactive') selected @endif>{{ucfirst('inactive')}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" name="update_admin" id="update_admin" value="Update Admin"/>
                </div>
        </form>
    </div>
</div>

@stop

