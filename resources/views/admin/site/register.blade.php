@extends('admin/layout/main_layout')
@section('content')
    <h2 class="margin-none">Create Admin&nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>

    <div class="separator-h"></div>
    <div class="panel panel-default col-md-8">
        {!! Breadcrumbs::render('createadmin') !!}
        <div class="panel-body">
            <input type="hidden" id="_token" value="{{ csrf_token() }}">

            <div class="form-group">
                <label>First Name: </label>
                <input type="text" class="form-control" name="first_name" id="fname" value=""/>
            </div>
            <div class="form-group">
                <label>Last Name: </label>
                <input type="text" class="form-control" name="last_name" id="lname" value=""/>
            </div>
            <div class="form-group">
                <label>Email: </label>
                <input type="email" class="form-control" name="email" id="email" value=""/>
            </div>
            <div class="form-group">
                <label>Username: </label>
                <input type="text" class="form-control" name="username" id="username" value=""/>
            </div>
            <div class="form-group">
                <label>Password: </label>
                <input type="password" class="form-control" name="password" id="password"
                       placeholder='Minimum 6 Characters' value=""/>
            </div>
            <div class="form-group">
                <label>Confirm Password: </label>
                <input type="password" class="form-control" name="con_password" id="con_password" value=""/>
            </div>
            <div class="form-group">
                <label>Admin Type: </label>
                <select id="admin_type" class="form-control">
                    <option value="">Select Type</option>
                    @if($response['status']['isSuperAdmin'])
                        <option value="admin">Super Admin</option>
                        <option value="mod">Moderator</option>
                        <option value="owner">Hotel Owner</option>
                    @elseif($response['status']['isModerator'])
                        <option value="owner">Hotel Owner</option>
                    @endif

                </select>
            </div>
            <div class="form-group">
                <label>Address: </label>
                <input type="text" class="form-control" name="address" id="address" value=""/>
            </div>
            <div class="form-group">
                <label>City: </label>
                <input type="text" class="form-control" name="city" id="city" value=""/>
            </div>
            <div class="form-group">
                <label>Zip: </label>
                <input type="text" class="form-control" name="zip" id="zip" value=""/>
            </div>
            <div class="form-group">
                <label>Country: </label>
                <input type="text" class="form-control" name="country" id="country" value=""/>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary btn-block" name="register" id="register"
                       value="Create Admin"/>
            </div>
        </div>
    </div>
    <script>
    </script>
@stop

