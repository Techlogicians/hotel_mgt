@extends('admin/layout/main_layout')
@section('content')
    <h2 class="margin-none">Create HotelAdmin &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>

    <div class="separator-h"></div>
    <div class="panel panel-default col-md-8">
        {!! Breadcrumbs::render('createHotelAdmin') !!}
        <div class="panel-body">
            <form action="{{URL::to('admin/hotelAdmin/create')}}" method="Post" id="admin-form">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <?php if ($errors->first() != "") { ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                    {{ $errors->first() }}
                </div>
                <?php } ?>
                <div class="form-group">
                    <div class="form-group">
                        <label>First Name: </label>
                        <input type="text" class="form-control" name="fname" id="fname" value=""/>
                    </div>
                    <div class="form-group">
                        <label>Last Name: </label>
                        <input type="text" class="form-control" name="lname" id="lname" value=""/>
                    </div>
                    <div class="form-group">
                        <label>Email: </label>
                        <input type="email" class="form-control" name="ao_email" id="ao_email" value=""/>
                    </div>
                    <div class="form-group">
                        <label>Username: </label>
                        <input type="text" class="form-control" name="ao_username" id="ao_username" value=""/>
                    </div>
                    <div class="form-group">
                        <label>Password: </label>
                        <input type="password" class="form-control" name="password" id="password"
                               placeholder='Minimum 6 Characters' value=""/>
                    </div>
                    <div class="form-group">
                        <label>Confirm Password: </label>
                        <input type="password" class="form-control" name="con_password" id="con_password" value=""/>
                    </div>
                    @if(isset($hotels))
                        <div class="form-group">
                            <label>Hotel Name: </label>
                            <select name="hotel_id" class="form-control">
                                <option value="">Select Hotel</option>
                                @foreach($hotels as $hotel)
                                    <option value="{{$hotel->hotel_id}}">{{$hotel->hotel_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                    <div class="form-group">
                        <label>Address: </label>
                        <input type="text" class="form-control" name="address" id="address" value=""/>
                    </div>
                    <div class="form-group">
                        <label>City: </label>
                        <input type="text" class="form-control" name="city" id="city" value=""/>
                    </div>
                    <div class="form-group">
                        <label>Zip: </label>
                        <input type="text" class="form-control" name="zip" id="zip" value=""/>
                    </div>
                    <div class="form-group">
                        <label>Country: </label>
                        <input type="text" class="form-control" name="country" id="country" value=""/>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-block" name="register" id="register"
                               value="Create Hotel Admin"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $("#admin-form").submit(function(){
          if(!isNaN($("#fname").val())||!isNaN($("#lname").val()))
          {
              alert("Name Cannot Be A number");
              return false;
          }
        })
    </script>
@stop

