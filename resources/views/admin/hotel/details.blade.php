@extends('admin.layout.main_layout')
@section('content')
<div class="panel panel-default col-md-8">
    <h2 class="margin-none">Hotel Details &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i> {!! Breadcrumbs::render('detailhotel',$hotel) !!}</h2>



    <div class="separator-h"></div>
    <div class="panel-body">
        <div class="form-group">
            <label>Hotel Name: </label>
            <input type="text" class="form-control" name="hotel_name" id="hotel_name" value="{{$hotel->hotel_name}}" disabled/>
        </div>
        <div class="form-group">
            <label>Hotel Description: </label>
            <input type="text" class="form-control" name="hotel_description" id="hotel_description" value="{{$hotel->hotel_description}}" disabled/>
        </div>
        <div class="form-group">
            <label>Hotel Banner Image: </label>
            <img src="{{URL::asset('public/images/hotels')}}/{{$hotel->hotel_banner_image}}" style="max-height: 200px;max-width: 220px;" disabled>
        </div>
        @if(!$response['status']['isHotelOwner'])
        <div class="form-group">
            <label>Hotel owner: </label>
            <select name="hotel_owner" class="form-control" disabled>
                <option value="{{$owner->ao_id}}">{{$owner->ao_username}}</option>
            </select>
        </div>
        @endif
        <div class="form-group">
            <label>Hotel Type: </label>
            <input type="text" class="form-control" name="hotel_type" id="hotel_type" value="{{$hotel->hotel_type}}" disabled />
        </div>
        <div class="form-group">
            <label>Hotel Category: </label>
            <input type="text" class="form-control" name="hotel_category" id="hotel_category"  value="{{$hotel->hotel_category}}" disabled/>
        </div>
        <div class="form-group">
            <label>Hotel Address: </label>
            <input type="text" class="form-control" name="hotel_address" id="hotel_address" value="{{$hotel->hotel_address}}" disabled/>
        </div>
        <div class="form-group">
            <label>Hotel City: </label>
            <input type="text" class="form-control" name="hotel_city" id="hotel_city" value="{{$hotel->hotel_city}}" disabled/>
        </div>
        <div class="form-group">
            <label>Hotel zip: </label>
            <input type="text" class="form-control" name="hotel_zip" id="hotel_zip" value="{{$hotel->hotel_zip}}" disabled/>
        </div>
        <div class="form-group">
            <label>Hotel country: </label>
            <input type="text" class="form-control" name="hotel_country" id="hotel_address" value="{{$hotel->hotel_country}}" disabled/>
        </div>

        <div class="form-group">
            <label>Hotel Facilities: </label>
            <input type="text" class="form-control" name="hotel_facilities" id="hotel_facilities" value="{{$hotel->hotel_facilities}}" disabled/>
        </div>
        <div class="form-group">
            <label>Hotel Website: </label>
            <input type="text" class="form-control" name="hotel_website" id="hotel_website" value="{{$hotel->hotel_website}}" disabled/>
        </div>
        <div class="form-group">
            <label>Hotel Facebook: </label>
            <input type="text" class="form-control" name="hotel_facebook" id="hotel_facebook" value="{{$hotel->hotel_facebook}}" disabled/>
        </div>
        <div class="form-group">
            <label>Hotel twitter: </label>
            <input type="text" class="form-control" name="hotel_twitter" id="hotel_twitter" value="{{$hotel->hotel_twitter}}" disabled/>
        </div>
        <div class="form-group">
            <label>Hotel Status: </label>
            <select name="hotel_status" class="form-control" disabled>
                @if($hotel->hotel_status == 'active')
                <option value="active"> active </option>
                @else
                <option value="inactive" > active </option>
                @endif
            </select>
        </div>

    </div>
</div>

@stop

