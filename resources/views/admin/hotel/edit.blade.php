@extends('admin.layout.main_layout')
@section('content')
    <h2 class="margin-none">Edit Hotel &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>

    <div class="separator-h"></div>
    <div class="panel panel-default col-md-8">
        {!! Breadcrumbs::render('edithotel',$hotel) !!}
        <div class="panel-body">
            <form action="{{URL::to('admin/hotel/update/'.$hotel->hotel_id)}}" method="Post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <?php if($errors->first()!=""){?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                        {{ $errors->first() }}
                </div>
                <?php }?>
                <div class="form-group">
                    <label>Hotel Name: </label>
                    <input type="text" class="form-control" name="hotel_name" id="hotel_name" value="{{$hotel->hotel_name}}"/>
                </div>
                <div class="form-group">
                    <label>Hotel Description: </label>
                    <input type="text" class="form-control" name="hotel_description" id="hotel_description" value="{{$hotel->hotel_description}}"/>
                </div>
                <div class="form-group">
                    <label>Hotel Banner Image: </label>
                    <input type="file" class="form-control" name="hotel_banner_image" id="hotel_banner_image" value="{{$hotel->hotel_banner_image}}"/>
                </div>
                <div class="form-group">
                    <label>Hotel Type: </label>

                    <select name="hotel_type" id="hotel_type" class="form-control">
                        <option value="bussinees_hotel" <?php echo($hotel->hotel_type == 'bussinees_hotel' )? ' selected="selected"' : '';?>>Business Hotel</option>
                        <option value="airport_hotel" <?php echo($hotel->hotel_type == 'airport_hotel' )? ' selected="selected"' : '';?>>Airport Hotel</option>
                        <option value="suite_hotel" <?php echo($hotel->hotel_type == 'suite_hotel' )? ' selected="selected"' : '';?>>Suite Hotel</option>
                        <option value="extended_stay_hotel" <?php echo($hotel->hotel_type == 'extended_stay_hotel' )? ' selected="selected"' : '';?>>Extended Stay Hotel</option>
                        <option value="resort_hotel" <?php echo($hotel->hotel_type == 'resort_hotel') ? ' selected="selected"' : '';?>>Resort Hotel</option>
                        <option value="homestay" <?php echo ($hotel->hotel_type == 'homestay') ? ' selected="selected"' : '';?>>Homestay</option>
                        <option value="timeshare" <?php echo ($hotel->hotel_type == 'timeshare') ? ' selected="selected"' : '';?>>Timeshare</option>
                        <option value="casino" <?php echo ($hotel->hotel_type == 'casino') ? ' selected="selected"' : '';?>>Casino</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Hotel Category: </label>
                    <select name="hotel_category" id="hotel_category" class="form-control">
                        <option value="1star" <?php echo( $hotel->hotel_category == '1star') ? ' selected="selected"' : '';?>>1 Star</option>
                        <option value="2star" <?php echo( $hotel->hotel_category == '2star' )? ' selected="selected"' : '';?>>2 Star</option>
                        <option value="3star" <?php echo( $hotel->hotel_category == '3star' )? ' selected="selected"' : '';?>>3 Star</option>
                        <option value="4star" <?php echo( $hotel->hotel_category == '4star') ? ' selected="selected"' : '';?>>4 Star</option>
                        <option value="5star" <?php echo( $hotel->hotel_category == '5star') ? ' selected="selected"' : '';?>>5 Star</option>
                        <option value="standard" <?php echo( $hotel->hotel_category == 'standard' )? ' selected="selected"' : '';?>>Standard</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Hotel Address: </label>
                    <input type="text" class="form-control" name="hotel_address" id="hotel_address" value="{{$hotel->hotel_address}}"/>
                </div>
                <div class="form-group">
                    <label>Hotel City: </label>
                    <input type="text" class="form-control" name="hotel_city" id="hotel_city" value="{{$hotel->hotel_city}}"/>
                </div>
                <div class="form-group">
                    <label>Hotel zip: </label>
                    <input type="text" class="form-control" name="hotel_zip" id="hotel_zip" value="{{$hotel->hotel_zip}}"/>
                </div>
                <div class="form-group">
                    <label>Hotel country: </label>
                    <input type="text" class="form-control" name="hotel_country" id="hotel_address" value="{{$hotel->hotel_country}}"/>
                </div>

                <div class="form-group">
                    <label>Hotel Facilities: </label>
                    <select multiple id="multiselect-custom" class="multiselect" name="hotel_facilities[]">
                        <?php $feature= json_decode($hotel->hotel_facilities) ; ?>

                        @foreach($hotel_feature as $r)
                            <option value="{{$r['ads_value']}}" @if(in_array($r['ads_value'], $feature)){{"Selected"}}@endif>{{$r['ads_value']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Hotel Website: </label>
                    <input type="text" class="form-control" name="hotel_website" id="hotel_website" value="{{$hotel->hotel_website}}"/>
                </div>
                <div class="form-group">
                    <label>Hotel Facebook: </label>
                    <input type="text" class="form-control" name="hotel_facebook" id="hotel_facebook" value="{{$hotel->hotel_facebook}}"/>
                </div>
                <div class="form-group">
                    <label>Hotel twitter: </label>
                    <input type="text" class="form-control" name="hotel_twitter" id="hotel_twitter" value="{{$hotel->hotel_twitter}}"/>
                </div>
                <div class="form-group">
                    <label>Hotel Status: </label>
                    <select name="hotel_status" class="form-control">
                        <option value="active" @if($hotel->hotel_status == 'active') selected @endif>active</option>
                        <option value="inactive" @if($hotel->hotel_status == 'inactive') selected @endif>inactive</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" name="update_hotel" id="update_hotel" value="Update Hotel"/>
                </div>
            </form>
        </div>
    </div>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/multiselect/assets/lib/js/jquery.multi-select.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/multiselect/assets/custom/js/multiselect.init.js?v=v1.0.2&sv=v0.0.1')}}"></script>
@stop

