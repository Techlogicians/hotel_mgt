@extends('admin.layout.main_layout')
@section('content')
    <h2 class="margin-none">Hotels List &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>

    <div class="separator-h"></div>
    <div class="row">
        <div class="widget widget-body-white widget-heading-simple">
            {!! Breadcrumbs::render('managehotel') !!}
            <div class="widget-body">
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token()}}">
                    <table class="display" cellspacing="0" width="100%" id="myTable">
                        <thead class="bg-gray">
                        <th>Id</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @foreach($hotels as $cat)
                            <tr>
                                <td>{{$ct= $cat['hotel_id']}} </td>
                                <td>{{$cat['hotel_name']}} </td>
                                <td>{{$cat['hotel_category']}} </td>
                                <td>
                                    @if($cat['hotel_status']=="active")
                                        <span id='status_{{$ct}}'>Active </span>
                                    @endif
                                    @if($cat['hotel_status']=="inactive")
                                        <span id='status_{{$ct}}'>Inactive </span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{URL::to('/admin/hotel/edit/').'/'.$ct}}" title="edit" class="btn btn-circle btn-success"><i class="icon-compose"></i></a>
                                    <a onclick="myfunction('{{$ct}}');" class="btn btn-circle btn-danger"><i class="fa fa-trash-o"></i></a>
                                    <a href="{{URL::to('/admin/hotel/details/').'/'.$ct}}" title="details" class="btn btn-circle btn-success"><i class="fa fa-align-justify"></i></a>
                                    @if($cat['hotel_status']=="active")

                                        <a id="block_hotel_{{$ct}}" href="javascript:void(0)" onclick="block_hotel('{{$ct}}')" title="Block" class="btn btn-circle btn-success"><i class="icon-lock-fill"></i></a>
                                        <a id="unblock_hotel_{{$ct}}"  onclick="unblock_hotel('{{$ct}}')" style="display:none;" title="Unblock" class="btn btn-circle btn-success"><i class="icon-unlock-fill"></i></a>
                                    @endif
                                    @if($cat['hotel_status']=="inactive")

                                        <a id="unblock_hotel_{{$ct}}" onclick="unblock_hotel('{{$ct}}')"  title="Unblock" class="btn btn-circle btn-success"><i class="icon-unlock-fill"></i></a>
                                        <a id="block_hotel_{{$ct}}" onclick="block_hotel('{{$ct}}')" style="display:none;" title="Block" class="btn btn-circle btn-success"><i class="icon-lock-fill"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/js/jquery.dataTables.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/TableTools/media/js/TableTools.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/ColVis/media/js/ColVis.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/custom/js/DT_bootstrap.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/custom/js/datatables.init.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/FixedHeader/FixedHeader.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/ColReorder/media/js/ColReorder.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script>

        $(document).ready(function(){
            $('#myTable').DataTable();
        });
    </script>
    <script>
        function block_hotel(id)
        {
            var token= $('#_token').val();
            $.ajax({
                url: $("#base_url").val() + "/admin/hotel/block",
                type: 'POST',
                data: {id: id,_token:token},
                dataType:'json',
                success: function(result) {
                    if (result.status)
                    {
                        $('#status_'+id).html('Inactive');
                        $('#block_hotel_' + id).hide();
                        $('#unblock_hotel_' + id).show();
                    }
                }
            });
        }
        function unblock_hotel(id)
        {
            var token= $('#_token').val();
            $.ajax({
                url: $("#base_url").val() + "/admin/hotel/unblock",
                type: 'POST',
                data: {id: id,_token:token},
                dataType:'json',
                success: function(result) {
                    if (result.status)
                    {
                        $('#status_'+id).html('Active');
                        $('#unblock_hotel_' + id).hide();
                        $('#block_hotel_' + id).show();
                    }


                }
            });
        }
        function myfunction(id) {

            if (confirm("Do you want to delete this item?") == true) {

                window.location.href =  $("meta[name='baseUrl']").attr("content") + "/admin/hotel/delete/"+id;
            } else {
                return false;
            }

        }
    </script>
@stop