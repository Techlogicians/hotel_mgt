@extends('admin.layout.main_layout')
@section('content')
    <h2 class="margin-none">Create Hotel &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>

    <div class="separator-h"></div>
    <div class="panel panel-default col-md-8">
        {!! Breadcrumbs::render('createhotel') !!}
        <div class="panel-body">
            <form action="{{URL::to('admin/hotel/save')}}" method="Post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <?php if($errors->first()!=""){?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                        {{ $errors->first() }}
                </div>
                <?php }?>
                <div class="form-group">
                    <label>*Hotel Name: </label>
                    <input type="text" class="form-control" name="hotel_name" id="hotel_name" value=""/>
                </div>
                <div class="form-group">
                    <label>*Hotel Description: </label>
                    <input type="text" class="form-control" name="hotel_description" id="hotel_description" value=""/>
                </div>
                <div class="form-group">
                    <label>Hotel Banner Image: </label>
                    <input type="file" class="form-control" name="hotel_banner_image" id="hotel_banner_image" value=""/>
                </div>
                @if(!$response['status']['isHotelOwner'])
                <div class="form-group">
                    <label>Hotel Owner: </label>
                    <select name="hotel_owner_id" class="form-control">
                        @foreach($owner as $own)
                            <option value="{{$own['ao_id']}}">{{$own['ao_firstname']." ".$own['ao_lastname'] }}</option>
                        @endforeach
                    </select>
                </div>
                @endif
                <div class="form-group">
                    <label>*Hotel Type: </label>
                    <select name="hotel_type" id="hotel_type" class="form-control">
                        <option value="bussinees_hotel">Business Hotel</option>
                        <option value="airport_hotel">Airport Hotel</option>
                        <option value="suite_hotel">Suite Hotel</option>
                        <option value="extended_stay_hotel">Extended Stay Hotel</option>
                        <option value="resort_hotel">Resort Hotel</option>
                        <option value="homestay">Homestay</option>
                        <option value="timeshare">Timeshare</option>
                        <option value="casino">Casino</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>*Hotel Category: </label>
                     <select name="hotel_category" id="hotel_category" class="form-control">
                         <option value="1star">1 Star</option>
                         <option value="2star">2 Star</option>
                         <option value="3star">3 Star</option>
                         <option value="4star">4 Star</option>
                         <option value="5star">5 Star</option>
                         <option value="standard">Standard</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>*Hotel Address: </label>
                    <input type="text" class="form-control" name="hotel_address" id="hotel_address" value=""/>
                </div>
                <div class="form-group">
                    <label>*Hotel City: </label>
                    <input type="text" class="form-control" name="hotel_city" id="hotel_city" value=""/>
                </div>
                <div class="form-group">
                    <label>*Hotel zip: </label>
                    <input type="text" class="form-control" name="hotel_zip" id="hotel_zip" value=""/>
                </div>
                <div class="form-group">
                    <label>*Hotel country: </label>
                    <input type="text" class="form-control" name="hotel_country" id="hotel_address" value=""/>
                </div>

                <div class="form-group">
                    <label>*Hotel Facalities: </label>
                    <select multiple id="multiselect-custom" class="multiselect" name="hotel_facilities[]">
                        @foreach($hotel_feature as $r)
                            <option value="{{$r['ads_value']}}">{{$r['ads_value']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Hotel Website: </label>
                    <input type="text" class="form-control" name="hotel_website" id="hotel_website" value=""/>
                </div>
                <div class="form-group">
                    <label>Hotel Facebook: </label>
                    <input type="text" class="form-control" name="hotel_facebook" id="hotel_facebook" value=""/>
                </div>
                <div class="form-group">
                    <label>Hotel twitter: </label>
                    <input type="text" class="form-control" name="hotel_twitter" id="hotel_twitter" value=""/>
                </div>
                {{--<div class="form-group">--}}
                    {{--<label>Hotel Status: </label>--}}
                    {{--<select name="hotel_status" class="form-control">--}}
                        {{--<option value="active">active</option>--}}
                        {{--<option value="inactive">inactive</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="form-group">
                   <div class="alert-info"> (*)marked fields are mandatory.</div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" name="register" id="register" value="Create Hotel"/>
                </div>
            </form>
        </div>
    </div>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/multiselect/assets/lib/js/jquery.multi-select.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/multiselect/assets/custom/js/multiselect.init.js?v=v1.0.2&sv=v0.0.1')}}"></script>
@stop

