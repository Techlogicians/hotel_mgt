<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 sidebar sidebar-discover"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 sidebar sidebar-discover"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 sidebar sidebar-discover"> <![endif]-->
<!--[if gt IE 8]> <html class="ie sidebar sidebar-discover"> <![endif]-->
<!--[if !IE]><!--><html class="sidebar sidebar-discover"><!-- <![endif]-->
<head>
    @include('admin.layout.head')
</head>
<body class="">

<!-- Main Container Fluid -->
<div class="container-fluid menu-hidden">

    <!-- Sidebar Menu -->
    @include('admin.layout.leftsidebar')
    <!-- // Sidebar Menu END -->

    <!-- Content -->
    <div id="content">

        @include('admin.layout.navigation')
        <!-- // END navbar -->



        <div class="innerLR">
            @yield('content')

        </div>



















    </div>
    <!-- // Content END -->

    <div class="clearfix"></div>
    <!-- // Sidebar menu & content wrapper END -->

    <!--            <div id="footer" class="hidden-print">

                      Copyright Line
                    <div class="copy">&copy; 2012 - 2014 - <a href="http://www.mosaicpro.biz">MosaicPro</a> - All Rights Reserved. <a href="http://themeforest.net/?ref=mosaicpro" target="_blank">Purchase BUSINESS on ThemeForest</a> - Current version: v1.0.2 / <a target="_blank" href="http://demo.mosaicpro.biz/coral/CHANGELOG.txt">changelog</a></div>
                      End Copyright Line

                </div>-->

    <!-- // Footer END -->

</div>
@include('admin.layout.footer');
</body>
</html>