<div id="menu" class="hidden-print hidden-xs">

                <div id="sidebar-discover-wrapper">
                    <ul class="list-unstyled">

                        <li>

                            <a href="{{URL::to('/admin')}}" class="glyphicons home " >
                                {{--<span class="badge pull-right badge-primary hidden-md"></span>--}}
                                <i></i><span>@lang('Overview')</span>
                            </a>

                        </li>
                        <li>
                            <a href="#sidebar-discover-Admin" class="glyphicons user" data-toggle="sidebar-discover"><span class="badge pull-right badge-primary hidden-md"></span><i></i><span>@lang('Admin')</span></a>
                            <div id="sidebar-discover-Admin" class="sidebar-discover-menu">
                                <div class="innerAll text-center border-bottom text-muted-dark">
                                    <strong>@lang('Admin')</strong>
                                    <button class="btn btn-xs btn-default close-discover"><i class="fa fa-fw fa-times"></i></button>
                                </div>
                                <ul class="animated fadeIn">
                                    @if($response['status']['isSuperAdmin'] || $response['status']['isModerator'])
                                    <li><a href="{{URL::to('admin/createadmin')}}"><i class="fa fa-circle-o"></i>@lang('Create Admin')</a></li>
                                    @endif
                                    <li><a href="{{URL::to('admin/createHotelAdmin')}}"><i class="fa fa-circle-o"></i>@lang('Create HotelAdmin')</a></li>
                                    <li><a href="{{URL::to('admin/manage')}}"><i class="fa fa-circle-o"></i>@lang('Manage Admin')</a></li>

                                </ul>
                            </div>
                        </li>
                        @if(!$response['status']['isModerator'])
                        <li>
                            <a href="#sidebar-discover-hotel" class="glyphicons building" data-toggle="sidebar-discover"><span class="badge pull-right badge-primary hidden-md"></span><i></i><span>@lang('Hotel')</span></a>
                            <div id="sidebar-discover-hotel" class="sidebar-discover-menu">
                                <div class="innerAll text-center border-bottom text-muted-dark">
                                    <strong>@lang('Hotel')</strong>
                                    <button class="btn btn-xs btn-default close-discover"><i class="fa fa-fw fa-times"></i></button>
                                </div>
                                <ul class="animated fadeIn">
                                    <li><a href="{{URL::to('admin/hotel/create')}}"><i class="fa fa-circle-o"></i>@lang('Create Hotel')</a></li>
                                    <li><a href="{{URL::to('admin/hotel/manage ')}}"><i class="fa fa-circle-o"></i>@lang('Manage Hotel')</a></li>
                                </ul>
                            </div>
                        </li>
                        @endif

                        <li>
                            <a href="#sidebar-discover-settings" class="glyphicons wrench" data-toggle="sidebar-discover"><span class="badge pull-right badge-primary hidden-md"></span><i></i><span>@lang('Settings')</span></a>
                            <div id="sidebar-discover-settings" class="sidebar-discover-menu">
                                <div class="innerAll text-center border-bottom text-muted-dark">
                                    <strong>@lang('Settings')</strong>
                                    <button class="btn btn-xs btn-default close-discover"><i class="fa fa-fw fa-times"></i></button>
                                </div>
                                <ul class="animated fadeIn">
                                    <li><a href="{{URL::to('admin/setting/roomtype/create')}}"><i class="fa fa-circle-o"></i>@lang('Create Room type')</a></li>
                                    <li><a href="{{URL::to('admin/setting/roomtype/manage')}}"><i class="fa fa-circle-o"></i>@lang('Room Types')</a></li>
                                    <li><a href="{{URL::to('admin/setting/roomfeature/create')}}"><i class="fa fa-circle-o"></i>@lang('Add Room Feature')</a></li>
                                    <li><a href="{{URL::to('admin/setting/roomfeature/manage')}}"><i class="fa fa-circle-o"></i>@lang('Room Features')</a></li>
                                    <li><a href="{{URL::to('admin/setting/hotelfeature/create')}}"><i class="fa fa-circle-o"></i>@lang('Add Hotel Feature')</a></li>
                                    <li><a href="{{URL::to('admin/setting/hotelfeature/manage')}}"><i class="fa fa-circle-o"></i>@lang('Hotel Features')</a></li>

                                </ul>
                            </div>
                        </li>

                        <li>
                            <a href="#sidebar-discover-room" class="glyphicons home" data-toggle="sidebar-discover"><span class="badge pull-right badge-primary hidden-md"></span><i></i><span>@lang('Room')</span></a>
                            <div id="sidebar-discover-room" class="sidebar-discover-menu">
                                <div class="innerAll text-center border-bottom text-muted-dark">
                                    <strong>@lang('Room')</strong>
                                    <button class="btn btn-xs btn-default close-discover"><i class="fa fa-fw fa-times"></i></button>
                                </div>
                                <ul class="animated fadeIn">
                                    <li><a href="{{URL::to('admin/room/create')}}"><i class="fa fa-circle-o"></i>@lang('Create Room')</a></li>
                                    <li><a href="{{URL::to('admin/room/manage')}}"><i class="fa fa-circle-o"></i>@lang('Manage Room ')</a></li>

                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="#sidebar-discover-voucher" class="glyphicons calculator" data-toggle="sidebar-discover"><span class="badge pull-right badge-primary hidden-md"></span><i></i><span>@lang('Voucher')</span></a>
                            <div id="sidebar-discover-voucher" class="sidebar-discover-menu">
                                <div class="innerAll text-center border-bottom text-muted-dark">
                                    <strong>@lang('Voucher')</strong>
                                    <button class="btn btn-xs btn-default close-discover"><i class="fa fa-fw fa-times"></i></button>
                                </div>
                                <ul class="animated fadeIn">
                                    <li><a href="{{URL::to('admin/vouchers/create')}}"><i class="fa fa-circle-o"></i>@lang('Create Voucher')</a></li>
                                    <li><a href="{{URL::to('admin/vouchers/manage')}}"><i class="fa fa-circle-o"></i>@lang('Manage Voucher ')</a></li>

                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="#sidebar-discover-booking" class="glyphicons book" data-toggle="sidebar-discover"><span class="badge pull-right badge-primary hidden-md"></span><i></i><span>@lang('Booking')</span></a>
                            <div id="sidebar-discover-booking" class="sidebar-discover-menu">
                                <div class="innerAll text-center border-bottom text-muted-dark">
                                    <strong>@lang('Booking')</strong>
                                    <button class="btn btn-xs btn-default close-discover"><i class="fa fa-fw fa-times"></i></button>
                                </div>
                                <ul class="animated fadeIn">
                                    <li><a href="{{URL::to('admin/booking/create')}}"><i class="fa fa-circle-o"></i>@lang('Create Booking')</a></li>
                                    <li><a href="{{URL::to('admin/booking/manage')}}"><i class="fa fa-circle-o"></i>@lang('Manage Booking ')</a></li>

                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>


</div>