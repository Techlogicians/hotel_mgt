<div class="navbar hidden-print main" role="navigation">
    <div class="user-action user-action-btn-navbar pull-left border-right">
        <button class="btn btn-sm btn-navbar btn-inverse btn-stroke"><i class="fa fa-bars fa-2x"></i></button>
    </div>



    <ul class="main pull-right" >
        {{--dropdown notif notifications hidden-xs--}}
        <li class="" style="display:none;">
            <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-bell-fill"></i> <span class="label label-danger">5</span></a>

            <ul class="dropdown-menu chat media-list pull-right">
                <li class="media">
                    <a class="pull-left" href="#"><img class="media-object thumb" src="../assets/images/people/100/15.jpg" alt="50x50" width="50"/></a>
                    <div class="media-body">
                        <span class="label label-default pull-right">5 min</span>
                        <h5 class="media-heading">Adrian D.</h5>
                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </li>
                <li class="media">
                    <a class="pull-left" href="#"><img class="media-object thumb" src="../assets/images/people/100/16.jpg" alt="50x50" width="50"/></a>
                    <div class="media-body">
                        <span class="label label-default pull-right">2 days</span>
                        <h5 class="media-heading">Jane B.</h5>
                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </li>
                <li class="media">
                    <a class="pull-left" href="#"><img class="media-object thumb" src="../assets/images/people/100/17.jpg" alt="50x50" width="50"/></a>
                    <div class="media-body">
                        <span class="label label-default pull-right">3 days</span>
                        <h5 class="media-heading">Andrew M.</h5>
                        <p class="margin-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </li>
                <li><a href="#" class="btn btn-primary"><i class="fa fa-list"></i> <span>View all messages</span></a></li>
            </ul>
        </li>

        <li class="dropdown username">
            <a href="" class="dropdown-toggle" data-toggle="dropdown">{{$response['status']['cuser']['username']}} <span class="caret"></span></a>

            <ul class="dropdown-menu pull-right">
                 <li><a href="{{URL::to('admin/changePassword')}}" class="glyphicons keys no-ajaxify"><i></i>Change Password</a></li>
                <li><a href="{{URL::to('admin/logout')}}" class="glyphicons log_out no-ajaxify"><i></i>Logout</a></li>
            </ul>
        </li>
    </ul>
</div>
