@extends('admin.layout.main_layout')
@section('content')


    <script src="{{URL::to('public/Plugins/datetimepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{URL::to('public/Plugins/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
    <link href="{{URL::to('public/Plugins/datetimepicker/bootstrap-datetimepicker.css')}}" type="text/css"
          rel="stylesheet"/>
    <link href="{{URL::to('public/Plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" type="text/css"
          rel="stylesheet"/>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/select2/assets/lib/js/select2.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/select2/assets/custom/js/select2.init.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <style>
        .cstm-invisible{
            padding: 0px !important;
        }
    </style>
    <h2 class="margin-none">Create Booking&nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>
    <div class="panel panel-default col-md-8">
        {!! Breadcrumbs::render('createbooking') !!}
        <div class="panel-body">
            <form action="{{URL::to('admin/booking/save')}}" method="Post" enctype="multipart/form-data" id="form">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token()}}">
                <?php if($errors->first() != ""){?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                    {{ $errors->first() }}
                </div>
                <?php }?>
                <div class="form-group">
                    <label>Booked By: </label>

                    <select name="booked_by"  class="form-control cstm-invisible" id="select2_1">
                        <option></option>
                        @foreach($users as $user)
                            <option value="{{$user['user_id']}}">{{$user['user_email']}}</option>
                        @endforeach
                    </select>
                </div>
                @if($response['status']['isHotelOwner'])
                    <div class="form-group" >
                        <label>Hotel Name: </label>
                        <select name="hotel_name" class="form-control" id="hotel_id">
                            <option></option>
                            @foreach($hotels as $hotel)
                                <option value="{{$hotel['hotel_id']}}">{{$hotel['hotel_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                @elseif($response['status']['isSuperAdmin'])
                    <div class="form-group">
                        <label>Hotel Name: </label>
                        <select name="hotel_name" class="form-control" id="hotel_id">
                            <option></option>
                            @foreach($hotels as $hotel)
                                <option value="{{$hotel['hotel_id']}}">{{$hotel['hotel_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div class="form-group">
                    <label>Room: </label>
                    <select name="room_name" class="form-control" id="room_id">

                    </select>
                </div>
                <div class="form-group">
                    <label>Book Start Date: </label>

                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' name="booking_start_date" class="form-control"/>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>

                </div>
                <div class="form-group">
                    <label>Book End Date: </label>

                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' name="booking_end_date" class="form-control"/>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>

                </div>
                <div class="form-group">
                    <label>Status: </label>
                    <select name="status" class="form-control">
                        <option value="payment_due">Payment Due</option>
                        <option value="confirmed">Confirmed</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" name="register" id="register"
                           value="Create Booking"/>
                </div>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $(function () {
                $('#datetimepicker1').datepicker({
                    format: 'yyyy-mm-dd'
                });
                $('#datetimepicker2').datepicker({
                    format: 'yyyy-mm-dd'
                });

                $('#hotel_id').change(
                        function()
                        {
                            var hotel_id = this.value;
                            var token = $('#token').val();
                            $.ajax({
                                url: $("#base_url").val() + "/admin/booking/hotel/roomlist",
                                type: 'POST',
                                data: {id: hotel_id,_token:token},
                                dataType:'json',
                                success: function(result) {
                                    if (result.status)
                                    {
                                        console.log(result.html);
                                        $('#room_id').html(result.html);

                                    }
                                }
                            });
                        }
                );
            });
        });
    </script>

@stop