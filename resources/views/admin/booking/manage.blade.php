@extends('admin.layout.main_layout')
@section('content')
    <h2 class="margin-none">Booking List &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i> </h2>
    <div class="row">
        {!! Breadcrumbs::render('bookingmanage') !!}
        <div class="widget widget-body-white widget-heading-simple">
            <div class="widget-body">
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token()}}">
                    <table class="display" cellspacing="0" width="100%" id="myTable">
                        <thead class="bg-gray">
                        <th>Id</th>
                        <th>Booked by</th>
                        <th>Hotel</th>
                        <th>Room</th>
                        <th>From Date</th>
                        <th>To Data</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @foreach($bookings as $item)

                            <tr>
                                <td>{{$ct= $item->booking_id}} </td>
                                <td>{{$item->users->user_email}} </td>
                                <td>{{$item->hotels->hotel_name}} </td>
                                <td>
                                    {{$item->hotel_rooms->hr_room_title}}
                                </td>
                                <td>{{$item->booking_start_date}} </td>
                                <td>{{$item->booking_end_date}} </td>
                                <td>{{$item->booking_status}} </td>
                                <td>
                                    <a href="{{URL::to('/admin/booking/edit/').'/'.$ct}}" title="edit"
                                       class="btn btn-circle btn-success"><i class="icon-compose"></i></a>
                                    <a onclick="myfunction('{{$ct}}');" class="btn btn-circle btn-danger"><i
                                                class="fa fa-trash-o"></i></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/js/jquery.dataTables.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/TableTools/media/js/TableTools.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/ColVis/media/js/ColVis.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/custom/js/DT_bootstrap.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/custom/js/datatables.init.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/FixedHeader/FixedHeader.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/tables/datatables/assets/lib/extras/ColReorder/media/js/ColReorder.min.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script>

        $(document).ready(function () {
            $('#myTable').DataTable();
        });
    </script>
    <script>
        function myfunction(id) {

            if (confirm("Do you want to delete this item?") == true) {

                window.location.href = $("meta[name='baseUrl']").attr("content") + "/admin/booking/delete/" + id;
            } else {
                return false;
            }

        }
    </script>
@stop