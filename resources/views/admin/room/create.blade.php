@extends('admin.layout.main_layout')
@section('content')
    <script src="{{URL::to('public/Plugins/RTE/editor.js')}}"></script>
    <link href="{{URL::to('public/Plugins/RTE/editor.css')}}" type="text/css" rel="stylesheet"/>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#txtEditor").Editor();

        });
    </script>
    <h2 class="margin-none">Create Hotel Room &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>
    <div class="panel panel-default col-md-8">
        {!! Breadcrumbs::render('createroom') !!}
        <div class="panel-body">
            <form action="{{URL::to('admin/room/save')}}" method="Post" enctype="multipart/form-data" id="form">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <?php if($errors->first() != ""){?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                    {{ $errors->first() }}
                </div>
                <?php }?>
                <div class="form-group">
                    <label>Hotel Name: </label>
                    <select name="hotel" class="form-control">
                        @foreach($hotels as $hotel)
                            <option value="{{$hotel['hotel_id']}}">{{$hotel['hotel_name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Room Title: </label>
                    <input type="text" class="form-control" name="room_title" id="hotel_name" value=""/>
                </div>
                <div class="form-group">
                    <label>Room Size: </label>
                    <input type="text" class="form-control" name="room_size" id="hotel_description" value=""/>
                </div>
                <div class="form-group">
                    <label>Room Type: </label>
                    <select name="room_type" class="form-control">
                        @foreach($room_types as $room)
                            <option value="{{$room['ads_value']}}">{{$room['ads_value']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Room For: </label>

                    <div>
                        <span>Adult:</span> <input type="text" class="form-control" name="adult" id="hotel_type" value=""/>
                        <span>Children:</span><input type="text" class="form-control" name="child" id="hotel_type" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label>Room Description: </label>
                    <textarea id="txtEditor" ></textarea>
                </div>
                <div class="form-group">
                    <label>Room Features: </label>
                    <select multiple id="multiselect-custom" class="multiselect" name="roomfeatures[]">
                        @foreach($room_features as $room)
                            <option value="{{$room['ads_value']}}">{{$room['ads_value']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Room Price: </label>
                    <input type="text" class="form-control" name="room_price" id="hotel_city" value=""/>
                </div>
                <div class="form-group">
                    <label>Room Images: </label>

                    <div class="images">
                        <input type="file" class="form-control files" name="images[]" id="hotel_zip" value=""/>
                    </div>
                    <a data-role="button" class="btn btn-file" id="addImage"> Add More </a>
                </div>
                <textarea id="description" name="description" style="display: none;"></textarea>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block"  name="register" id="register"
                           value="Create Room"/>
                </div>
            </form>
        </div>
    </div>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/multiselect/assets/lib/js/jquery.multi-select.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/multiselect/assets/custom/js/multiselect.init.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script>
        $("#addImage").click(function () {
            if ($(".files").last().val() != "") {
                $(".images").append('<input type="file" class="form-control files" name="images[]" id="hotel_zip" value=""/>');
            }
        })
      $("#form").submit(function(){
          $("#description").val($(".Editor-editor").html());


      })
    </script>






@stop

