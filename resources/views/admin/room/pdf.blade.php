
    <h2 class="margin-none"><i class="fa fa-fw fa-list text-muted"></i>&nbsp;Room List </h2>
    <div class="row">
        {!! Breadcrumbs::render('manageroom') !!}
        <div class="widget widget-body-white widget-heading-simple">
            <div class="widget-body">
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token()}}">
                    <table class="display" cellspacing="0" width="100%" id="myTable">
                        <thead class="bg-gray">
                        <tr>
                            <th>Id</th>
                            <th>Hotel</th>
                            <th>Room</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($rooms as $item)

                            <tr>
                                <td>{{$ct= $item->hr_id}} </td>
                                <td>{{$item->hotels->hotel_name}} </td>
                                <td>{{$item->hr_room_title}} </td>
                                <td>
                                    @if($item->hr_room_status=="active")
                                        <span id='status_{{$ct}}'>Active </span>
                                    @endif
                                    @if($item->hr_room_status=="inactive")
                                        <span id='status_{{$ct}}'>Inactive </span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{URL::to('/admin/room/edit/').'/'.$ct}}" title="edit" class="btn btn-circle btn-success"><i class="icon-compose"></i></a>
                                    <a onclick="myfunction('{{$ct}}');" class="btn btn-circle btn-danger"><i class="fa fa-trash-o"></i></a>
                                    {{--<a href="{{URL::to('/admin/hotel/details/').'/'.$ct}}" title="details" class="btn btn-circle btn-success"><i class="fa fa-align-justify"></i></a>--}}
                                    @if($item->hr_room_status=="active")

                                        <a id="block_hotel_{{$ct}}" href="javascript:void(0)" onclick="block_hotel('{{$ct}}')" title="Block" class="btn btn-circle btn-success"><i class="icon-lock-fill"></i></a>
                                        <a id="unblock_hotel_{{$ct}}"  onclick="unblock_hotel('{{$ct}}')" style="display:none;" title="Unblock" class="btn btn-circle btn-success"><i class="icon-unlock-fill"></i></a>
                                    @endif
                                    @if($item->hr_room_status=="inactive")

                                        <a id="unblock_hotel_{{$ct}}" onclick="unblock_hotel('{{$ct}}')"  title="Unblock" class="btn btn-circle btn-success"><i class="icon-unlock-fill"></i></a>
                                        <a id="block_hotel_{{$ct}}" onclick="block_hotel('{{$ct}}')" style="display:none;" title="Block" class="btn btn-circle btn-success"><i class="icon-lock-fill"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
