@extends('admin.layout.main_layout')
@section('content')
    <script src="{{URL::to('public/Plugins/RTE/editor.js')}}"></script>
    <link href="{{URL::to('public/Plugins/RTE/editor.css')}}" type="text/css" rel="stylesheet"/>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtEditor").Editor();
            $(".Editor-editor").html($("#description").val())
        });
    </script>
    <h2 class="margin-none"><i class="fa fa-fw fa-pencil text-muted"></i>&nbsp;Edit Hotel </h2>
    <div class="panel panel-default col-md-8">
        {!! Breadcrumbs::render('editroom',$room) !!}
        <div class="panel-body">
            <form action="{{URL::to('admin/room/update/'.$room->hr_id)}}" method="Post" enctype="multipart/form-data" id="form">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token()}}">
                <?php if($errors->first() != ""){?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                    {{ $errors->first() }}
                </div>
                <?php }?>
                <div class="form-group">
                    <label>Hotel Name: </label>
                    <select name="hotel" class="form-control">
                        @foreach($hotels as $hotel)
                            <option value="{{$hotel['hotel_id']}}" <?php if ($room->hotels->hotel_id == $hotel['hotel_id']) {
                                echo "Selected";
                            }?>>{{$hotel['hotel_name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Room Title: </label>
                    <input type="text" class="form-control" name="room_title" id="hotel_name"
                           value="{{$room->hr_room_title}}"/>
                </div>
                <div class="form-group">
                    <label>Room Size: </label>
                    <input type="text" class="form-control" name="room_size" id="hotel_description"
                           value="{{$room->hr_room_size}}"/>
                </div>
                <div class="form-group">
                    <label>Room Type: </label>
                    <select name="room_type" class="form-control">
                        @foreach($room_types as $r)
                            <option value="{{$r['ads_value']}}"<?php if ($room->hr_room_type == $r['ads_value']) {
                                echo "Selected";
                            }?>>{{$r['ads_value']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Room For: </label>

                    <div>
                        <span>Adult:</span> <input type="text" class="form-control" name="adult" id="hotel_type"
                                                   value="{{$room->hr_adult}}"/>
                        <span>Children:</span><input type="text" class="form-control" name="child" id="hotel_type"
                                                     value="{{$room->hr_children}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label>Room Description: </label>
                    <textarea id="txtEditor"></textarea>
                </div>
                <div class="form-group">
                    <label>Room Features: </label>
                    <select multiple id="multiselect-custom" class="multiselect" name="roomfeatures[]">
                        <?php $feature= json_decode($room->hr_room_features)?>
                        @foreach($room_features as $r)
                            <option value="{{$r['ads_value']}}" @if(in_array($r['ads_value'], $feature)){{"Selected"}}@endif>{{$r['ads_value']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Room Price: </label>
                    <input type="text" class="form-control" name="room_price" id="hotel_city"
                           value="{{$room->hr_price}}"/>
                </div>
                <div class="form-group">
                    <p>Room Images: </p>

                    <?php $files = json_decode($room->hr_room_images);
                    $count = count($files);
                    for($i = 0;$i < $count;$i++)
                    {?>
                    <div class="photos" id="{{$i}}">
                        <img class="Image" src="{{url::to('public/images/rooms/'.$files[$i])}}" width="180"
                             height="150"/>

                        <span class="close-btn"><a href="javascript:void(0)" class="cross" data-id="{{$i}}"
                                                   data-room-id="{{$room->hr_id}}"
                                                   data-value="{{$files[$i]}}">X</a></span>

                    </div>

                    <?php } ?>

                    <div class="images">

                    </div>
                    <a data-role="button" class="btn btn-file" id="addImage"> Add More </a>
                </div>
                <textarea id="description" name="description"
                          style="display: none;">{{$room->hr_room_description}}</textarea>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" name="register" id="register"
                           value="Save Changes"/>
                </div>
            </form>
        </div>
    </div>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/multiselect/assets/lib/js/jquery.multi-select.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script src="{{URL::to('resources/assets/components/common/forms/elements/multiselect/assets/custom/js/multiselect.init.js?v=v1.0.2&sv=v0.0.1')}}"></script>
    <script>
        $("#addImage").click(function () {
            if ($(".files").last().val() != "") {
                $(".images").append('<input type="file" class="form-control files" name="images[]" id="hotel_zip" value=""/>');
            }
        })
        $("#form").submit(function () {
            $("#description").val($(".Editor-editor").html());


        })
        $(".photos").hover(function () {

            $(this).children(".close-btn").show();

        }, function () {
            $(this).find(".close-btn").hide();
        })
        $(".cross").click(function () {
            var id = $(this).attr("data-room-id");
            var photo = $(this).attr("data-id");
            var name = $(this).attr("data-value");
            var token = $('#_token').val();
            var data = {id: id, name: name, _token: token};

            $.ajax({
                type: "POST",
                url: $("#base_url").val() + "/admin/room/removeimage",
                data: data,
                success: function (data) {
                    $("#" + photo).hide("slow");
                }
            });
        })
    </script>


    <style>
        .photos {
            float: left;
            border: 1px solid #111;
            position: relative;
        }

        .close-btn {
            border: 2px solid #c2c2c2;
            position: absolute;
            padding: 1px 5px;
            top: 7px;
            background-color: #605F61;
            right: 7px;
            border-radius: 20px;
            display: none;
        }

        .close-btn a {
            font-size: 15px;
            font-weight: bold;
            color: white;
            text-decoration: none;
        }

    </style>



@stop

