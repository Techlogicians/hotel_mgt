@extends('admin.layout.main_layout')
@section('content')
    <h2 class="margin-none">Edit Room Feature &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h2>

    <div class="separator-h"></div>
    <div class="panel panel-default col-md-8">
        {!! Breadcrumbs::render('edithotelfeature',$setting) !!}
        <div class="panel-body">
            <form action="{{URL::to('admin/setting/hotelfeature/update/'.$setting->ads_id)}}" method="Post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <?php if($errors->first()!=""){?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                    {{ $errors->first() }}
                </div>
                <?php }?>
                <div class="form-group">
                    <label>Room Type </label>
                    <input type="text" class="form-control" name="room_feature" id="room_feature" value="{{$setting->ads_value}}" value=""/>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" name="register" id="register" value="Save Change"/>
                </div>
            </form>
        </div>
    </div>

@stop

