@extends('user.layout.main_layout')

@section('content')
    <!-- Parallax Effect -->
    <script type="text/javascript">$(document).ready(function () {
            $('#parallax-pagetitle').parallax("50%", -0.55);
        });</script>

    <section class="parallax-effect">
        <div id="parallax-pagetitle" style="background-image: url(./resources/assets/frontend/images/parallax/parallax-01.jpg);">
            <div class="color-overlay">
                <!-- Page title -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <ol class="breadcrumb">
                                <li><a href="index.html">Home</a></li>
                                <li><a href="room-list.html">Room list view</a></li>
                                <li class="active">Room detail</li>
                            </ol>
                            <h1>Room detail</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container mt50">
        <div class="row">
            <div id="confirmation" class="alert alert-info" style="display: none;"></div>
            <!-- Slider -->
            <section class="standard-slider room-slider">
                <div class="col-sm-12 col-md-8">
                    <div id="owl-standard" class="owl-carousel">
                        <?php
                        $images = json_decode($room->hr_room_images);
                        ?>
                        @foreach($images as $image)
                            <div class="item"><a href="{{URL::to('public/images/rooms/'.$image)}}"
                                                 data-rel="prettyPhoto[gallery1]"><img
                                            src="{{URL::to('public/images/rooms/'.$image)}}" alt="Image 2"
                                            class="img-responsive"></a></div>
                        @endforeach
                    </div>
                </div>
            </section>

            <!-- Reservation form -->
            <section id="reservation-form" class="mt50 clearfix">
                <div class="col-sm-12 col-md-4">
                    <form class="reservation-vertical clearfix" role="form" method="post" action="php/reservation.php"
                          name="reservationform" id="reservationform">
                        <h2 class="lined-heading"><span>Reservation</span></h2>

                        <div class="price">
                            <h4>{{$room->hr_room_title}}</h4>
                            &euro; {{$room->hr_price}},-<span> a night</span></div>
                        <input type="hidden" value="{{$room->hr_price}}" id="hr_price">

                        <div id="message"></div>
                        <!-- Error message display -->
                        {{--<div class="form-group">--}}
                        {{--<label for="email" accesskey="E">E-mail</label>--}}
                        {{--<input name="email" type="text" id="email" value="" class="form-control" placeholder="Please enter your E-mail"/>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <select class="hidden" name="room" id="room" disabled="disabled">
                                <option selected="selected">{{$room->hr_room_type}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="checkin">Check-in</label>

                            <div class="popover-icon" data-container="body" data-toggle="popover" data-trigger="hover"
                                 data-placement="right" data-content="Check-In is from 11:00"><i
                                        class="fa fa-info-circle fa-lg"> </i></div>
                            <i class="fa fa-calendar infield"></i>
                            <input name="checkin" type="text" id="checkin" value="" class="form-control"
                                   placeholder="Check-in"/>
                        </div>
                        <div class="form-group">
                            <label for="checkout">Check-out</label>

                            <div class="popover-icon" data-container="body" data-toggle="popover" data-trigger="hover"
                                 data-placement="right" data-content="Check-out is from 12:00"><i
                                        class="fa fa-info-circle fa-lg"> </i></div>
                            <i class="fa fa-calendar infield"></i>
                            <input name="checkout" type="text" id="checkout" value="" class="form-control"
                                   placeholder="Check-out"/>
                        </div>
                        <div class="form-group">
                            <div class="guests-select">
                                <label>Guests</label>
                                <i class="fa fa-user infield"></i>

                                <div class="total form-control" id="test">1</div>
                                <div class="guests">
                                    <div class="form-group adults">
                                        <label for="adults">Adults</label>

                                        <div class="popover-icon" data-container="body" data-toggle="popover"
                                             data-trigger="hover" data-placement="right" data-content="+15 years"><i
                                                    class="fa fa-info-circle fa-lg"> </i></div>
                                        <select name="adults" id="adults" class="form-control">
                                            <option value="1">1 adult</option>
                                            <option value="2">2 adults</option>
                                            <option value="3">3 adults</option>
                                        </select>
                                    </div>
                                    <div class="form-group children">
                                        <label for="children">Children</label>

                                        <div class="popover-icon" data-container="body" data-toggle="popover"
                                             data-trigger="hover" data-placement="right" data-content="0 till 15 years">
                                            <i class="fa fa-info-circle fa-lg"> </i></div>
                                        <select name="children" id="children" class="form-control">
                                            <option value="0">0 children</option>
                                            <option value="1">1 child</option>
                                            <option value="2">2 children</option>
                                            <option value="3">3 children</option>
                                        </select>
                                    </div>
                                    <button type="button" class="btn btn-default button-save btn-block">Save</button>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary btn-block" id="bookBtn" data-toggle="modal"
                                @if( $LoggedIn)data-target="#bookingModal" @else data-target="#loginModal"@endif>Book
                            Now
                        </button>
                    </form>
                </div>
            </section>

            <!-- Room Content -->
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 mt50">
                            <h2 class="lined-heading"><span>Room Details</span></h2>

                            <h3 class="mt50">Room overview</h3>
                            <table class="table table-striped mt30">
                                <tbody>
                                <?php
                                $i = 0;
                                $facilities = json_decode($room->hr_room_features);?>
                                <tr>
                                    <?php foreach($facilities as $key=>$facility){
                                    $i++;
                                    ?>


                                    <td><i class="fa fa-check-circle"></i> {{$facility}}</td>
                                    <?php if($i % 3 == 0){ ?>
                                </tr>
                                <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>
                            <p class="mt501">{!!$room->hr_room_description !!}</p>
                        </div>
                        <div class="col-sm-5 mt50">
                            <h2 class="lined-heading"><span>Overview</span></h2>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#overview" data-toggle="tab">Overview</a></li>
                                <li><a href="#facilities" data-toggle="tab">Facilities</a></li>
                                <li><a href="#extra" data-toggle="tab">Extra</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="overview">
                                    <p class="mt501">
                                        Adults : {{$room->hr_adult}} <br>
                                        Childrens : {{$room->hr_children}}
                                    </p>

                                </div>
                                <div class="tab-pane fade" id="facilities">
                                    <p class="mt501">
                                        <?php

                                        $facilities = json_decode($room->hr_room_features);?>

                                        <?php foreach($facilities as $key=>$facility){
                                        ?>


                                    <div><i class="fa fa-check-circle"></i> {{$facility}}</div>


                                    <?php } ?>

                                    </p>
                                </div>
                                <div class="tab-pane fade" id="extra"><p class="mt501">Aa vestibulum risus mattis vitae.
                                        Aliquam vitae varius elit, non facilisis massa. Vestibulum luctus diam mollis
                                        gravida bibendum. Aliquam mattis velit dolor, sit amet semper erat auctor vel.
                                        Integer auctor in dui ac vehicula. Integer fermentum nunc ut arcu feugiat, nec
                                        placerat nunc tincidunt. Pellentesque in massa eu augue placerat cursus sed quis
                                        magna.</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <!-- Other Rooms -->
    <section class="rooms mt50">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="lined-heading"><span>Other rooms</span></h2>
                </div>
                @if(!empty($otherRooms))
                    @foreach($otherRooms as $item)
                        <div class="col-sm-4">
                            <div class="room-thumb">
                                @if($item->hr_room_images=='')
                                <img src="{{URL::to('resources/assets/frontend/images/rooms/356x228.gif')}}"
                                        alt="room 2" class="img-responsive"/>
                                @else
                                    <?php  $image = json_decode($item->hr_room_images);?>
                                    <img src="{{URL::to('public/images/rooms/'.$image[0])}}"
                                         alt="room 2" class="img-responsive"/>
                                @endif
                                <div class="mask">
                                    <div class="main">
                                        <h5>{{$item->hr_room_title}} </h5>

                                        <div class="price">&euro; {{$item->hr_price}}<span>a night</span></div>
                                    </div>
                                    <div class="content">
                                        <?php $str = substr($item->hr_room_description,0,strpos($item->hr_room_description,' ',200)) . "..."; ?>
                                        <p>
                                            {!!html_entity_decode($str)!!}
                                        </p>

                                        <div class="row">
                                            <div class="col-xs-6">
                                                <ul class="list-unstyled">
                                                    <?php $fac= json_decode($item->hr_room_features); $count = count($fac);  ?>
                                                        @for($i=0;$i<round($count/2);$i++)
                                                            <li><i class="fa fa-check-circle"></i> {{$fac[$i]}}</li>
                                                        @endfor
                                                </ul>
                                            </div>
                                            <div class="col-xs-6">
                                                <ul class="list-unstyled">
                                                    @for($i=round($count/2);$i<$count;$i++)
                                                        <li><i class="fa fa-check-circle"></i> {{$fac[$i]}}</li>
                                                    @endfor
                                                </ul>
                                            </div>
                                        </div>
                                        <a href="{{URL::to('/roomdetails/'.$item->hr_id)}}" class="btn btn-primary btn-block">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- Booking Modal Starts!-->
    <div id="bookingModal" class="modal fade" role="dialog" style="top:100px!important;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Booking Confirmation</h4>
                </div>
                <div class="alert-danger" id="msg" style="display: none;"></div>
                <div class="modal-body">
                    Hello {{$user['username']}},<br>
                    Check the following list for your billing details.Please confirm to continue booking process.
                    <div class="row">
                        <div class="col-sm-6">

                            <p><b>Booking Start:</b></p>

                            <p><b>Booking End:</b></p>

                            <p><b>Total Days:</b></p>

                            <p><b>Rate:</b></p>

                            <p><b>Total Amount:</b></p>

                            <p><b>Total guests</b></p>
                        </div>
                        <div class="col-sm-6">
                            <p id="start"></p>

                            <p id="end"></p>

                            <p id="days"></p>

                            <p id="rate"></p>

                            <p id="amount"></p>

                            <p id="guests"></p>
                        </div>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" id="confirm" class="btn btn-info" style="display: none">Confirm</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Booking Modal Ends!-->


    <!-- Login Modal Starts!-->
    <div id="loginModal" class="modal fade" role="dialog" style="top:100px!important;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title alert-warning alert-bg-remove">Error!!</h4>
                </div>
                <div class="modal-body  alert-warning alert-bg-remove">
                    <b>You are not logged in!!</b> Please Login to continue.
                </div>
                <div class="modal-footer">
                    <a class="btn btn-info" href="{{URL::to('login')}}">Log In</a>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Login  Modal Ends!-->

    <input type="hidden" value="{{$room->hr_id}}" id="roomid">
    <input type="hidden" value="{{$room->hr_adult}}" id="hr_adult">
    <input type="hidden" value="{{$room->hr_children}}" id="hr_child">
    <input type="hidden" value="{{$room->hr_hotel_id}}" id="hotelid">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

    <script>
        $("#bookBtn").click(function () {
            var date1 = new Date($("#checkin").val());
            var date2 = new Date($("#checkout").val());
            var rate = $("#hr_price").val();
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            var guests = $(".total").text();
            var adult = $("#hr_adult").val();
            var child = $("#hr_child").val();
            var start = $("#checkin").val();
            var end = $("#checkout").val();

            if (start == "") {
                $("#msg").text("Error!! Check In Date is empty").show("slow");

            }
            else if (end === "") {
                $("#msg").text("Error!!Check Out Date is empty").show("slow");
            }
            else if ($("#children").val() > child) {
                $("#msg").text("There is space for " + child + " children").show("slow");
            }
            else if ($("#adults").val() > adult) {
                $("#msg").text("There is space for " + adult + " adults").show("slow");
            }
            else {
                $("#msg").hide();
                $("#start").text($("#checkin").val());
                $("#end").text($("#checkout").val());
                $("#days").text(diffDays);
                $("#rate").text(rate);
                $("#amount").text(rate * diffDays);
                $("#guests").text(guests + ' (' + $("#children").val() + ' children ' + $("#adults").val() + " Adults " + ')')
                $("#confirm").show();
            }


        })
        $("#confirm").click(function () {
            var start = $("#checkin").val();
            var end = $("#checkout").val();
            var room = $("#roomid").val();
            var hotel = $("#hotelid").val();
            var token = $("#token").val();
            var data = {start: start, end: end, room: room, hotel: hotel, _token: token}

            $.ajax({
                type: "POST",
                url: $("#base_url").val() + "/createbooking",
                data: data,
                datatype: "JSON",
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    $(".close").trigger("click");
                    $("#confirmation").text(obj.msg).show("slow");
                    $("body").scroll($("#confirmation").height())
                    setInterval(function () {
                        $("#confirmation").hide("slow")
                    }, 15000);
                }
            });


        })
    </script>

@stop