@extends('user.layout.main_layout')

@section('content')
    <!-- Parallax Effect -->
    <script type="text/javascript">$(document).ready(function () {
            $('#parallax-pagetitle').parallax("50%", -0.55);
        });</script>

    <section class="parallax-effect">
        <div id="parallax-pagetitle" style="background-image: url(./resources/assets/frontend/images/parallax/parallax-01.jpg);">
            <div class="color-overlay">
                <!-- Page title -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <ol class="breadcrumb">
                                <li><a href="index.html">Home</a></li>
                                <li class="active">Rooms list view</li>
                            </ol>
                            <h1>Rooms list view</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Filter -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="nav nav-pills" id="filters">
                    <li class="active"><a href="#" data-filter="*">All</a></li>
                    @foreach($roomtypes as $roomtype)

                    <li><a href="#" data-filter=".{{$roomtype->ads_value}}">{{$roomtype->ads_value}}</a></li>

                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <!-- Rooms -->
    <section class="rooms mt100">
        <div class="container">
            <div class="row room-list fadeIn appear">

                @foreach($roomlist as $room)
                <!-- Room -->
                <div class="col-sm-4 {{$room->hr_room_type}}">
                    <?php
                    $photos = json_decode($room->hr_room_images);
                    $display_image = $photos[0];
                    ?>
                    <div class="room-thumb"> <img src="{{URL::to('public/images/rooms/'.$display_image)}}" alt="room 1" class="img-responsive" />
                        <div class="mask">
                            <div class="main">
                                <h5>{{$room->hr_room_title}}</h5>
                                <div class="price">&euro; {{$room->hr_price}}<span>a night</span></div>
                            </div>
                            <div class="content">
                                {{--<p>{{$room->hr_room_description}}</p>--}}
                                <div class="row">
                                    <div class="col-xs-6">
                                        <ul class="list-unstyled">
                                            <?php $facilaties = json_decode($room->hr_room_features);
                                            $count   = count($facilaties);

                                            ?>
                                            @for($i=0;$i<round($count/2);$i++)
                                                <li><i class="fa fa-check-circle"></i> {{$facilaties[$i]}}</li>
                                            @endfor
                                        </ul>
                                    </div>
                                    <div class="col-xs-6">
                                        <ul class="list-unstyled">
                                            @for($i=round($count/2);$i<$count;$i++)
                                                <li><i class="fa fa-check-circle"></i> {{$facilaties[$i]}}</li>
                                            @endfor
                                        </ul>
                                    </div>
                                </div>
                                <a href="{{URL::to('/roomdetails')}}{{"/".$room->hr_id}}" class="btn btn-primary btn-block">Book Now</a> </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>

@stop