@extends('user.layout.main_layout')

@section('content')
    <!-- Parallax Effect -->
    <script type="text/javascript">$(document).ready(function () {
            $('#parallax-pagetitle').parallax("50%", -0.55);
        });</script>

    <section class="parallax-effect">
        <div id="parallax-pagetitle" style="background-image: url(./resources/assets/frontend/images/parallax/parallax-01.jpg);">
            <div class="color-overlay">
                <!-- Page title -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <ol class="breadcrumb">
                                <li><a href="index.html">Home</a></li>
                                <li><a href="room-list.html">Hotels</a></li>
                                <li class="active">Hotel detail</li>
                            </ol>
                            <h1>Hotel detail</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container mt50">
        <div class="row">
            <div id="confirmation" class="alert alert-info" style="display: none;"></div>
            <!-- Slider -->
            <section class="standard-slider room-slider">
                <div class="col-sm-12 col-md-8">
                    <div id="owl-standard" class="owl-carousel">

                        <div class="item"><a href="{{URL::to('public/images/hotels/'.$hotel->hotel_banner_image)}}"
                                             data-rel="prettyPhoto[gallery1]"><img
                                        src="{{URL::to('public/images/hotels/'.$hotel->hotel_banner_image)}}"
                                        alt="Image 2"
                                        class="img-responsive"></a></div>
                    </div>
                </div>
            </section>


            <div class="col-sm-12 col-md-4 room-thumb1 mt52">
                <h3 class="lined-heading"><span>{{$hotel->hotel_name}}</span></h3>

                <div class="col-md-6">
                    <div class="form-group cstm-pro">
                        <label for="email" accesskey="E">Address</label>

                        <p>{{$hotel->hotel_address}}</p>
                    </div>
                    <div class="form-group cstm-pro">
                        <label for="email" accesskey="E">City</label>

                        <p>{{$hotel->hotel_city}}</p>
                    </div>
                    <div class="form-group cstm-pro">
                        <label for="email" accesskey="E">Country</label>

                        <p>{{$hotel->hotel_country}}</p>
                    </div>

                    <div class="form-group cstm-pro">
                        <label for="email" accesskey="E">Zip</label>

                        <p>{{$hotel->hotel_zip}}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group cstm-pro">
                        <label for="email" accesskey="E">Website</label>

                        <p>{{$hotel->hotel_website}}</p>
                    </div>

                </div>


            </div>

            <!-- Room Content -->
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 mt50">
                            <h2 class="lined-heading"><span>Hotel Details</span></h2>

                            <h3 class="mt50">Hotel overview</h3>
                            <table class="table table-striped mt30">
                                <tbody>
                                <?php
                                $i = 0;
                                $facilities = json_decode($hotel->hotel_facilities);?>
                                <tr>
                                    <?php foreach($facilities as $key=>$facility){
                                    $i++;
                                    ?>


                                    <td><i class="fa fa-check-circle"></i> {{$facility}}</td>
                                    <?php if($i % 3 == 0){ ?>
                                </tr>
                                <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>
                            <p class="mt501">{!!$hotel->hotel_description !!}</p>
                        </div>
                        <div class="col-sm-5 mt50">
                            <h2 class="lined-heading"><span>Overview</span></h2>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#overview" data-toggle="tab">Overview</a></li>
                                <li><a href="#facilities" data-toggle="tab">Facilities</a></li>
                                <li><a href="#extra" data-toggle="tab">Extra</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="overview">
                                    <p class="mt501">
                                        {{--Adults : {{$room->hr_adult}} <br>--}}
                                        {{--Childrens : {{$room->hr_children}}--}}
                                    </p>

                                </div>
                                <div class="tab-pane fade" id="facilities">
                                    <p class="mt501">
                                        <?php

                                        $facilities = json_decode($hotel->hotel_facilities);?>

                                        <?php foreach($facilities as $key=>$facility){
                                        ?>


                                    <div><i class="fa fa-check-circle"></i> {{$facility}}</div>


                                    <?php } ?>

                                    </p>
                                </div>
                                <div class="tab-pane fade" id="extra"><p class="mt501">Aa vestibulum risus mattis vitae.
                                        Aliquam vitae varius elit, non facilisis massa. Vestibulum luctus diam mollis
                                        gravida bibendum. Aliquam mattis velit dolor, sit amet semper erat auctor vel.
                                        Integer auctor in dui ac vehicula. Integer fermentum nunc ut arcu feugiat, nec
                                        placerat nunc tincidunt. Pellentesque in massa eu augue placerat cursus sed quis
                                        magna.</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <!-- Other Rooms -->
    <section class="rooms mt50">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="lined-heading"><span>Room List</span></h2>
                </div>
                @if(!empty($otherRooms))
                    @foreach($otherRooms as $item)
                        <div class="col-sm-4">
                            <div class="room-thumb">
                                @if($item->hr_room_images=='')
                                    <img src="{{URL::to('resources/assets/frontend/images/rooms/356x228.gif')}}"
                                         alt="room 2" class="img-responsive"/>
                                @else
                                    <?php  $image = json_decode($item->hr_room_images);?>
                                    <img src="{{URL::to('public/images/rooms/'.$image[0])}}"
                                         alt="room 2" class="img-responsive"/>
                                @endif
                                <div class="mask">
                                    <div class="main">
                                        <h5>{{$item->hr_room_title}} </h5>

                                        <div class="price">&euro; {{$item->hr_price}}<span>a night</span></div>
                                    </div>
                                    <div class="content">
                                        <?php $str = substr($item->hr_room_description, 0, strpos($item->hr_room_description, ' ', 200)) . "..."; ?>
                                        <p>
                                            {!!html_entity_decode($str)!!}
                                        </p>

                                        <div class="row">
                                            <div class="col-xs-6">
                                                <ul class="list-unstyled">
                                                    <?php $fac = json_decode($item->hr_room_features); $count = count($fac);  ?>
                                                    @for($i=0;$i<round($count/2);$i++)
                                                        <li><i class="fa fa-check-circle"></i> {{$fac[$i]}}</li>
                                                    @endfor
                                                </ul>
                                            </div>
                                            <div class="col-xs-6">
                                                <ul class="list-unstyled">
                                                    @for($i=round($count/2);$i<$count;$i++)
                                                        <li><i class="fa fa-check-circle"></i> {{$fac[$i]}}</li>
                                                    @endfor
                                                </ul>
                                            </div>
                                        </div>
                                        <a href="{{URL::to('/roomdetails/'.$item->hr_id)}}"
                                           class="btn btn-primary btn-block">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>




@stop