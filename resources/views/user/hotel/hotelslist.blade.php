@extends('user.layout.main_layout')

@section('content')
    <!-- Parallax Effect -->
    <script type="text/javascript">$(document).ready(function () {
            $('#parallax-pagetitle').parallax("50%", -0.55);
        });</script>

    <section class="parallax-effect">
        <div id="parallax-pagetitle" style="background-image: url(./resources/assets/frontend/images/parallax/parallax-01.jpg);">
            <div class="color-overlay">
                <!-- Page title -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <ol class="breadcrumb">
                                <li><a href="index.html">Home</a></li>
                                <li class="active">Hotels list view</li>
                            </ol>
                            <h1>Rooms list view</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Filter -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="nav nav-pills">
                    <li @if($type==null) class="active"@endif><a href="{{URL::to('/hotels-list')}}">All</a></li>
                    <li @if($type=='bussinees_hotel') class="active"@endif><a
                                href="{{URL::to('/hotels-list?type=bussinees_hotel')}}">Business Hotel</a></li>
                    <li @if($type=='airport_hotel') class="active"@endif ><a
                                href="{{URL::to('/hotels-list?type=airport_hotel')}}">Airport Hotel</a></li>
                    <li @if($type=='suite_hotel') class="active"@endif><a
                                href="{{URL::to('/hotels-list?type=suite_hotel')}}">Suite Hotel</a></li>
                    <li @if($type=='extended_stay_hotel') class="active"@endif><a
                                href="{{URL::to('/hotels-list?type=extended_stay_hotel')}}">Extended Stay Hotel</a></li>
                    <li @if($type=='resort_hotel') class="active"@endif><a
                                href="{{URL::to('/hotels-list?type=resort_hotel')}}">Resort Hotel</a></li>
                    <li @if($type=='homestay') class="active"@endif><a href="{{URL::to('/hotels-list?type=homestay')}}">Homestay</a>
                    </li>
                    <li @if($type=='timeshare') class="active"@endif><a
                                href="{{URL::to('/hotels-list?type=timeshare')}}">Timeshare</a></li>
                    <li @if($type=='casino') class="active"@endif><a href="{{URL::to('/hotels-list?type=casino')}}">Casino</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Rooms -->




    <section class="rooms mt100">
        <div class="container">
            <div class="row room-list fadeIn appear">

                @foreach($hotels as $Item)
                    <!-- Room -->
                    <div class="col-sm-4 {{$Item->hotel_type}}">
                        <div class="room-thumb">
                            @if($Item->hotel_banner_image=='')
                                <img src="{{URL::to('resources/assets/frontend/images/rooms/356x228.gif')}}"
                                     alt="room 2" class="img-responsive"/>

                            @else
                                <img src="{{URL::to('public/images/hotels/'.$Item->hotel_banner_image)}}"
                                     alt="room 2" class="img-responsive"/>
                            @endif
                            <div class="mask">
                                <div class="main">
                                    <h5>{{$Item->hotel_name}} </h5>

                                    <div class="price">&euro; 149<span>a night</span></div>
                                </div>
                                <div class="content">
                                    <p>{{$Item->hotel_description}}</p>

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <ul class="list-unstyled">
                                                <?php $facilaties = json_decode($Item->hotel_facilities);
                                                $count = count($facilaties);

                                                ?>
                                                @for($i=0;$i<round($count/2);$i++)
                                                    <li><i class="fa fa-check-circle"></i> {{$facilaties[$i]}}</li>
                                                @endfor
                                            </ul>
                                        </div>
                                        <div class="col-xs-6">
                                            <ul class="list-unstyled">
                                                @for($i=round($count/2);$i<$count;$i++)
                                                    <li><i class="fa fa-check-circle"></i> {{$facilaties[$i]}}</li>
                                                @endfor
                                            </ul>
                                        </div>
                                    </div>
                                    {{--<a href="room-detail.html" class="btn btn-primary btn-block">Read More</a>--}}
                                    <a href="{{URL::to('/hotel/')}}{{"/".$Item->hotel_id}}"
                                       class="btn btn-primary btn-block">Room List</a>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>
        </div>
        <div class="pagination-row">
            {!!$hotels->render()!!}
        </div>
    </section>

@stop