@extends('user.layout.main_layout')

@section('content')
<script type="text/javascript">$(document).ready(function () {
        $('#parallax-pagetitle').parallax("50%", -0.55);
    });</script>

<section class="parallax-effect">
    <div id="parallax-pagetitle" style="background-image: url(./resources/assets/frontend/images/parallax/parallax-01.jpg);">
        <div class="color-overlay">
            <!-- Page title -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">login</li>
                        </ol>
                        <h1>Login</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">

        <!-- Contact details -->

        <!-- Contact form -->
        <section id="contact-form" class="mt50">
            <div class="col-md-4">
            </div>
            <div class="col-md-4 room-thumb1">
                <h2 class="lined-heading"><span>Login</span></h2>
                <div id="message" style="color:red">
                    {{ $errors->first() }}
                    @if (Session::has('verified'))
                    <div class="alert alert-info">{{ Session::get('verified') }}</div>
                    @endif
                </div>
                <!-- Error message display -->
                <form class="clearfix mt50 login-form" role="form" method="post" action="{{URL::to('/checkLogin')}}" name="contactform">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">

                                <label for="username" accesskey="E"><span class="required">*</span>Email</label>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input name="username" type="text" id="username" placeholder="email" value="" class="form-control"/>
                                <div class="alert-danger alert-bg-remove" id="usernameError" style="display: none;"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="`password" accesskey="E"><span class="required">*</span> Password</label>
                                <input name="password" type="password" id="password" value="" class="form-control"/>
                                <div class="alert-danger alert-bg-remove" id="passwordError" style="display: none;"></div>
                            </div>
                            <div class="form-group cstm-btn">
                                <input type="checkbox">&nbsp;Remember Me</input>
                            </div>
                        </div>
                    </div>
                    <div class="cstm-btn">
                        <input type="submit" class="btn  btn-lg btn-default" onclick="return false;" value="Cancel"/>
                        <input type="submit" class="btn  btn-lg btn-primary" value="Login"/>
                    </div>
                    <div class="cstm-btn margin-top">
                        <a href="{{Url::to('/signup')}}">Not registered yet! Register Now</a>
                    </div>
                </form>

            </div>
        </section>
    </div>
</div>
<script>
    $("#password").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $('form[name="contactform"]').submit();
        }
    });
</script>

@stop

