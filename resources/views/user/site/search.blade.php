@extends('user.layout.main_layout')

@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!-- Parallax Effect -->
    <script type="text/javascript">$(document).ready(function () {
            $('#parallax-pagetitle').parallax("50%", -0.55);
        });</script>

    <section class="parallax-effect">
        <div id="parallax-pagetitle" style="background-image: url(./images/parallax/parallax-01.jpg);">
            <div class="color-overlay">
                <!-- Page title -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <ol class="breadcrumb">
                                <li><a href="index.html">Home</a></li>
                                <li><a href="room-list.html">Room list view</a></li>
                                <li class="active">Room detail</li>
                            </ol>
                            <h1>Search Results</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="reservation-form" class="mt50 clearfix">
        <div class="col-md-1">
        </div>
        <div class="col-md-10 cstm-srch">
            <form class="reservation-vertical clearfix" action="{{URL::to('/available-hotels')}}" method="get"
                  name="reservationform">

                <h2 class="lined-heading cstm-srch-2"><span>Search Room</span></h2>

                <div id="message" class="alert alert-danger" style="display: none;"></div>
                <!-- Error message display -->
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="email" accesskey="E">I want to go</label>
                        <input name="Search" type="text" value="{{$input['Search']}}" id="destination"
                               class="form-control"


                               placeholder="Enter your area or city or hotel name"/>
                    </div>
                    <div class="form-group">
                        <label for="room">Room Type</label>

                        <div class="popover-icon" data-container="body" data-toggle="popover"
                             data-trigger="hover" data-placement="right"
                             data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."><i
                                    class="fa fa-info-circle fa-lg"> </i></div>
                        <select class="form-control" name="room" id="room">
                            <option selected="selected" readonly value="">Select a room</option>
                            @foreach($room_types as $type)
                                <option value="{{$type['ads_value']}}" @if($input['room']==$type['ads_value']){{"Selected"}}@endif>{{$type['ads_value']}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="row cstm-bottom">
                    <div class="col-md-4">
                        <div class="row cstm-bottom">
                            <div class="col-md-6 no-padding">
                                <label for="checkin">Check-In</label>

                                <div class="popover-icon" data-container="body" data-toggle="popover"
                                     data-trigger="hover" data-placement="right"
                                     data-content="Check-In is from 11:00"><i
                                            class="fa fa-info-circle fa-lg"> </i></div>
                                <i class="fa fa-calendar infield"></i>
                                <input name="checkin" type="text" id="checkin" value="" class="form-control"
                                       placeholder="Check-in"/>
                            </div>
                            <div class="col-md-6 no-margin">
                                <label for="checkin">Check-Out</label>

                                <div class="popover-icon" data-container="body" data-toggle="popover"
                                     data-trigger="hover" data-placement="right"
                                     data-content="Check-In is from 11:00"><i
                                            class="fa fa-info-circle fa-lg"> </i></div>
                                <i class="fa fa-calendar infield"></i>
                                <input name="checkout" type="text" id="checkout" value="" class="form-control"
                                       placeholder="Check-out"/>
                            </div>
                        </div>
                        <div class="row cstm-bottom">
                            <div class="col-md-6 no-margin cstm-lbl">
                                <label for="checkin">Room 1</label>
                            </div>
                            <div class="col-md-6 no-margin">
                                <div class="guests-select">
                                    <label>Guests</label>
                                    <i class="fa fa-user infield"></i>

                                    <div class="total form-control" id="test">1</div>
                                    <div class="guests">
                                        <div class="form-group adults">
                                            <label for="adults">Adults</label>

                                            <div class="popover-icon" data-container="body"
                                                 data-toggle="popover" data-trigger="hover"
                                                 data-placement="right" data-content="+18 years"><i
                                                        class="fa fa-info-circle fa-lg"> </i></div>
                                            <select name="adults" id="adults" class="form-control">
                                                <option value="1">1 adult</option>
                                                <option value="2">2 adults</option>
                                                <option value="3">3 adults</option>
                                            </select>
                                        </div>
                                        <div class="form-group children">
                                            <label for="children">Children</label>

                                            <div class="popover-icon" data-container="body"
                                                 data-toggle="popover" data-trigger="hover"
                                                 data-placement="right" data-content="0 till 18 years"><i
                                                        class="fa fa-info-circle fa-lg"> </i></div>
                                            <select name="children" id="children" class="form-control">
                                                <option value="0">0 children</option>
                                                <option value="1">1 child</option>
                                                <option value="2">2 children</option>
                                                <option value="3">3 children</option>
                                            </select>
                                        </div>
                                        <button type="button" class="btn btn-default button-save btn-block">
                                            Save
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="from-group cstm-add">
                            <a href="#">Add another room</a>
                        </div>

                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="email" accesskey="E">Nights</label>
                            <select name="nights" id="" class="form-control">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="1">4</option>
                                <option value="2">5</option>
                                <option value="3">6</option>
                            </select>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token()}}">
                        <button type="submit" class="btn btn-primary btn-block">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <section>
        <div class="container mt50">
            <div class="row">
                @if($hotels!=null)
                    @foreach($hotels as $Item)
                        <div class="col-sm-4">
                            <div class="room-thumb">
                                @if($Item->hotel_banner_image=='')
                                    <img src="{{URL::to('resources/assets/frontend/images/rooms/356x228.gif')}}"
                                         alt="room 2" class="img-responsive"/>

                                @else
                                    <img src="{{URL::to('public/images/hotels/'.$Item->hotel_banner_image)}}"
                                         alt="room 2" class="img-responsive"/>
                                @endif
                                <div class="mask">
                                    <div class="main">
                                        <h5>{{$Item->hotel_name}} </h5>

                                        <div class="price">&euro; 149<span>a night</span></div>
                                    </div>
                                    <div class="content">
                                        <p>{{$Item->hotel_description}}</p>

                                        <div class="row">
                                            <div class="col-xs-6">
                                                <ul class="list-unstyled">
                                                    <?php $facilaties = json_decode($Item->hotel_facilities);
                                                    $count = count($facilaties);

                                                    ?>
                                                    @for($i=0;$i<round($count/2);$i++)
                                                        <li><i class="fa fa-check-circle"></i> {{$facilaties[$i]}}</li>
                                                    @endfor
                                                </ul>
                                            </div>
                                            <div class="col-xs-6">
                                                <ul class="list-unstyled">
                                                    @for($i=round($count/2);$i<$count;$i++)
                                                        <li><i class="fa fa-check-circle"></i> {{$facilaties[$i]}}</li>
                                                    @endfor
                                                </ul>
                                            </div>
                                        </div>
                                        {{--<a href="room-detail.html" class="btn btn-primary btn-block">Read More</a>--}}
                                        <a href="{{URL::to('/roomlist/')}}{{"/".$Item->hotel_id}}"
                                           class="btn btn-primary btn-block">Room List</a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    @endforeach
                @else
                    <div class="alert alert-danger">
                        <b>Sorry!</b> No Hotels Were Found Matching Your Search.

                    </div>
                @endif

            </div>
            {{--<div class="row">--}}
            {{--<ul class="pagination pagination-lg clearfix">--}}
            {{--<li class="disabled"><a href="#">«</a></li>--}}
            {{--<li class="active"><a href="#">1</a></li>--}}
            {{--<li><a href="#">2</a></li>--}}
            {{--<li><a href="#">3</a></li>--}}
            {{--<li><a href="#">4</a></li>--}}
            {{--<li><a href="#">5</a></li>--}}
            {{--<li><a href="#">»</a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}
        </div>
    </section>
    <input type="hidden" value="{{ csrf_token()}}" id="LocToken">




    <script>
        $(".reservation-vertical").submit(function () {
            if ($("#destination").val() == "") {
                $("#message").text('Please Type Your Destination').show("slow");
                setInterval(function () {
                    $("#message").hide("slow")
                }, 5000)
                return false;
            }
        })
        $("#destination").keyup(function () {

            var data = {location: $("#destination").val(), _token: $("#LocToken").val()};
            $.ajax({
                type: "POST",
                url: $("#base_url").val() + "/getLocations",
                data: data,
                datatype: "Json",
                success: function (data) {
                    var availableTags = $.parseJSON(data);
                    $("#destination").autocomplete({
                        source: availableTags
                    });

                }
            });
        })
    </script>
@stop