@extends('user.layout.main_layout')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<section class="revolution-slider">
    <div class="bannercontainer">
        <div class="banner">
            <ul>
                <!-- Slide 1 -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="1500">
                    <!-- Main Image -->
                    <img src="{{URL::to('resources/assets/frontend/images/slides/slide-bg.jpg')}}"
                         style="opacity:0;" alt="slidebg1" data-bgfit="cover" data-bgposition="left bottom"
                         data-bgrepeat="no-repeat">
                    <!-- Layers -->
                    <!-- Layer 1 -->
                    <div class="caption sft revolution-starhotel bigtext"
                         data-x="505"
                         data-y="30"
                         data-speed="700"
                         data-start="1700"
                         data-easing="easeOutBack">
                        <span>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </span>
                        A Five Star Hotel
                        <span>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </span>
                    </div>
                    <!-- Layer 2 -->
                    <div class="caption sft revolution-starhotel smalltext"
                         data-x="605"
                         data-y="105"
                         data-speed="800"
                         data-start="1700"
                         data-easing="easeOutBack">
                        <span>And we like to keep it that way!</span></div>
                    <!-- Layer 3 -->
                    <div class="caption sft"
                         data-x="775"
                         data-y="175"
                         data-speed="1000"
                         data-start="1900"
                         data-easing="easeOutBack">
                        <a href="room-list.html" class="button btn btn-info btn-lg">See rooms</a>
                    </div>
                </li>
                <!-- Slide 2 -->
                <li data-transition="boxfade" data-slotamount="7" data-masterspeed="1000">
                    <!-- Main Image -->
                    <img src="{{URL::to('resources/assets/frontend/images/slides/slide-bg-02.jpg')}}"
                         alt="darkblurbg" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                    <!-- Layers -->
                    <!-- Layer 1 -->
                    <div class="caption sft revolution-starhotel bigtext"
                         data-x="585"
                         data-y="30"
                         data-speed="700"
                         data-start="1700"
                         data-easing="easeOutBack">
                        <span>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </span>
                        Double room
                        <span>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </span>
                    </div>
                    <!-- Layer 2 -->
                    <div class="caption sft revolution-starhotel smalltext"
                         data-x="682"
                         data-y="105"
                         data-speed="800"
                         data-start="1700"
                         data-easing="easeOutBack">
                        <span>€ 99,- a night this summer</span></div>
                    <!-- Layer 3 -->
                    <div class="caption sft"
                         data-x="785"
                         data-y="175"
                         data-speed="1000"
                         data-start="1900"
                         data-easing="easeOutBack">
                        <a href="room-detail.html" class="button btn btn-info btn-lg">Book this room</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>

<!-- Reservation form -->
<section id="reservation-form" classs="cstm-pos-marg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <form class="reservation-vertical clearfix" action="{{URL::to('/available-hotels')}}" method="get"
                      name="reservationform">

                    <h2 class="lined-heading"><span>Search Room</span></h2>

                    <div id="message1">
                        @if (Session::has('regSuccess'))
                        <div class="alert alert-success"><b>Registration Successful! </b> A mail has been sent
                            to your email Address. Please follow the instructions
                        </div>
                        @endif
                    </div>
                    <div id="message" class="alert alert-danger" style="display: none;"></div>
                    <!-- Error message display -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" accesskey="E">I want to go</label>
                            <input name="Search" type="text" value="" id="destination" class="form-control"

                                   placeholder="Enter your area or city or hotel name"/>
                        </div>
                        <div class="form-group">
                            <label for="room">Room Type</label>

                            <div class="popover-icon" data-container="body" data-toggle="popover"
                                 data-trigger="hover" data-placement="right"
                                 data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."><i
                                    class="fa fa-info-circle fa-lg"> </i></div>
                            <select class="form-control" name="room" id="room">
                                <option selected="selected" readonly value="">Select a room</option>
                                @foreach($room_types as $type)
                                <option value="{{$type['ads_value']}}">{{$type['ads_value']}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="row cstm-bottom">
                        <div class="col-md-4">
                            <div class="row cstm-bottom">
                                <div class="col-md-6 no-padding">
                                    <label for="checkin">Check-In</label>

                                    <div class="popover-icon" data-container="body" data-toggle="popover"
                                         data-trigger="hover" data-placement="right"
                                         data-content="Check-In is from 11:00"><i
                                            class="fa fa-info-circle fa-lg"> </i></div>
                                    <i class="fa fa-calendar infield"></i>
                                    <input name="checkin" type="text" id="checkin" value="" class="form-control"
                                           placeholder="Check-in"/>
                                </div>
                                <div class="col-md-6 no-margin cstm-pos-marg">
                                    <label for="checkin">Check-Out</label>

                                    <div class="popover-icon" data-container="body" data-toggle="popover"
                                         data-trigger="hover" data-placement="right"
                                         data-content="Check-In is from 11:00"><i
                                            class="fa fa-info-circle fa-lg"> </i></div>
                                    <i class="fa fa-calendar infield"></i>
                                    <input name="checkout" type="text" id="checkout" value="" class="form-control"
                                           placeholder="Check-out"/>
                                </div>
                            </div>
                            <div class="row cstm-bottom">
                                <div class="col-md-6 no-margin cstm-lbl">
                                    <label for="checkin">Room 1</label>
                                </div>
                                <div class="col-md-6 no-margin">
                                    <div class="guests-select">
                                        <label>Guests</label>
                                        <i class="fa fa-user infield"></i>

                                        <div class="total form-control" id="test">1</div>
                                        <div class="guests">
                                            <div class="form-group adults">
                                                <label for="adults">Adults</label>

                                                <div class="popover-icon" data-container="body"
                                                     data-toggle="popover" data-trigger="hover"
                                                     data-placement="right" data-content="+18 years"><i
                                                        class="fa fa-info-circle fa-lg"> </i></div>
                                                <select name="adults" id="adults" class="form-control">
                                                    <option value="1">1 adult</option>
                                                    <option value="2">2 adults</option>
                                                    <option value="3">3 adults</option>
                                                </select>
                                            </div>
                                            <div class="form-group children">
                                                <label for="children">Children</label>

                                                <div class="popover-icon" data-container="body"
                                                     data-toggle="popover" data-trigger="hover"
                                                     data-placement="right" data-content="0 till 18 years"><i
                                                        class="fa fa-info-circle fa-lg"> </i></div>
                                                <select name="children" id="children" class="form-control">
                                                    <option value="0">0 children</option>
                                                    <option value="1">1 child</option>
                                                    <option value="2">2 children</option>
                                                    <option value="3">3 children</option>
                                                </select>
                                            </div>
                                            <button type="button" class="btn btn-default button-save btn-block">
                                                Save
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="from-group cstm-add">
                                <a href="#">Add another room</a>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="email" accesskey="E">Nights</label>
                                <select name="nights" id="" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="1">4</option>
                                    <option value="2">5</option>
                                    <option value="3">6</option>
                                </select>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                            <button type="submit" class="btn btn-primary btn-block">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- Rooms -->
<section class="rooms mt50">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="lined-heading"><span>Guests Favorite Rooms</span></h2>
            </div>
            <!-- Room -->
            <div class="col-sm-4">
                <div class="room-thumb"><img src="{{URL::to('resources/assets/frontend/images/rooms/356x228.gif')}}" alt="room 1" class="img-responsive"/>

                    <div class="mask">
                        <div class="main">
                            <h5>Double bedroom</h5>

                            <div class="price">&euro; 99<span>a night</span></div>
                        </div>
                        <div class="content">
                            <p><span>A modern hotel room in Star Hotel</span> Nunc tempor erat in magna pulvinar
                                fermentum. Pellentesque scelerisque at leo nec vestibulum.
                                malesuada metus.</p>

                            <div class="row">
                                <div class="col-xs-6">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
                                        <li><i class="fa fa-check-circle"></i> Private balcony</li>
                                        <li><i class="fa fa-check-circle"></i> Sea view</li>
                                    </ul>
                                </div>
                                <div class="col-xs-6">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check-circle"></i> Free Wi-Fi</li>
                                        <li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
                                        <li><i class="fa fa-check-circle"></i> Bathroom</li>
                                    </ul>
                                </div>
                            </div>
                            <a href="room-detail.html" class="btn btn-primary btn-block">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Room -->
            <div class="col-sm-4">
                <div class="room-thumb"><img src="{{URL::to('resources/assets/frontend/images/rooms/356x228.gif')}}"
                                             alt="room 2" class="img-responsive"/>

                    <div class="mask">
                        <div class="main">
                            <h5>King Size Bedroom </h5>

                            <div class="price">&euro; 149<span>a night</span></div>
                        </div>
                        <div class="content">
                            <p><span>A modern hotel room in Star Hotel</span> Nunc tempor erat in magna pulvinar
                                fermentum. Pellentesque scelerisque at leo nec vestibulum.
                                malesuada metus.</p>

                            <div class="row">
                                <div class="col-xs-6">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
                                        <li><i class="fa fa-check-circle"></i> Private balcony</li>
                                        <li><i class="fa fa-check-circle"></i> Sea view</li>
                                    </ul>
                                </div>
                                <div class="col-xs-6">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check-circle"></i> Free Wi-Fi</li>
                                        <li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
                                        <li><i class="fa fa-check-circle"></i> Bathroom</li>
                                    </ul>
                                </div>
                            </div>
                            <a href="room-detail.html" class="btn btn-primary btn-block">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Room -->
            <div class="col-sm-4">
                <div class="room-thumb"><img src="{{URL::to('resources/assets/frontend/images/rooms/356x228.gif')}}"
                                             alt="room 3" class="img-responsive"/>

                    <div class="mask">
                        <div class="main">
                            <h5>Single room</h5>

                            <div class="price">&euro; 120<span>a night</span></div>
                        </div>
                        <div class="content">
                            <p><span>A modern hotel room in Star Hotel</span> Nunc tempor erat in magna pulvinar
                                fermentum. Pellentesque scelerisque at leo nec vestibulum.
                                malesuada metus.</p>

                            <div class="row">
                                <div class="col-xs-6">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
                                        <li><i class="fa fa-check-circle"></i> Private balcony</li>
                                        <li><i class="fa fa-check-circle"></i> Sea view</li>
                                    </ul>
                                </div>
                                <div class="col-xs-6">
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check-circle"></i> Free Wi-Fi</li>
                                        <li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
                                        <li><i class="fa fa-check-circle"></i> Bathroom</li>
                                    </ul>
                                </div>
                            </div>
                            <a href="room-detail.html" class="btn btn-primary btn-block">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- USP's -->
<section class="usp mt100">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="lined-heading"><span>USP Section</span></h2>
            </div>
            <div class="col-sm-3 bounceIn appear" data-start="0">
                <div class="box-icon">
                    <div class="circle"><i class="fa fa-glass fa-lg"></i></div>
                    <h3>Beverages included</h3>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum eleifend augue,
                        quis rhoncus purus fermentum. </p>
                    <a href="#">Read more<i class="fa fa-angle-right"></i></a></div>
            </div>
            <div class="col-sm-3 bounceIn appear" data-start="400">
                <div class="box-icon">
                    <div class="circle"><i class="fa fa-credit-card fa-lg"></i></div>
                    <h3>Stay First, Pay After!</h3>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum eleifend augue,
                        quis rhoncus purus fermentum. </p>
                    <a href="#">Read more<i class="fa fa-angle-right"></i></a></div>
            </div>
            <div class="col-sm-3 bounceIn appear" data-start="800">
                <div class="box-icon">
                    <div class="circle"><i class="fa fa-cutlery fa-lg"></i></div>
                    <h3>24 Hour Restaurant</h3>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum eleifend augue,
                        quis rhoncus purus fermentum. </p>
                    <a href="#">Read more<i class="fa fa-angle-right"></i></a></div>
            </div>
            <div class="col-sm-3 bounceIn appear" data-start="1200">
                <div class="box-icon">
                    <div class="circle"><i class="fa fa-tint fa-lg"></i></div>
                    <h3>Spa Included!</h3>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum eleifend augue,
                        quis rhoncus purus fermentum. </p>
                    <a href="#">Read more<i class="fa fa-angle-right"></i></a></div>
            </div>
        </div>
    </div>
</section>

<!-- Parallax Effect -->
<script type="text/javascript">$(document).ready(function () {
    $('#parallax-image').parallax("50%", -0.25);
});</script>

<input type="hidden" value="{{ csrf_token()}}" id="LocToken">

<script>
    $(".reservation-vertical").submit(function () {
        if ($("#destination").val() == "") {
            $("#message").text('Please Type Your Destination').show("slow");
            setInterval(function () {
                $("#message").hide("slow")
            }, 5000)
            return false;
        }
    })
    $("#destination").keyup(function () {

        var data = {location: $("#destination").val(), _token: $("#LocToken").val()};
        $.ajax({
            type: "POST",
            url: $("#base_url").val() + "/getLocations",
            data: data,
            datatype: "Json",
            success: function (data) {
                var obj = $.parseJSON(data);
                var availableTags = $.parseJSON(data);

                $("#destination").autocomplete({
                    source: availableTags
                });

            }
        });
    })
</script>
@stop

