@extends('user.layout.main_layout')
@section('content')
    <script type="text/javascript">$(document).ready(function () {
            $('#parallax-pagetitle').parallax("50%", -0.55);
        });</script>

    <section class="parallax-effect">
        <div id="parallax-pagetitle" style="background-image: url(./resources/assets/frontend/images/parallax/parallax-01.jpg);">
            <div class="color-overlay">
                <!-- Page title -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <ol class="breadcrumb">
                                <li><a href="index.html">Home</a></li>
                                <li class="active">Contact</li>
                            </ol>
                            <h1>Sign Up</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">

            <!-- Contact details -->

            <!-- Contact form -->
            <section id="contact-form" class="mt50">
                <div class="col-md-3">
                </div>
                <div class="col-md-6 room-thumb1">
                    <h2 class="lined-heading"><span>Sign Up Now</span></h2>

                    <div id="message" style="color:red"> {{ $errors->first() }}</div>
                    <!-- Error message display -->
                    <form class="clearfix mt50 reg-form" role="form" method="post" action="{{URL::to('/register')}}"
                          name="contactform">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <label for="fname" accesskey="U"><span class="required">*</span> First Name</label>
                                    <input name="firstname" type="text" id="firstname" class="form-control" value=""/>
                                    <div class="alert-danger alert-bg-remove" id="fnameError" style="display: none;"></div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lname" accesskey="E"><span class="required">*</span> Last Name</label>
                                    <input name="lastname" type="text" id="lastname" value="" class="form-control"/>
                                    <div class="alert-danger alert-bg-remove" id="lnameError" style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email" accesskey="E"><span class="required">*</span> E-mail</label>
                                    <input name="email" type="text" id="email" value="" class="form-control"/>
                                    <div class="alert-danger alert-bg-remove" id="mailError" style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="password" accesskey="E"><span class="required">*</span>
                                        Password</label>
                                    <input name="password" type="password" id="password" value=""
                                           class="form-control"/>
                                    <div class="alert-danger alert-bg-remove" id="passwordError" style="display: none;"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email" accesskey="E"><span class="required">*</span> Confirm
                                        Password</label>
                                    <input name="password_confirmation" type="password" id="password_confirmation"
                                           value="" class="form-control"/>
                                    <div class="alert-danger alert-bg-remove" id="CpasswordError" style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row cstm-btn">
                            <Input type="submit" class="btn  btn-lg btn-default" value="Cancel"
                                  id="cancel" onclick="return false;">
                            <Input type="submit" class="btn  btn-lg btn-primary" value="Sign up">
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    <script>
        $("#password_confirmation").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                $('form[name="contactform"]').submit();
            }
        });
        $('#cancel').click(
                function()
                {
                    window.location = $('#base_url').val()+"/";
                }
        );
    </script>

@stop