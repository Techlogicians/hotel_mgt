<footer>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-6"> &copy; 2015 Company_name All Rights Reserved. </div>
                <div class="col-xs-6 text-right">
                    <ul>
                        <li><a href="{{URL::to('/')}}">Home</a></li>
                        <li><a href="{{Url::to("/hotels-list")}}">Hotel List View</a></li>
                        <li><a href="#">Find a Hotel</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>