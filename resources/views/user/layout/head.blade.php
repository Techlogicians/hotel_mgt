<meta charset="utf-8">
    <title>Thumbler</title>
    <meta name="baseUrl" content="{{URL::to('/')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/css/style.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/css/animate.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/css/prettyPhoto.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/css/smoothness/jquery-ui-1.10.4.custom.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/rs-plugin/css/settings.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/css/theme.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/css/colors/turquoise.css')}}">
    <link rel="stylesheet" href="{{URL::to('resources/assets/frontend/css/responsive.css')}}">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600,700">

    <!-- Javascripts -->
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/jquery-1.11.0.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/bootstrap-hover-dropdown.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/jquery.parallax-1.1.3.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/jquery.nicescroll.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/jquery.prettyPhoto.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/jquery-ui-1.10.4.custom.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/jquery.jigowatt.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/jquery.sticky.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/waypoints.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/jquery.isotope.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/jquery.gmap.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/http://maps.google.com/maps/api/js?sensor=false')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('resources/assets/frontend/js/custom.js')}}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')}}"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js')}}"></script>
    <![endif]-->
<input type="hidden" value="{{URL::to('/')}}" id='base_url' />

