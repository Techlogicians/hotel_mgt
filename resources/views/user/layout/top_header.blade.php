<!-- Parallax Effect -->
<script type="text/javascript">$(document).ready(function () {
        $('#parallax-pagetitle').parallax("50%", -0.55);
    });</script>

<div id="top-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="th-text pull-left">
                    <div class="th-item"> <a href="#"><i class="fa fa-phone"></i> 00-000 000 000</a> </div>
                    <div class="th-item"> <a href="#"><i class="fa fa-envelope"></i> mail@hotel.com </a></div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="th-text pull-right">
                    <div class="th-item cstm-sign-log">
                        <?php $id=Session::get('user');?>
                        @if(empty($id))
                           <a href="{{URL::to('/login')}}">Login</a> /
                        @else
                                <a href="{{URL::to('/logout')}}">Logout</a>/
                         @endif

                                <a href="{{URL::to('/signup')}}">Sign Up</a>
                    </div>
                    {{--<div class="th-item">--}}
                        {{--<div class="social-icons"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-linkedin"></i></a> <a href="#"><i class="fa fa-youtube-play"></i></a> </div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
</div>