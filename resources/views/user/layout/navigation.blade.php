
    <!-- Navigation -->
    <div class="navbar yamm navbar-default" id="sticky">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a href="{{URL::to('/')}}" class="navbar-brand">
                    <!-- Logo -->
                    <div id="logo"> <img id="default-logo" src="{{URL::to('resources/assets/frontend/images/your-logo.png')}}" alt="Starhotel" style="height:44px;"> <img id="retina-logo" src="images/logo-retina.png" alt="Starhotel" style="height:44px;"> </div>
                </a> </div>
            <div id="navbar-collapse-grid" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown active"> <a href="{{URL::to('/')}}">Home</a>
                    </li>
                    <li class="dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle js-activated">Hotels<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{Url::to("/hotels-list")}}">Hotel List View</a></li>
                            <li><a href="#">Find a Hotel</a></li>
                        </ul>
                    </li>
                    <li><a href="">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
