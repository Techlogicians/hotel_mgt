<!doctype html>
<html>
    <head>
        @include('user/layout/head')
    </head>
    <body >
           @include('user/layout/top_header')
            <header class="">
                @include('user/layout/navigation')
            </header>

            <div id="main">

                @yield('content')

            </div>

            <footer class="">
                @include('user/layout/footer')
            </footer>
           <div id="go-top"><i class="fa fa-angle-up fa-2x"></i></div>
    </body>
</html>