<!DOCTYPE html>
<html>
<head>
    <title>Zabuto | Calendar | Action</title>
    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- jQuery CDN -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

    <!-- Bootstrap CDN -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Zabuto -->
    <link rel="stylesheet" type="text/css" href="//zabuto.com/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="//zabuto.com/assets/css/examples.css">

    <!-- Zabuto Calendar -->
    <script src="{{asset('public/js/zabuto_calendar.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/zabuto_calendar.min.css')}}">

</head>
<body>

<!-- container -->
<div class="container example">

    <div class="">
        <div class="">

            <div id="date-popover" class="popover top"
                 style="cursor: pointer; display: block; margin-left: 33%; margin-top: -50px; width: 175px;">
                <div class="arrow"></div>
                <h3 class="popover-title" style="display: none;"></h3>

                <div id="date-popover-content" class="popover-content"></div>
            </div>

            <div id="my-calendar"></div>

            <script type="application/javascript">
                $(document).ready(function () {
                    $("#date-popover").popover({html: true, trigger: "manual"});
                    $("#date-popover").hide();
                    $("#date-popover").click(function (e) {
                        $(this).hide();
                    });

                    $("#my-calendar").zabuto_calendar({
                        action: function () {
                            return myDateFunction(this.id, false);
                        },
                        action_nav: function () {
                            return myNavFunction(this.id);
                        },
                        ajax: {
                            url: "show_data.php?action=1",
                            modal: true
                        },
                        legend: [
                            {type: "text", label: "Special event", badge: "00"},
                            {type: "block", label: "Regular event"}
                        ]
                    });
                });

                function myDateFunction(id, fromModal) {

                    $("#date-popover").hide();
                    if (fromModal) {
                        $("#" + id + "_modal").modal("hide");
                    }
                    var date = $("#" + id).data("date");
                    alert(date);
                    var hasEvent = $("#" + id).data("hasEvent");
                    if (hasEvent && !fromModal) {
                        return false;
                    }
                    $("#date-popover-content").html('You clicked on date ' + date);
                    $("#date-popover").show();
                    return true;
                }

                function myNavFunction(id) {
                    $("#date-popover").hide();
                    var nav = $("#" + id).data("navigation");
                    var to = $("#" + id).data("to");
                    console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
                }
            </script>

        </div>

    </div>
</div>
<!-- /container -->

</body>
</html>
