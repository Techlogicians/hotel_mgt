@extends('user.layout.main_layout')

@section('content')
    <script type="text/javascript">$(document).ready(function () {
            $('#parallax-pagetitle').parallax("50%", -0.55);
        });</script>

    <section class="parallax-effect">
        <div id="parallax-pagetitle" style="background-image: url(./resources/assets/frontend/images/parallax/parallax-01.jpg);">
            <div class="color-overlay">
                <!-- Page title -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <ol class="breadcrumb">
                                <li><a href="{{URL::to('/profile')}}">Profile</a></li>
                                <li class="active">Password</li>
                            </ol>
                            <h1>Change Password</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">

            <!-- Contact details -->

            <!-- Contact form -->
            <section id="contact-form" class="mt50">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <h2 class="lined-heading"><span>Change Password</span></h2>
                    <div id="message" style="color:red">
                        {{ $errors->first() }}
                    </div>
                    <!-- Error message display -->
                    <form class="clearfix mt50" role="form" method="post" action="{{URL::to('/updatePassword')}}" name="contactform">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <label for="oldpassword" accesskey="U"><span class="required">*</span> Old Password</label>
                                    <input name="oldpassword" type="password" id="oldpassword" class="form-control" value="" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="newpassword" accesskey="E"><span class="required">*</span>New Password</label>
                                    <input name="new_password" type="password" id="newpassword" placeholder="Must be atleast 6 characters" value="" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="confirm_newpassword" accesskey="E"><span class="required">*</span>Retype New Password</label>
                                    <input name="new_password_confirmation" type="password" id="newpassword_confirmation" value="" class="form-control"/>
                                </div>
                            </div>

                        </div>
                        <input type="submit" class="btn  btn-lg btn-default" id="cancel" value="Cancel" onclick="return false;"/>
                        <input type="submit" class="btn  btn-lg btn-primary" value="Update"/>
                    </form>

                </div>
            </section>
        </div>
    </div>
    <script>
        $("#newpassword_confirmation").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                $('form[name="contactform"]').submit();
            }
        });
        $('#cancel').click(
                function()
                {
                    window.location = $('#base_url').val()+"/profile";
                }
        );
    </script>

@stop

