@extends('user.layout.main_layout')

@section('content')
    <style>
        .help-block{
            color:red;
        }
        .form-group input[type="text"] {
            color:#000000;
        }
    </style>
    <script type="text/javascript">$(document).ready(function () {
            $('#parallax-pagetitle').parallax("50%", -0.55);
        });</script>

    <section class="parallax-effect">
        <div id="parallax-pagetitle" style="background-image: url(./resources/assets/frontend/images/parallax/parallax-01.jpg);">
            <div class="color-overlay">
                <!-- Page title -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <ol class="breadcrumb">
                                <li><a href="{{URL::to('/profile')}}">Profile</a></li>
                                <li class="active">Edit profile</li>
                            </ol>
                            <h1>Edit Profile</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">

            <!-- Contact details -->

            <!-- Contact form -->
            <section id="contact-form" class="mt50">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <h2 class="lined-heading"><span>Edit Profile</span></h2>
                    {{--<div id="message" style="color:red">--}}
                        {{--{{ $errors->first() }}--}}
                    {{--</div>--}}
                    <!-- Error message display -->
                    <form class="clearfix mt50" role="form" method="post" action="{{URL::to('/updateprofile')}}" name="contactform">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">

                                    <label for="fname" accesskey="U"><span class="required">*</span> First Name</label>

                                    <input name="firstname" type="text" id="name" class="form-control" value="{{$user->user_firstname}}"/>
                                    @if ($errors->has('firstname')) <p class="help-block">{{ $errors->first('firstname') }}</p> @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">

                                    <label for="email" accesskey="E"><span class="required">*</span> Last Name</label>
                                    <input name="lastname" type="text" id="email" value="{{$user->user_lastname}}" class="form-control"/>
                                    @if ($errors->has('lastname')) <p class="help-block">{{ $errors->first('lastname') }}</p> @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    @if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
                                    <label for="email" accesskey="E"><span class="required">*</span>Address</label>
                                    <input name="address" type="text" id="email" value="{{$user->user_address}}" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">

                                    <label for="email" accesskey="E"><span class="required">*</span>Country</label>
                                    <select name="country" class="form-control" id="county">
                                        @foreach($locations as $location)
                                            <option value="{{$location->location_id}}" <?php echo( $user->user_country == $location->location_id) ? ' selected="selected"' : '';?>>{{$location->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">

                                    <label for="email" accesskey="E"><span class="required">*</span> City</label>
                                    <select name="city" id="city" class="form-control">
                                        @foreach($cities as $city)
                                            <option value="{{$city->location_id}}" <?php echo( $user->user_city == $city->location_id) ? ' selected="selected"' : '';?>>{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">

                                    <label for="email" accesskey="E"><span class="required">*</span>Zip</label>
                                    <input name="zip" type="text" id="email" value="{{$user->user_zip}}" class="form-control"/>
                                    @if ($errors->has('zip')) <p class="help-block">{{ $errors->first('zip') }}</p> @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">

                                    <label for="email" accesskey="E"><span class="required">*</span>Phone</label>
                                    <input name="phone" type="text" id="email" value="{{$user->user_phone}}" class="form-control"/>
                                    @if ($errors->has('phone')) <p class="help-block">{{ $errors->first('phone') }}</p> @endif
                                </div>
                            </div>
                        </div>
                        <input type="button" id="cancel" class="btn  btn-lg btn-default" value="Cancel" />
                        <input type="submit" class="btn  btn-lg btn-primary" value="Update"/>
                    </form>

                </div>
            </section>
        </div>
    </div>
    <script>
        $(document).ready(
                function()
                {
                    $('#cancel').click(
                         function()
                         {
                             window.location = $('#base_url').val()+"/profile";
                         }
                    );
                    $('#county').change(
                            function()
                            {
                                var country = this.value;
                                var token = $('#token').val();
                                $.ajax({
                                    url: $("#base_url").val() + "/citylist",
                                    type: 'POST',
                                    data: {id: country,_token:token},
                                    dataType:'json',
                                    success: function(result) {
                                        if (result.status)
                                        {
                                            console.log(result.html);
                                            $('#city').html(result.html);

                                        }
                                    }
                                });
                            }
                    );
                }
        );
    </script>

@stop

