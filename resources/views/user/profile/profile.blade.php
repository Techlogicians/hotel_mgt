@extends('user.layout.main_layout')

@section('content')
    <script type="text/javascript">$(document).ready(function () {
            $('#parallax-pagetitle').parallax("50%", -0.55);
        });</script>

    <section class="parallax-effect">
        <div id="parallax-pagetitle" style="background-image: url(./resources/assets/frontend/images/parallax/parallax-01.jpg);">
            <div class="color-overlay">
                <!-- Page title -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <ol class="breadcrumb">
                                <li><a href="index.html">Home</a></li>
                                <li class="active">Profile</li>
                            </ol>
                            <h1>Profile</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">

            <!-- Contact details -->

            <!-- Contact form -->
            <section id="contact-form" class="mt50">
                <div class="col-md-2">
                </div>
                <div class="col-md-10">
                    <h2 class="lined-heading"><span>Details Info About me</span></h2>
                    {{--edit photo--}}
                    <div class="modal fade" id="profile-photo-modal" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">×</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Update Profile Photo</h4>
                                </div>
                                <div class="modal-body">
                                    <div style="position:relative;">
                                        <a class="btn btn-primary btn-mob" href="javascript:;">
                                            Choose File...
                                            <input class="upload-profile-image upload-mob-image" type="file"
                                                   name="file_source" size="40"
                                                   onchange='$("#upload-file-info").html($(this).val());'>
                                        </a>
                                        &nbsp;
                                        <span class="label label-info" id="upload-file-info"></span>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-success" id="changeProfile"
                                            data-dismiss="modal">Save changes
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--edit photo--}}
                    <div id="message">
                        @if (Session::has('updated'))
                            <div class="alert alert-info">{{ Session::pull('updated') }}</div>
                        @endif
                    </div>
                    <!-- Error message display -->
                    <form class="clearfix mt50" role="form" method="post" action="php/contact.php" name="contactform"
                          id="contactform">
                        <div class="row">

                            <div class="span2">
                                @if($user->user_photo!='')
                                    <img src="{{asset('public/images/users/'.$user->user_photo)}}" alt="Image 1"
                                         class="img-thumbnail img-responsive" alt="" class="img-rounded">
                                @else
                                    <img src="{{URL::to('resources/assets/frontend/images/rooms/356x228.gif')}}"
                                         alt="Image 1"
                                         class="img-thumbnail img-responsive" alt="" class="img-rounded">
                                @endif

                            </div> <button type="button" class="btn btn-info" data-toggle="modal" data-target="#profile-photo-modal">Change Profile Picture</button>

                            <div class="span4">
                                <blockquote>
                                    <p>{{$user->user_firstname}} {{$user->user_lastname}}</p>
                                    @if($user->cities!=null)
                                        <small><cite title="Source Title">{{$user->cities->name}}
                                                , {{$user->countries->name}} <i class="icon-map-marker"></i></cite>
                                        </small>
                                    @endif
                                </blockquote>
                                <p>
                                    <i class="fa fa-envelope"></i> &nbsp;{{$user->user_email}} <br>
                                    @if($user->user_phone!='')
                                        <i class="fa fa-phone"></i>&nbsp;{{$user->user_phone}} <br>
                                    @endif
                                    <i class="fa fa-pencil-square-o"></i>&nbsp;<a href="{{URL::to('/changePassword')}}">Change
                                        Password</a>


                                </p>
                            </div>


                        </div>
                    </form>
                    <div>
                        <a href="{{URL::to('/editprofile')}}" type="submit" class="btn  btn-lg btn-primary"
                           data-role="button">Edit Profile</a>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script>
        $(function () {
            $('.upload-profile-image').on('change', function (event) {// we fetch the file data
                var files = this.files;
                var reader = new FileReader();
                var name = this.value;
                reader.onload = function (e) {
                    $(".user-img").find("img").attr("src", e.target.result);
                };
                reader.readAsDataURL(files[0]);
            });
            $('#changeProfile').on("click", function () {
                var avatar = $('.upload-profile-image')[0].files;
                if (avatar.length != 0) {
                    var formData = new FormData();
                    formData.append('avatar', $('.upload-profile-image').get(0).files[0]);
                    formData.append('_token', $('#token').val())
                    $.ajax({
                        url: "<?php echo URL::to('/'); ?>" + "/changeprofilepic",
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            obj = JSON.parse(result);
                            if (obj.status) {
                                alert(obj.msg);
                                location.reload();
                            }
                        }
                    });
                }
            });

        });
    </script>

@stop

